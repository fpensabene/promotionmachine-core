/*
 Navicat MySQL Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50538
 Source Host           : localhost
 Source Database       : promotionmachine2015_new

 Target Server Type    : MySQL
 Target Server Version : 50538
 File Encoding         : utf-8

 Date: 07/15/2015 15:17:07 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `CORE_Concorsi`
-- ----------------------------
DROP TABLE IF EXISTS `CORE_Concorsi`;
CREATE TABLE `CORE_Concorsi` (
  `concorsoID` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  `data_inizio` datetime DEFAULT NULL,
  `data_fine` datetime DEFAULT NULL,
  `regolamento` varchar(250) DEFAULT NULL,
  `privacy` varchar(250) DEFAULT NULL,
  `FK_concorsi_stati` int(11) NOT NULL,
  `codice_analytics` text,
  `admin_email` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`concorsoID`),
  KEY `FK_concorsi_stati` (`FK_concorsi_stati`),
  KEY `FK_concorsi_stati_2` (`FK_concorsi_stati`),
  KEY `FK_concorsi_stati_3` (`FK_concorsi_stati`),
  CONSTRAINT `fk_CORE_concorsi_stato1` FOREIGN KEY (`FK_concorsi_stati`) REFERENCES `stati` (`statoID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `CORE_Periodi`
-- ----------------------------
DROP TABLE IF EXISTS `CORE_Periodi`;
CREATE TABLE `CORE_Periodi` (
  `periodoID` int(11) NOT NULL AUTO_INCREMENT,
  `descrizione` varchar(250) DEFAULT NULL,
  `data_inizio` datetime DEFAULT NULL,
  `data_fine` datetime DEFAULT NULL,
  `FK_periodi_concorsi` int(11) NOT NULL,
  `FK_periodi_stati` int(11) NOT NULL,
  PRIMARY KEY (`periodoID`),
  KEY `fk_CORE_periodi_CORE_concorso1_idx` (`FK_periodi_concorsi`),
  KEY `fk_CORE_periodi_stato1_idx` (`FK_periodi_stati`),
  CONSTRAINT `fk_CORE_periodi_CORE_concorso1` FOREIGN KEY (`FK_periodi_concorsi`) REFERENCES `CORE_Concorsi` (`concorsoID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_CORE_periodi_stato1` FOREIGN KEY (`FK_periodi_stati`) REFERENCES `stati` (`statoID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `CORE_Users`
-- ----------------------------
DROP TABLE IF EXISTS `CORE_Users`;
CREATE TABLE `CORE_Users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `nome` varchar(45) DEFAULT NULL COMMENT '					',
  `cognome` varchar(45) DEFAULT NULL,
  `cf` varchar(45) DEFAULT NULL,
  `documento` varchar(45) DEFAULT NULL,
  `data_nascita` datetime DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `FK_user_concorso` int(11) DEFAULT NULL,
  `double_optint` tinyint(4) DEFAULT NULL,
  `ip_address` varchar(15) NOT NULL,
  `created_on` int(11) NOT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `last_login` int(11) DEFAULT NULL,
  `informativa_a` tinyint(255) DEFAULT NULL,
  `informativa_b` tinyint(255) DEFAULT NULL,
  `informativa_c` tinyint(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_user_concorso` (`FK_user_concorso`),
  CONSTRAINT `fk_CORE_User_concorso` FOREIGN KEY (`FK_user_concorso`) REFERENCES `CORE_Concorsi` (`concorsoID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `CORE_controllers`
-- ----------------------------
DROP TABLE IF EXISTS `CORE_controllers`;
CREATE TABLE `CORE_controllers` (
  `controllerID` int(11) NOT NULL AUTO_INCREMENT,
  `nome_controller_backend` varchar(32) DEFAULT NULL,
  `nome_controller_frontend` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`controllerID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `CORE_prove`
-- ----------------------------
DROP TABLE IF EXISTS `CORE_prove`;
CREATE TABLE `CORE_prove` (
  `provaID` int(11) NOT NULL AUTO_INCREMENT,
  `FK_prove_periodi` int(11) NOT NULL,
  `FK_prove_stati` int(11) NOT NULL,
  `nome` varchar(45) DEFAULT NULL,
  `descrizione` text,
  `data_inizio` datetime DEFAULT NULL,
  `data_fine` datetime DEFAULT NULL,
  `FK_prove_tipiProve` int(11) NOT NULL,
  `ripetibile` varchar(4) DEFAULT '0',
  `slug` varchar(128) DEFAULT NULL,
  `solo_maggiorenni` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`provaID`),
  UNIQUE KEY `slug` (`slug`),
  KEY `fk_CORE_prove_CORE_periodi_idx` (`FK_prove_periodi`),
  KEY `fk_CORE_prove_stato1_idx` (`FK_prove_stati`),
  KEY `FK_prove_tipiProve` (`FK_prove_tipiProve`),
  CONSTRAINT `fk_CORE_prove_CORE_periodi` FOREIGN KEY (`FK_prove_periodi`) REFERENCES `CORE_Periodi` (`periodoID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_CORE_prove_stato1` FOREIGN KEY (`FK_prove_stati`) REFERENCES `stati` (`statoID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_CORE_prove_tipiProve` FOREIGN KEY (`FK_prove_tipiProve`) REFERENCES `CORE_tipoProva` (`tipoProvaID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `CORE_tipoProva`
-- ----------------------------
DROP TABLE IF EXISTS `CORE_tipoProva`;
CREATE TABLE `CORE_tipoProva` (
  `tipoProvaID` int(11) NOT NULL AUTO_INCREMENT,
  `punteggio` int(11) DEFAULT NULL,
  `descrizione` varchar(250) DEFAULT NULL,
  `FK_CORE_tipoProva_CORE_controllers` int(11) DEFAULT NULL,
  PRIMARY KEY (`tipoProvaID`),
  KEY `tipoProvaID` (`tipoProvaID`),
  KEY `FK_CORE_tipoProva_CORE_controllers` (`FK_CORE_tipoProva_CORE_controllers`),
  CONSTRAINT `FK_CORE_tipoProva_CORE_controllers` FOREIGN KEY (`FK_CORE_tipoProva_CORE_controllers`) REFERENCES `CORE_controllers` (`controllerID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `ci_sessions`
-- ----------------------------
DROP TABLE IF EXISTS `ci_sessions`;
CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) DEFAULT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `estrazione`
-- ----------------------------
DROP TABLE IF EXISTS `estrazione`;
CREATE TABLE `estrazione` (
  `instantWinID` int(11) NOT NULL,
  `data_inizio` datetime DEFAULT NULL,
  `data_fine` datetime DEFAULT NULL,
  `descrizione` varchar(45) DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `FK_estrazione_tipoEstrazione` int(11) NOT NULL,
  PRIMARY KEY (`instantWinID`),
  KEY `fk_estrazione_tipoEstrazione1_idx` (`FK_estrazione_tipoEstrazione`),
  CONSTRAINT `fk_estrazione_tipoEstrazione1` FOREIGN KEY (`FK_estrazione_tipoEstrazione`) REFERENCES `tipoEstrazione` (`tipoEstrazioneID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `estrazione_has_CORE_prove`
-- ----------------------------
DROP TABLE IF EXISTS `estrazione_has_CORE_prove`;
CREATE TABLE `estrazione_has_CORE_prove` (
  `estrazione_instantWinID` int(11) NOT NULL,
  `CORE_prove_provaID` int(11) NOT NULL,
  PRIMARY KEY (`estrazione_instantWinID`,`CORE_prove_provaID`),
  KEY `fk_estrazione_has_CORE_prove_CORE_prove1_idx` (`CORE_prove_provaID`),
  KEY `fk_estrazione_has_CORE_prove_estrazione1_idx` (`estrazione_instantWinID`),
  CONSTRAINT `fk_estrazione_has_CORE_prove_CORE_prove1` FOREIGN KEY (`CORE_prove_provaID`) REFERENCES `CORE_prove` (`provaID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_estrazione_has_CORE_prove_estrazione1` FOREIGN KEY (`estrazione_instantWinID`) REFERENCES `estrazione` (`instantWinID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `groups`
-- ----------------------------
DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `helpContestant`
-- ----------------------------
DROP TABLE IF EXISTS `helpContestant`;
CREATE TABLE `helpContestant` (
  `helpContestantID` int(11) NOT NULL,
  `messaggio` text,
  `risposto` tinyint(4) DEFAULT NULL,
  `FK_helpContestant_contestant` int(11) NOT NULL,
  PRIMARY KEY (`helpContestantID`),
  KEY `FK_helpContestant_contestant` (`FK_helpContestant_contestant`),
  CONSTRAINT `fk_helpContestant_CORE_User1` FOREIGN KEY (`FK_helpContestant_contestant`) REFERENCES `CORE_Users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `login_attempts`
-- ----------------------------
DROP TABLE IF EXISTS `login_attempts`;
CREATE TABLE `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `punteggio`
-- ----------------------------
DROP TABLE IF EXISTS `punteggio`;
CREATE TABLE `punteggio` (
  `punteggioID` int(11) NOT NULL AUTO_INCREMENT,
  `data_convalida` datetime DEFAULT NULL,
  `FK_punteggio_validitaProva` int(11) NOT NULL,
  `data_azione` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `FK_punteggio_user` int(11) NOT NULL,
  `FK_punteggio_prova` int(11) NOT NULL,
  `punteggio` int(11) DEFAULT NULL,
  `ip` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`punteggioID`),
  KEY `fk_punteggio_validitaProva1_idx` (`FK_punteggio_validitaProva`),
  KEY `fk_punteggio_prova` (`FK_punteggio_prova`) USING BTREE,
  KEY `FK_punteggio_user` (`FK_punteggio_user`),
  CONSTRAINT `fk_punteggio_CORE_prova1` FOREIGN KEY (`FK_punteggio_prova`) REFERENCES `CORE_prove` (`provaID`) ON UPDATE NO ACTION,
  CONSTRAINT `fk_punteggio_CORE_user1` FOREIGN KEY (`FK_punteggio_user`) REFERENCES `CORE_Users` (`id`) ON UPDATE NO ACTION,
  CONSTRAINT `fk_punteggio_validitaProva1` FOREIGN KEY (`FK_punteggio_validitaProva`) REFERENCES `validitaProva` (`validitaProvaID`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `quesito1-3`
-- ----------------------------
DROP TABLE IF EXISTS `quesito1-3`;
CREATE TABLE `quesito1-3` (
  `idQUIZ_quesito` int(11) NOT NULL,
  `domanda` varchar(45) DEFAULT NULL,
  `risposta_1` varchar(45) DEFAULT NULL,
  `risposta_2` varchar(45) DEFAULT NULL,
  `risposta_3` varchar(45) DEFAULT NULL,
  `FK_quesito1-3_tipoProva` int(11) NOT NULL,
  PRIMARY KEY (`idQUIZ_quesito`),
  KEY `fk_QUIZ_quesito_1-3_CORE_tipo_prova1_idx` (`FK_quesito1-3_tipoProva`),
  CONSTRAINT `fk_QUIZ_quesito_1-3_CORE_tipo_prova1` FOREIGN KEY (`FK_quesito1-3_tipoProva`) REFERENCES `CORE_tipoProva` (`tipoProvaID`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `stati`
-- ----------------------------
DROP TABLE IF EXISTS `stati`;
CREATE TABLE `stati` (
  `statoID` int(11) NOT NULL AUTO_INCREMENT,
  `descrizione` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`statoID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `tipoEstrazione`
-- ----------------------------
DROP TABLE IF EXISTS `tipoEstrazione`;
CREATE TABLE `tipoEstrazione` (
  `tipoEstrazioneID` int(11) NOT NULL,
  `descrizione` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`tipoEstrazioneID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `upload`
-- ----------------------------
DROP TABLE IF EXISTS `upload`;
CREATE TABLE `upload` (
  `idUPLOAD_carica` int(11) NOT NULL AUTO_INCREMENT,
  `FK_upload_tipoProva` int(11) NOT NULL,
  `FK_upload_punteggio` int(11) NOT NULL,
  `motivazione_moderazione` text,
  `textcol_1` text,
  `textcol_2` text,
  `textcol_3` text,
  `textcol_4` text,
  `textcol_5` text,
  `textcol_6` text,
  `textcol_7` text,
  `textcol_8` text,
  `textcol_9` text,
  `textcol_10` text,
  `datetimecol_1` datetime DEFAULT NULL,
  `datetimecol_2` datetime DEFAULT NULL,
  `datetimecol_3` datetime DEFAULT NULL,
  `datetimecol_4` datetime DEFAULT NULL,
  `datetimecol_5` datetime DEFAULT NULL,
  `datetimecol_6` datetime DEFAULT NULL,
  `datetimecol_7` datetime DEFAULT NULL,
  `datetimecol_8` datetime DEFAULT NULL,
  `datetimecol_9` datetime DEFAULT NULL,
  `datetimecol_10` datetime DEFAULT NULL,
  `filecol_1` text,
  `filecol_2` text,
  `filecol_3` text,
  `filecol_4` text,
  `filecol_5` text,
  `filecol_6` text,
  `filecol_7` text,
  `filecol_8` text,
  `filecol_9` text,
  `filecol_10` text,
  PRIMARY KEY (`idUPLOAD_carica`),
  KEY `fk_UPLOAD_carica_CORE_tipo_prova1_idx` (`FK_upload_tipoProva`),
  KEY `FK_upload_punteggio` (`FK_upload_punteggio`),
  CONSTRAINT `fk_UPLOAD_carica_CORE_punteggio1` FOREIGN KEY (`FK_upload_punteggio`) REFERENCES `punteggio` (`punteggioID`) ON UPDATE NO ACTION,
  CONSTRAINT `fk_UPLOAD_carica_CORE_tipo_prova1` FOREIGN KEY (`FK_upload_tipoProva`) REFERENCES `CORE_tipoProva` (`tipoProvaID`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `users_groups`
-- ----------------------------
DROP TABLE IF EXISTS `users_groups`;
CREATE TABLE `users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`),
  CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `validitaProva`
-- ----------------------------
DROP TABLE IF EXISTS `validitaProva`;
CREATE TABLE `validitaProva` (
  `validitaProvaID` int(11) NOT NULL AUTO_INCREMENT,
  `descrizione` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`validitaProvaID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
--  View structure for `generator_16`
-- ----------------------------
DROP VIEW IF EXISTS `generator_16`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `generator_16` AS select 0 AS `n` union all select 1 AS `1` union all select 2 AS `2` union all select 3 AS `3` union all select 4 AS `4` union all select 5 AS `5` union all select 6 AS `6` union all select 7 AS `7` union all select 8 AS `8` union all select 9 AS `9` union all select 10 AS `10` union all select 11 AS `11` union all select 12 AS `12` union all select 13 AS `13` union all select 14 AS `14` union all select 15 AS `15`;

-- ----------------------------
--  View structure for `generator_256`
-- ----------------------------
DROP VIEW IF EXISTS `generator_256`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `generator_256` AS select ((`hi`.`n` << 4) | `lo`.`n`) AS `n` from (`generator_16` `lo` join `generator_16` `hi`);

-- ----------------------------
--  View structure for `generator_4k`
-- ----------------------------
DROP VIEW IF EXISTS `generator_4k`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `generator_4k` AS select ((`hi`.`n` << 8) | `lo`.`n`) AS `n` from (`generator_256` `lo` join `generator_16` `hi`);

-- ----------------------------
--  Triggers structure for table CORE_Concorsi
-- ----------------------------
DROP TRIGGER IF EXISTS `allinea_prove_periodi_concorso`;
delimiter ;;
CREATE TRIGGER `allinea_prove_periodi_concorso` AFTER UPDATE ON `CORE_Concorsi` FOR EACH ROW BEGIN

   UPDATE CORE_Periodi periodo set periodo.data_inizio = NEW.data_inizio
   WHERE 
		(periodo.data_inizio < NEW.data_inizio
		OR periodo.data_inizio > NEW.data_fine)
   AND periodo.FK_periodi_concorsi = NEW.concorsoID;

   UPDATE CORE_Periodi periodo set periodo.data_fine = NEW.data_fine
   WHERE 
		(periodo.data_fine < NEW.data_inizio
		OR periodo.data_fine > NEW.data_fine)
   AND periodo.FK_periodi_concorsi = NEW.concorsoID;


   UPDATE CORE_prove prova set prova.data_inizio = NEW.data_inizio
   WHERE (prova.data_inizio < NEW.data_inizio
		 OR prova.data_inizio > NEW.data_fine)
   AND prova.FK_prove_periodi = ANY (SELECT periodoID from CORE_Periodi p where p.FK_periodi_concorsi = NEW.concorsoID );

   UPDATE CORE_prove prova set prova.data_fine = NEW.data_fine
   WHERE (prova.data_fine < NEW.data_inizio
		OR prova.data_fine > NEW.data_fine)
   AND prova.FK_prove_periodi = ANY (SELECT periodoID from CORE_Periodi p where p.FK_periodi_concorsi = NEW.concorsoID );

END
 ;;
delimiter ;

delimiter ;;
-- ----------------------------
--  Triggers structure for table CORE_Periodi
-- ----------------------------
 ;;
delimiter ;
DROP TRIGGER IF EXISTS `allinea_prove_periodo`;
delimiter ;;
CREATE TRIGGER `allinea_prove_periodo` AFTER UPDATE ON `CORE_Periodi` FOR EACH ROW BEGIN

   UPDATE CORE_prove prova set prova.data_inizio = NEW.data_inizio
   WHERE /*prova.data_inizio < NEW.data_inizio*/
	(prova.data_inizio < NEW.data_inizio
		OR prova.data_inizio > NEW.data_fine)
   AND prova.FK_prove_periodi = NEW.periodoID;

   UPDATE CORE_prove prova set prova.data_fine = NEW.data_fine
   WHERE /*prova.data_fine > NEW.data_fine*/
		(prova.data_fine < NEW.data_inizio
		OR prova.data_fine > NEW.data_fine)
   AND prova.FK_prove_periodi = NEW.periodoID;

END
 ;;
delimiter ;

delimiter ;;
-- ----------------------------
--  Triggers structure for table CORE_prove
-- ----------------------------
 ;;
delimiter ;
DROP TRIGGER IF EXISTS `CORE_prove_trg1`;
delimiter ;;
CREATE TRIGGER `CORE_prove_trg1` BEFORE INSERT ON `CORE_prove` FOR EACH ROW BEGIN
DECLARE slugvar varchar(128);
DECLARE countdup int;
DECLARE unixtimestamp TIMESTAMP;
SET slugvar := ' ';
SET countdup := 0;

select UNIX_TIMESTAMP() into unixtimestamp;
select LOWER(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TRIM(NEW.nome), ':', ''), ')', ''), '(', ''), ',', ''), '\\', ''), '/', ''), '"', ''), '?', ''), '\'', ''), '&', ''), '!', ''), '.', ''), ' ', '-'), '--', '-'), '--', '-')) into slugvar;
select count(provaID) from CORE_prove where CORE_prove.slug = slugvar and CORE_prove.provaID != new.provaID INTO countdup;
case 
	when countdup > 0
		then SET slugvar := concat(slugvar,'-',unixtimestamp);
	else SET slugvar:=slugvar;
end case;
SET new.slug = slugvar;
   -- variable declarations

   -- trigger code

END
 ;;
delimiter ;
DROP TRIGGER IF EXISTS `CORE_prove_trg2`;
delimiter ;;
CREATE TRIGGER `CORE_prove_trg2` BEFORE UPDATE ON `CORE_prove` FOR EACH ROW BEGIN
DECLARE slugvar varchar(128);
DECLARE countdup int;
DECLARE unixtimestamp varchar(128);
SET slugvar := ' ';
SET countdup := 0;

select CURRENT_TIMESTAMP into unixtimestamp;
select LOWER(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TRIM(NEW.nome), ':', ''), ')', ''), '(', ''), ',', ''), '\\', ''), '/', ''), '"', ''), '?', ''), '\'', ''), '&', ''), '!', ''), '.', ''), ' ', '-'), '--', '-'), '--', '-')) into slugvar;
select count(provaID) from CORE_prove where CORE_prove.slug = slugvar and CORE_prove.provaID != new.provaID INTO countdup;
case 
	when countdup > 0
		then SET slugvar := concat(slugvar,'-',UNIX_TIMESTAMP());
	else SET slugvar:=slugvar;
end case;
SET new.slug = slugvar;
   -- variable declarations

   -- trigger code

END
 ;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
