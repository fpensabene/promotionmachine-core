INSERT INTO `groups` (`id`, `name`, `description`) VALUES
     (1, 'admin','Administrator'),
     (2, 'contestants','Partecipanti al concorso'),
     (3, 'moderator_group', 'Moderatori'),
     (4, 'banned', 'Esclusi dal concorso');

INSERT INTO `CORE_Users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `created_on`, `last_login`, `active`, `nome`, `cognome` ) VALUES
     (1, '127.0.0.1','diegoburgio','$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36','','diego.burgio@oltremedia.it','',NULL,'1268889823','1268889823','1', 'Diego','Burgio'),
     (2, '127.0.0.1','stefanocella','$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36','','stefano.cella@oltremedia.it','',NULL,'1268889823','1268889823','1', 'Stefano','Cella'),
     (3, '127.0.0.1','francescopensabene','$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36','','francesco.pensabene@oltremedia.it','',NULL,'1268889823','1268889823','1', 'Francesco','Pensabene');
/*
	admin@admin.com:password
*/
INSERT INTO `users_groups` (`user_id`, `group_id`) VALUES
     (1,1),
     (2,1),
     (3,1);
     

INSERT INTO `stati` (`descrizione`) VALUES 
	('Attivo'),
	('Disattivo');	
     
INSERT INTO `CORE_Concorsi` ( `data_inizio`, `privacy`, `regolamento`, `nome`, `data_fine`, `FK_concorsi_stati`) VALUES 
	('2015-01-01 00:00:00', null, null, 'Concorso', '2016-12-31 00:00:00', '1');
	
INSERT INTO `validitaProva` ( `descrizione`) VALUES 
	('Valida'),
	('Non valida'),
	('In attesa di validazione');

INSERT INTO `CORE_controllers` ( `nome_controller_frontend`, `nome_controller_backend`) VALUES 
	('upload', 'upload_backend'),
	('iscrizione', 'iscrizione');

INSERT INTO `CORE_tipoProva` ( `punteggio`, `FK_CORE_tipoProva_CORE_controllers`, `descrizione`) VALUES 
	( '1', '1', 'Upload'),
	( '0', '2', 'Iscrizione');
	
	