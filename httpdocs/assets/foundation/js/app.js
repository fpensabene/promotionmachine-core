// Foundation JavaScript
// Documentation can be found at: http://foundation.zurb.com/docs
$(document).foundation({
  equalizer : {
    // Specify if Equalizer should make elements equal height once they become stacked.
    equalize_on_stack: true
  }
});

function printMsg(tipo,messaggio){
	if ($("#output").is(":visible")){
		$("#output").hide();
	}
	$("#output").removeClass("success").removeClass("alert");
	$("#output").addClass(tipo);
	$("#output").html(messaggio);
	$("#output").toggle( "drop", {direction: "up"}, 200  );
}

// where: è una select che dobbiamo popolare
// base_url è la url di base a cui fare la chiamata

function popolaStati(where,base_url){
	$.ajax({
	    type:'GET',
	    dataType: 'json',
	    async: false,
	    url:base_url+'stato/get_dati_stati_json/',
	    data:{},
	    success:function(response){
	    //attenzione. success (questo sopra) vuol dire che la chiamata ha avuto successo, che il server ha risposto senza dare errori fatali
	    	if (response.result=='success'){
		    	var dati = response.output;
		        for (var i = 0; i < dati.length; i++) {
			        $(where).append("<option value='"+dati[i]['statoID']+"'>"+dati[i]['descrizione']+"</option>");
				}
	        }
	        else {
	        }
	    },
	    error:function(response){
	    }
	});	
}

function popolaTipiProva(where,base_url){
	$.ajax({
	    type:'GET',
	    dataType: 'json',
	    url:base_url+'tipo_prova/get_dati_tipi_prova/',
	    data:{},
	    success:function(response){
	    //attenzione. success (questo sopra) vuol dire che la chiamata ha avuto successo, che il server ha risposto senza dare errori fatali
	    	if (response.result=='success'){
		    	var dati = response.output;
		        for (var i = 0; i < dati.length; i++) {
			        $(where).append("<option value='"+dati[i]['tipoProvaID']+"'>"+dati[i]['descrizione']+"</option>");
				}
	        }
	        else {
	        }
	    },
	    error:function(response){
	    }
	});		
}

