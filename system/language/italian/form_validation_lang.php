<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2015, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (http://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2015, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	http://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['form_validation_required']		= 'Il campo {field} &egrave; richiesto';
$lang['form_validation_isset']			= 'Il campo {field} deve contenere un valore';
$lang['form_validation_valid_email']		= 'Il campo {field} deve contenere un\'indirizzo email valido.';
$lang['form_validation_valid_emails']		= 'Il campo {field} must contain all valid email addresses.';
$lang['form_validation_valid_url']		= 'Il campo {field} deve contenre una URL valida';
$lang['form_validation_valid_ip']		= 'Il campo {field} must contain a valid IP.';
$lang['form_validation_min_length']		= 'Il campo {field} deve essere di almeno {param} caratteri.';
$lang['form_validation_max_length']		= 'Il campo {field} cannot exceed {param} characters in length.';
$lang['form_validation_exact_length']		= 'Il campo {field} non pu&ograve; essere pi&ugrave; lungo di {param} caratteri.';
$lang['form_validation_alpha']			= 'Il campo {field} may only contain alphabetical characters.';
$lang['form_validation_alpha_numeric']		= 'Il campo {field} may only contain alpha-numeric characters.';
$lang['form_validation_alpha_numeric_spaces']	= 'Il campo {field} may only contain alpha-numeric characters and spaces.';
$lang['form_validation_alpha_dash']		= 'Il campo {field} may only contain alpha-numeric characters, underscores, and dashes.';
$lang['form_validation_numeric']		= 'Il campo {field} must contain only numbers.';
$lang['form_validation_is_numeric']		= 'Il campo {field} must contain only numeric characters.';
$lang['form_validation_integer']		= 'Il campo {field} must contain an integer.';
$lang['form_validation_regex_match']		= 'Il campo {field} is not in the correct format.';
$lang['form_validation_matches']		= 'Il campo {field} deve essere uguale al campo {param}.';
$lang['form_validation_differs']		= 'Il campo {field} must differ from the {param} field.';
$lang['form_validation_is_unique'] 		= 'Il campo {field} must contain a unique value.';
$lang['form_validation_is_natural']		= 'Il campo {field} must only contain digits.';
$lang['form_validation_is_natural_no_zero']	= 'Il campo {field} must only contain digits and must be greater than zero.';
$lang['form_validation_decimal']		= 'Il campo {field} must contain a decimal number.';
$lang['form_validation_less_than']		= 'Il campo {field} must contain a number less than {param}.';
$lang['form_validation_less_than_equal_to']	= 'Il campo {field} must contain a number less than or equal to {param}.';
$lang['form_validation_greater_than']		= 'Il campo {field} must contain a number greater than {param}.';
$lang['form_validation_greater_than_equal_to']	= 'Il campo {field} must contain a number greater than or equal to {param}.';
$lang['form_validation_error_message_not_set']	= 'Unable to access an error message corresponding to your field name {field}.';
$lang['form_validation_in_list']		= 'Il campo {field} must be one of: {param}.';
