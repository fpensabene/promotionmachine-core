<?php 

// config.env.php viene caricato in index.php 
// Una volta caricato viene eseguito il ciclo qui sotto e quindi viene definita la costante ENVIRONMENT

// Successivamente in database.php viene caricato il file config.master.php che attraverso la costante ENVIRONMENT
// carica il set di variabili per il database


// INOLTRE come consigliato nella index.php alla riga 54 - abbiamo inserito anche le impostazioni di ERROR REPORTING
// in ognuno dei files di configurazione di AMBIENTE (ENVIRONMENTS)



if ( ! defined('ENVIRONMENT'))
{
	switch (strtolower($_SERVER['HTTP_HOST'])) {
		case 'xxxxxxx' :
			define('ENVIRONMENT', 'production');
			//define('ENV_DEBUG', FALSE);
			//echo 'production';
		break;
		
		case 'prm2015.oltremedia.net' :
			define('ENVIRONMENT', 'staging');
			//define('ENV_DEBUG', FALSE);
			//echo 'staging';
		break;
		
		default :
			define('ENVIRONMENT', 'local');
			//define('ENV_DEBUG', FALSE);
			//echo 'local';
		break;
	}
}


/* End of file config.env.php */
/* Location: ./config/config.env.php */