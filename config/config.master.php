<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/* include i dati per la connessione al db in base alla variabile defined('ENVIRONMENT')*/


if (defined('ENVIRONMENT'))
{
	/**
	 * Load our environment-specific config file
	 * which contains our database credentials
	 * 
	 * @see config/config.local.php
	 * @see config/config.dev.php
	 * @see config/config.stage.php
	 * @see config/config.prod.php
	 */
	require 'config.' . ENVIRONMENT . '.php';
	
}


// LISTA DI VARIABILI GLOBALI DEFINITE PER OGNI ISTANZA DI PROMOTIONMACHINE