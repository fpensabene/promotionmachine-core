<?php echo $barraNavigazione; ?>
<?php echo $testataConcorso; ?>

<!---***********************************-->
<!---*** ERRORI E ALLERT SERVER SIDE ***-->
<!---***********************************-->

<?php if (isset($message) && $message): ?>
<div class="row">
	<div class="small-12 medium-8 large-8 medium-centered large-centered column">
		<div id="output" data-alert class="alert-box radius <?if($message == 'success') echo "success"; else echo "alert";?>" >
			<a href="#" class="close">&times;</a><?if($message == 'success') echo "Hai svolto correttamente la prova. Grazie!"; else echo "Errore durante lo svolgimento della prova"?>
		</div>
	</div> <!--chiudo column-->
</div> <!--chiudo row-->
<?php endif; ?>


<!---*************-->
<!---*** INTRO ***-->
<!---*************-->
<section class="hrow">
	<div class="row">
		<div class="small-12 medium-8 large-8 medium-centered large-centered column">
		<div class="hrow-boxTitolo">
				<h1 class="hrow-boxTitolo-title">LISTA PROVE</h1>
				<p class="hrow-boxTitolo-subTitle">Ciao <strong><?=$this->ion_auth->user()->row()->nome?></strong>,<br>
			ogni mese, se registri almeno uno scontrino, puoi vincere una bici UHU RECYCLE! Compra uno stic UHU RENATURE presso un punto vendita I LOVE UHU e registra lo scontrino: puoi vincere una bici UHU RECYCLE! clicca sulle prove attive, segui le istruzioni e buona fortuna!</p>
			</div>	
			
		</div>
	</div>
</section>



<!---**************************-->
<!---*** LISTA PROVE ATTIVE ***-->
<!---**************************-->
<section class="hrow">
	<div class="row">
		<div class="small-12 medium-8 large-8 medium-centered large-centered column">
			<div class="hrow-listaProve hrow-listaProve--Attive">
				<!---**************************-->
				<!---*** LISTA PROVE ATTIVE ***-->
				<!---**************************-->
				<h3 class="listaProve-header">Lista delle prove attive:</h3>
				<div id="js-prove-attive" class="listaProve-content"></div>
			</div>			
		</div>
	</div>
</section>

<!---***************************-->
<!---*** LISTA PROVE SCADUTE ***-->
<!---***************************-->
<section class="hrow">
	<div class="row">
		<div class="small-12 medium-8 large-8 medium-centered large-centered column">
			<div class="hrow-listaProve hrow-listaProve--Scadute">
				<!---***************************-->
				<!---*** LISTA PROVE SCADUTE ***-->
				<!---***************************-->
				<h3 class="listaProve-header">Lista delle prove scadute che hai eseguito:</h3>
				<div id="js-prove-scadute" class="listaProve-content"></div>
			</div>	
		</div>
	</div>
</section>





	
	



<script>
	<?php
		// 	$dati_prove è un json che arriva dal controller
	?>
	$(document).ready(function(){
		var dati = <?=$dati_prove?>;
		var size_dati = <?=$dati_prove_size?>;
		var nome = "";
		var descrizione ="";
		var provaID ="";
		var slug = "";
		var div_prove = "";
		
		if (dati){
	    	for (var i = 0; i < size_dati; i++) {		    	
		    	nome = dati[i]['nome'];
		    	descrizione = dati[i]['descrizione'];
		    	slug = dati[i]['slug'];
		    	nome_controller = dati[i]['nome_controller_frontend'];
		    	if (dati[i]['collocazione_temporale'] == "presente"){
			    	div_prove = "#js-prove-attive";
		    	}
		    	else {
			    	div_prove = "#js-prove-scadute";
		    	}
		    	$(div_prove).append("<h5 class='listaProve-content-titolo'>"+nome+"</h5>");
		    	if (dati[i]['collocazione_temporale'] == "presente"){
		    		$(div_prove).append("<p class='listaProve-content-desc'>"+descrizione+"</p>");	
		    	}
		    	$(div_prove).append("<a class='button tiny radius' href='<?=base_url()?>"+nome_controller+"/"+slug+"'>vai</a>");
	    	} 
	    }
	})
</script>