<?php echo $barraNavigazione; ?>
<?php echo $testataConcorso; ?>

<!---*************-->
<!---*** INTRO ***-->
<!---*************-->
<section class="hrow">
	<div class="row">
		<div class="small-12 medium-8 large-8 medium-centered large-centered column">
		<div class="hrow-boxTitolo">
				<h1 class="hrow-boxTitolo-title">Iscriviti a UHU RECYCLE!</h1>
				<p class="hrow-boxTitolo-subTitle">Compila il form di iscrizione, clicca il tasto "registrati dati" e segui le istruzioni</p>
			</div>	
			
		</div>
	</div>
</section>


<!---***********************-->
<!---*** FORM ISCRIZIONE ***-->
<!---***********************-->
<section class="hrow">
	<div class="row">
		<div class="small-12 medium-8 large-8 medium-centered large-centered column">
			<div class="form-generico">
			
			
			
			<?php if($message): ?>
				<div id="infoMessage" class="info-message">
					<?php echo $message;?>
				</div>
			<?php endif; ?>
			
			
			<?php echo form_open("partecipa/iscriviti", array('id' => 'form_iscrizione'));?>
			<?php echo form_input($concorsoID);?>
			<div class="row">
				<div class="small-12 column">
					<label>
					Nome:
					<?php echo form_input($first_name, '', 'class="radius"');?>
					</label>
				</div> <!--chiudo column-->
			</div> <!--chiudo row-->
			
			<div class="row">
				<div class="small-12 column">
					<label>
					Cognome
					<?php echo form_input($last_name, '', 'class="radius"');?>
					</label>
				</div> <!--chiudo column-->
			</div> <!--chiudo row-->
			
			<div class="row">
				<div class="small-12 column">
					<label>
					Codice Fiscale
					<?php echo form_input($codice_fiscale, '', 'class="radius"');?>				
					</label>
				</div> <!--chiudo column-->
			</div> <!--chiudo row-->
			
			
			<div class="row">
				<div class="small-12 column">
					<label>
					Data di nascita:
					<?php echo form_input($data_nascita, '', 'class="radius"');?>
					</label>
				</div> <!--chiudo column-->
			</div> <!--chiudo row-->
			
			<div class="row">
				<div class="small-12 column">
					<label>
					Email:
					<?php echo form_input($email, '', 'class="radius"');?>
					</label>
				</div> <!--chiudo column-->
			</div> <!--chiudo row-->
			
			<div class="row">
				<div class="small-12 column">
					<label>
					Ripeti l'indirizzo email:
					<?php echo form_input($email_confirm, '', 'class="radius"');?>
					</label>
				</div> <!--chiudo column-->
			</div> <!--chiudo row-->
			
			<div class="row">
				<div class="small-12 column">
					<label>
					Scegli una password (da 8 a 20 caratteri):
					<?php echo form_input($password, '', 'class="radius"');?>				
					</label>
				</div> <!--chiudo column-->
			</div> <!--chiudo row-->
			
			<div class="row">
				<div class="small-12 column">
					<label>
					Conferma la password:
					<?php echo form_input($password_confirm, '', 'class="radius"');?>
					</label>
				</div> <!--chiudo column-->
			</div> <!--chiudo row-->
			
			<div class="row">
				<div class="small-12 column">
					<label>
					<?php echo "Ho letto <a target='_blank' href=".site_url().$this->dati_concorso['output']['privacy'].">l'informativa</a>  e accetto il punto A";?> <br />
		            <?php echo form_input($informativa_a);?>
					</label>
				</div> <!--chiudo column-->
			</div> <!--chiudo row-->
			
			<div class="row">
				<div class="small-12 column">
					<label>
					<?php echo "Ho letto <a target='_blank' href=".site_url().$this->dati_concorso['output']['privacy'].">l'informativa</a>  e accetto il punto B";?> <br />
		            <?php echo form_input($informativa_b);?>
					</label>
				</div> <!--chiudo column-->
			</div> <!--chiudo row-->
			
			<div class="row">
				<div class="small-12 column">
					<label>
					<?php echo "Ho letto <a target='_blank' href=".site_url().$this->dati_concorso['output']['privacy'].">l'informativa</a>  e accetto il punto C";?> <br />
		            <?php echo form_input($informativa_c);?>
					</label>
				</div> <!--chiudo column-->
			</div> <!--chiudo row-->
			
			<div class="row">
				<div class="small-12 column">
					<label>
					<?php echo "Ho letto <a target='_blank' href=".site_url().$this->dati_concorso['output']['regolamento'].">il regolamento</a>";?> <br />
					</label>
				</div> <!--chiudo column-->
			</div> <!--chiudo row-->
			
			<div class="row">
				<div class="small-12 column">
					<div class="bottone-form"><?php echo form_submit('submit', 'Registra dati', 'class="button radius expand"');?></div>
				</div> <!--chiudo column-->
			</div> <!--chiudo row-->
			
			
			<?php echo form_close();?>
			
			
			</div>
		</div> <!--chiudo column-->
	</div> <!--chiudo row-->
</section>



<script>
	$(document).ready(function(){
		//impedisce il copia incolla
		$('#email, #email_confirm, #password, #password_confirm').bind('copy paste',function(e) {
	    	e.preventDefault(); return false; 
		});
		//inizializza il datepicker
		$("input[name='data_nascita']").datepicker({
			dateFormat: 'dd/mm/yy',
			changeMonth: true,
			changeYear: true,
			yearRange: "1930:2005"
		});	
		
		$("#form_iscrizione").validate({		
			rules: {
				first_name: {
					required: true,
					minlength: 2				
				},
				last_name: {
					required: true,
					minlength: 2				
				},
				codice_fiscale: {
					required: true,
					minlength: 16,
					regex: /^[A-Z]{6}\d{2}[A-Z]\d{2}[A-Z]\d{3}[A-Z]$/
				},
				data_nascita: {
					required: true,
					minlength: 10,
					regex: /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/
				},
				email: {
					required: true,
					customEmail: true
				},
				email_confirm: {
					required: true,
					customEmail: true,
					equalTo: email
				},
				password: {
					required: true,
					minlength: 8,
					maxlength: 20
				},
				password_confirm: {
					required: true,
					minlength: 8,
					maxlength: 20,
					equalTo: password
				},
				informativa_b: {
					required: true
				}
			},
			messages: {
				first_name: {
					required: 'Devi inserire il tuo nome.',
					minlength: 'Il nome inserito non rispetta la lunghezza minima richiesta (2 caratteri)'				
				},
				last_name: {
					required: 'Devi inserire il tuo cognome.',
					minlength: 'Il cognome inserito non rispetta la lunghezza minima richiesta (2 caratteri)'				
				},
				codice_fiscale: {
					required: 'Devi inserire il tuo codice fiscale.',
					regex: "Il codice fiscale inserito &egrave; errato."			
				},
				data_nascita: {
					required: 'Devi inserire la tua data di nascita.',
					minlength: 'La data di nascita inserita non &egrave; valida.',
					regex: 'La data di nascita inserita non &egrave; valida.'
				},
				email: {
					required: 'Devi inserire la tua email',
				},
				email_confirm: {
					required: 'Devi inserire la tua email',
					equalTo: 'L\'email inserita non coincide con quella inserita nel campo "Email".'
				},
				password: {
					required: 'Devi inserire una password.',
					minlength: 'La lunghezza della password deve essere compresa tra 8 e 20 caratteri.',
					maxlength: 'La lunghezza della password deve essere compresa tra 8 e 20 caratteri.'
				},
				password_confirm: {
					required: 'Devi inserire una password.',
					minlength: 'La lunghezza della password deve essere compresa tra 8 e 20 caratteri.',
					maxlength: 'La lunghezza della password deve essere compresa tra 8 e 20 caratteri.',
					equalTo: 'La password inserita non coincide con quella inserita nel campo "Password".'
				},
				informativa_b: {
					required: 'Per iscriverti, devi obbligatoriamente accettare il punto B dell\'informativa.'
				}
			}
			
		});
		
	});
	
</script>
