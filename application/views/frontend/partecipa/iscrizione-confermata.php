<!---*****************************-->
<!---*** PAGINA PREMI IN PALIO ***-->
<!---*****************************-->

<?php echo $barraNavigazione; ?>
<?php echo $testataConcorso; ?>


<!---*************-->
<!---*** INTRO ***-->
<!---*************-->
<section class="hrow">
	<div class="row">
		<div class="small-12 medium-8 large-8 medium-centered large-centered column">
		<div class="hrow-boxTitolo">
				<h1 class="hrow-boxTitolo-title">ISCRIZIONE CONFERMATA!</h1>
				<p class="hrow-boxTitolo-subTitle">Complimenti! Hai completato con successo la tua iscrizione e puoi partecipare al concorso UHU RECYCLE! <a href="<?=site_url()?>partecipa/">Fai il login</a> e accedi alla tua pagina personale. Buona fortuna!<br>Il Team UHU.</p>
			</div>	
			
		</div>
	</div>
</section>