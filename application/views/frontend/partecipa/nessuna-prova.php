<?php echo $barraNavigazione; ?>
<?php echo $testataConcorso; ?>



<!---*************-->
<!---*** INTRO ***-->
<!---*************-->
<section class="hrow">
	<div class="row">
		<div class="small-12 medium-8 large-8 medium-centered large-centered column">
		<div class="hrow-boxTitolo">
				<h1 class="hrow-boxTitolo-title">LISTA PROVE</h1>
				<p class="hrow-boxTitolo-subTitle">Ciao <strong><?=$this->ion_auth->user()->row()->nome?></strong>,<br>
			ogni mese, se registri almeno uno scontrino, puoi vincere una bici UHU RECYCLE! Compra uno stic UHU RENATURE presso un punto vendita I LOVE UHU e registra lo scontrino: puoi vincere una bici UHU RECYCLE! clicca sulle prove attive, segui le istruzioni e buona fortuna!</p>
			</div>	
			
		</div>
	</div>
</section>


<!---**************************-->
<!---*** LISTA PROVE ATTIVE ***-->
<!---**************************-->
<section class="hrow">
	<div class="row">
		<div class="small-12 medium-8 large-8 medium-centered large-centered column">
			<div class="hrow-listaProve hrow-listaProve--Attive">
				<!---**************************-->
				<!---*** LISTA PROVE ATTIVE ***-->
				<!---**************************-->
				<h3 class="listaProve-header">Lista delle prove attive:</h3>
				<div id="js-prove-attive" class="listaProve-content">AL MOMENTO NON CI SONO PROVE ATTIVE DA SOSTENERE</div>
			</div>			
		</div>
	</div>
</section>


