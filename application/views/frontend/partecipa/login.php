<?php echo $barraNavigazione; ?>
<?php echo $testataConcorso; ?>

<section class="hrow">
	<div class="row">
		<div class="small-12 medium-8 large-8 medium-centered large-centered column">
		
	
	
			<!---************************-->
			<!---*** MESSAGGI SISTEMA ***-->
			<!---************************-->
			<div class="row">
				<div class="small-12 column">
						<?php if($message): ?>
							<div id="infoMessage" class="info-message">
								<?php echo $message;?>
							</div>
						<?php endif; ?>
				</div> <!--chiudo column-->
			</div> <!--chiudo row-->
	
	
			<div class="row" data-equalizer>
				
				<!---*********************-->
				<!---*** BOX ISCRIVITI ***-->
				<!---*********************-->
				<div class="small-12 medium-6 large-6 column">
					<div class="hrow-iscriviti " data-equalizer-watch>
						<h1 class="iscriviti-title">Iscriviti e Vinci!</h1>
						<p class="iscriviti-subTitle">Compra lo stic UHU renature, conserva lo scontrino, Vinci ogni mese una bici UHU RECYCLE!</p>
						<a class="button radius expand" href="<?=site_url()?>partecipa/iscriviti">Iscriviti ora!</a>
					</div>
				</div> <!--chiudo column-->
				
				<!---*****************-->
				<!---*** BOX LOGIN ***-->
				<!---*****************-->
				<div class="small-12 medium-6 large-6 column">
					<div class="hrow-login " data-equalizer-watch>
						<h3 class="iscriviti-title">Già Iscritto?</h3>
						<p class="iscriviti-subTitle">Accedi con la tua email e password e gioca!</p>
						<?php echo form_open("partecipa");?>	
							<div class="row">
								<div class="small-12 column">
									<label>Username (email):
									<?php echo form_input($identity, '', 'class="radius"');?>
									</label>
								</div> <!--chiudo column-->
							</div> <!--chiudo row-->
								
							<div class="row">
								<div class="small-12 column">
									<label>Password:
									<?php echo form_input($password, '', 'class="radius"');?>
									</label>
								</div> <!--chiudo column-->
							</div> <!--chiudo row-->
							
							<!--
							<div class="row">
								<div class="small-12 column">
									<?php echo lang('login_remember_label', 'remember');?>
									<?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
								</div> 
							</div> 
							-->
							
							<div class="row">
								<div class="small-12 column">
									<?php echo form_submit('submit', 'Accedi', 'class="button radius expand"');?>
								</div> <!--chiudo column-->
							</div> <!--chiudo row-->
							
							<div class="row">
								<div class="small-12 column text-center">
									<a class="recupera-pwd" href="<?=site_url()?>partecipa/recupera_password">Recupera la password</a>
								</div> <!--chiudo column-->
							</div> <!--chiudo row-->
							
							
						<?php echo form_close();?>
					</div> <!---*** chiudo hrow-login ***-->
				</div> <!--chiudo column-->
					
			</div> <!--chiudo row-->
	
	
		
		</div> <!--chiudo column-->
	</div> <!--chiudo row-->
</section>	

	
	
	
