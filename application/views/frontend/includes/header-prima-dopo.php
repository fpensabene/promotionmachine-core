<!doctype html>
<html class="no-js" lang="en">
	<head>
		<!---***********************-->
		<!---*** HEADER FRONTEND ***-->
		<!---***********************-->
		
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		
		<!---*** TYPEKIT - UHU RECYCLE ***-->
		<script src="//use.typekit.net/ehh7ppn.js"></script>
		<script>try{Typekit.load();}catch(e){}</script>
		
		<!---*** SEO TAGS ***-->
		<title><?echo $this->nome_concorso; ?> - <?php if(isset($titolo_pagina)) {echo $titolo_pagina;}?></title>
		
		<!---*** FOUNDATION css ***-->
		<link rel="stylesheet" href="<?echo site_url();?>assets/foundation-frontend/stylesheets/app.css" />    
		<link rel="stylesheet" href="<?echo site_url();?>assets/foundation-icons/foundation-icons.css" />    


		<!---*** JQUERY JS ***-->
		<script src="<?=site_url()?>assets/foundation-frontend/bower_components/jquery/dist/jquery.min.js"></script> 
		<script src="<?echo site_url();?>assets/foundation-frontend/bower_components/modernizr/modernizr.js"></script> 

		<!-- FOUNDATION -->
		<script src="<?echo site_url();?>assets/foundation-frontend/bower_components/foundation/js/foundation.min.js"></script>
		
		 
			
		<!---*** ANALYTICS ***-->
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
		  ga('create', 'UA-28301176-7', 'auto');
		  ga('send', 'pageview');
		</script>
		
	</head>  
  <body class="<?= $body_class ?>"> 



<section class="main-section">
<!---*** INSERIRE TUTTO IL CONTENUTO DELLA PAGINA QUI DENTRO	 ***-->