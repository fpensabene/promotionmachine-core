<!---*** BARRA DI NAVIGAZIONE ***-->
<nav  class="navigazione-desktop show-for-medium-up">
	<div class="row">
		<div class="small-12 column">
			<ul class="prmMainNav">
				<!---*** <li class="prmMainNav-navItem"><a class="prmMainNav-navItem-link" href="<?=site_url()?>">HOME</a></li> ***-->
					
				<?php if ($this->ion_auth->logged_in()): ?>	
					<li class="prmMainNav-navItem">
						<a class="prmMainNav-navItem-link <?php if ($this->uri->segment(1) == 'partecipa' or $this->uri->segment(1) == '') { echo 'active';}?>" href="<?=site_url()?>partecipa/">PAGINA PERSONALE</a>
					</li>
				<?php else: ?>	
					<li class="prmMainNav-navItem">
						<a class="prmMainNav-navItem-link <?php if ($this->uri->segment(1) == 'partecipa' or $this->uri->segment(1) == '') { echo 'active';}?>" href="<?=site_url()?>partecipa/">PARTECIPA!</a>
					</li>					
				<?php endif; ?>
						

				<li class="prmMainNav-navItem">
					<a class="prmMainNav-navItem-link <?php if ($this->uri->segment(1) == 'premi') { echo 'active';}?>" href="<?=site_url()?>premi">PREMI IN PALIO</a>
				</li> 
				<li class="prmMainNav-navItem">
					<a class="prmMainNav-navItem-link <?php if ($this->uri->segment(1) == 'assistenza') { echo 'active';}?>" href="<?=site_url()?>assistenza">ASSISTENZA</a>
				</li>  
				<li class="prmMainNav-navItem">
					<a class="prmMainNav-navItem-link" download="" href="<?=site_url().$this->dati_concorso['output']['regolamento']?>">REGOLAMENTO</a>
				</li> 
				<?php if ($this->ion_auth->logged_in()): ?>
					<li class="prmMainNav-navItem"><a class="prmMainNav-navItem-link" href="<?=site_url()?>partecipa/logout">LOGOUT</a></li>
				<?php endif; ?>
				
			</ul>	
		</div> <!--chiudo column-->
	</div> <!--chiudo row-->
</nav>