<!doctype html>
<html class="no-js" lang="en">
	<head>
		<!---***********************-->
		<!---*** HEADER FRONTEND ***-->
		<!---***********************-->
		
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		
		<!---*** TYPEKIT - UHU RECYCLE ***-->
		<script src="//use.typekit.net/ehh7ppn.js"></script>
		<script>try{Typekit.load();}catch(e){}</script>
		
		<!---*** SEO TAGS ***-->
		<title><?echo $this->nome_concorso; ?> - <?php if(isset($titolo_pagina)) {echo $titolo_pagina;}?></title>
		
		<!---*** FOUNDATION css ***-->
		<link rel="stylesheet" href="<?echo site_url();?>assets/foundation-frontend/stylesheets/app.css" />    
		<link rel="stylesheet" href="<?echo site_url();?>assets/foundation-icons/foundation-icons.css" />    

		<!---*** JQUERY CSS ***-->
		<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/cupertino/jquery-ui.css"/> 
		<link rel="stylesheet" href="http://cdn.jsdelivr.net/jquery.ui.timepicker.addon/1.4.5/jquery-ui-timepicker-addon.min.css"/> 

		<!---*** JQUERY JS ***-->
		<script src="<?=site_url()?>assets/foundation-frontend/bower_components/jquery/dist/jquery.min.js"></script> 
		<script src="<?echo site_url();?>assets/foundation-frontend/bower_components/modernizr/modernizr.js"></script> 
		<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>

		<!-- FOUNDATION -->
		<script src="<?echo site_url();?>assets/foundation-frontend/bower_components/foundation/js/foundation.min.js"></script>
		
		<!---*** TIME PICKER JS ***-->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.4.5/jquery-ui-timepicker-addon.min.js"></script>   

		<!-- jQuery Validation -->
		<script src="<?=site_url()?>assets/jquery-validation/dist/jquery.validate.min.js"></script> 
		<script src="<?=site_url()?>assets/jquery-validation/dist/custom-additional-methods.js"></script> 		
		<script src="<?=site_url()?>assets/jquery-validation/src/localization/messages_it.js"></script> 

		<!---*** ANALYTICS ***-->
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
		  ga('create', 'UA-28301176-7', 'auto');
		  ga('send', 'pageview');
		</script>
		
	</head>  
  <body class="<?= $body_class ?>"> 


<div class="off-canvas-wrap" data-offcanvas>
  <div class="inner-wrap"> 
	  
	<!---*** BARRA DI NAVIGAZIONE OFF CANVAS ***-->  
	<nav class="tab-bar show-for-small">
		<!---*** HAMBURGER ICON SX ***-->
		<section class="left-small">
			<a class="left-off-canvas-toggle menu-icon" href="#"><span></span></a>
		</section>
		
		<!---*** TITOLO CENTRALE ***-->
		<section class="middle tab-bar-section">
			<h1 class="title">UHU RECYCLE</h1>
		</section>
    </nav>
	
	<!---*** BARRA DI NAVIGAZIONE OFF-CANVAS SX ***-->
    <aside class="left-off-canvas-menu">
      <ul class="off-canvas-list">
        <li><label>CONCORSO</label></li>
        <li><a href="#">Inserire qui i links per il menu mobile</a></li>
        <li><a href="#">...</a></li>
      </ul>
    </aside>



<section class="main-section">
<!---*** INSERIRE TUTTO IL CONTENUTO DELLA PAGINA QUI DENTRO	 ***-->