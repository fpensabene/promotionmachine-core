
<?php echo $barraNavigazione; ?>
<?php echo $testataConcorso; ?>

<script src='https://www.google.com/recaptcha/api.js'></script>
<!---*************-->
<!---*** INTRO ***-->
<!---*************-->
<div class="hrow">	
	<div class="row">
		<div class="small-12 medium-8 large-8 medium-centered large-centered column">
			<div class="hrow-boxTitolo">
				<h1 class="hrow-boxTitolo-title">RICHIEDI ASSISTENZA</h1>
				<p class="hrow-boxTitolo-subTitle">Compila il form qui sotto per ricevere aiuto</p>
			</div>				
		</div>
		<div class="small-12 medium-8 large-8 medium-centered large-centered column">
			<div style="display:none" id="output" data-alert class="alert-box radius">
			  <a href="#" class="close">&times;</a>
			</div>
		</div>
	</div>
</div>
<div class="hrow">
	<form class="form-generico" action="" method="POST" id="form_assistenza" method="POST" enctype="multipart/form-data">
		<div class="row">
			<div class="small-12 medium-8 large-8 medium-centered large-centered column">
					<label for "from_address"> Il tuo indirizzo email:
						<input class="radius" id="from_address" name="from_address" type="text"/>
					</label>
			</div>
		</div>
		<div class="row">
			<div class="small-12 medium-8 large-8 medium-centered large-centered column">
					<label for "from_conferma"> Conferma indirizzo email:
						<input class="radius" id="from_conferma" name="from_conferma" type="text"/>
					</label>
			</div>
		</div>
		<div class="row">
			<div class="small-12 medium-8 large-8 medium-centered large-centered column">
					<label for "assistenza_body"> Scrivi qui il tuo messaggio:
						<textarea class="radius" rows="10" cols="20" name="assistenza_body" id="assistenza_body"></textarea>
					</label>
			</div>
		</div>
		<div class="row">
			<div class="small-12 medium-8 large-8 medium-centered large-centered column">
				<div class="margin-bottom-1">
					<?php if (isset($recaptcha_html)) echo $recaptcha_html; ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="small-12 medium-8 large-8 medium-centered large-centered column">
				<button class="radius button" id="button_go" type="button" onclick="javascript:inviaRichiestaAssistenza('<?=base_url()?>')">INVIA</button>
				<div id="spinner" class="spinner" style="display:none;">
				    <img id="img-spinner" src="/assets/img-frontend/ajax-loader.gif" alt="Loading"/>
				</div>
			</div>
		</div>
		<div class="g-recaptcha" data-sitekey="6LcT6QkTAAAAAA-tyFPjDkGaXLykrr8pqwgMmobS"></div>
	</form>
</div>

<script>
function inviaRichiestaAssistenza(base_url){
	if (!$("#form_assistenza").valid()){
		return false;
	}
	var from_address = $("#from_address").val();
	var from_conferma = $("#from_conferma").val();
	var assistenza_body = $("#assistenza_body").val();
	var risposta_captcha = $("#g-recaptcha-response").val();
	
	$.ajax({
	    type:'POST',
	    dataType: 'json',
	    beforeSend: function(){
		  $("#spinner").show();
		  $("#button_go").attr("disabled","disabled");
		  $("#button_go").addClass("disabled");
		},
		complete: function(){
		  $("#spinner").hide();
		  $("#button_go").removeAttr("disabled");
		  $("#button_go").removeClass("disabled");
		},
	    url:base_url+'assistenza/invia_richiesta',
	    data: {
		    from_address:from_address,
		    from_conferma:from_conferma,
		    assistenza_body : assistenza_body,
		    risposta_captcha: risposta_captcha
	    },
	    success:function(response){
	    //attenzione. success (questo sopra) vuol dire che la chiamata ha avuto successo, che il server ha risposto senza dare errori fatali, non vuol dire che la funzione abbia fatto quel che doveva fare
	    	if (response.result=='success'){
		        printMsg('success',response.message); 
		     	$('#form_assistenza').hide();   
	        }
	        else {
		        printMsg('alert',response.message); 
		        if (response.details == 'captcha'){
		        }
	        }
	    },
	    error:function(response){
	        printMsg('alert','Errore durante la chiamata'); 
	    }
	});	
}
$(document).ready(function(){
	$('#from_address, #from_conferma').bind('copy paste',function(e) {
    	e.preventDefault(); return false; 
	});
	$("#form_assistenza").validate({
		rules: {
			from_address: {
				required: true,
				customEmail: true
			},
			from_conferma: {
				required: true,
				customEmail: true,
				equalTo: from_address
			},
			assistenza_body: {
				required: true,
				minlength: 20
			}			
		},
		messages:{
			from_address: {
				required: 'Devi inserire la tua email'
			},
			from_conferma: {
				required: 'Devi inserire la tua email',
				equalTo: 'L\'email inserita non coincide con quella inserita nel campo "Il tuo indirizzo email".'
			},
			assistenza_body: {
				required: 'Devi inserire un messaggio.',
				minlength: 'Il messaggio che hai inserito &egrave; troppo corto.'
			}
		}
	});
});
</script>