<html>
	<body>
		<p>Ciao <?=$nome?> <?=$cognome?>,</p>
		<p>Hai richiesto di modificare la tua password per il concorso UHU ReCYCLE.</p>
		<p>Clicca sul link sottostante per impostare una nuova password:</p>
		<p><strong><a href="<?php echo site_url().'partecipa/reset_password/'. $forgotten_password_code?>">Clicca qui per cambiare la password.</a></strong></p>
		<br>
		<p>Grazie e a presto!</p>
		<p><strong>Il Team UHU</strong></p>	
	</body>
</html>

<?php
	// questo template viene richiamato dal metodo:
	// application/libraries/Ion_auth -> forgotten_password()	
?>