<html>
	<body>
		<p>Ciao  <strong><?= $nome ?> <?= $cognome ?></strong>,</p>
		<p>Hai compilato il form di iscrizione al concorso <strong>UHU RECYCLE</strong> ma non hai ancora completato l'iscrizione.</p>
		<p>Per completarla clicca il link sottostante:</p>
		<strong>INSERIRE LINK ATTIVAZIONE</strong>
		<p>Se invece non hai compilato tu il form oppure non desideri più iscriverti, elimina questa mail.</p>
		<br>
		<p>Grazie e a presto!</p>
		<p><strong>Il Team UHU</strong></p>
	</body>
</html>

<?php
	// questo template viene richiamato dal metodo:
	// application/controllers/Gestione_concorso.php -> sollecita_contestant()	
?>