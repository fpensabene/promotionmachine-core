<html>
<body>
	<h1><?php echo sprintf(lang('email_recupera_password_heading'), $identity);?></h1>
	<p><?php echo sprintf(lang('email_recupera_password_subheading'), anchor('admin/reset_password/'. $forgotten_password_code, lang('email_recupera_password_link')));?></p>
</body>
</html>