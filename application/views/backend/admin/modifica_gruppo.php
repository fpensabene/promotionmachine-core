<body class="backend-page"> 

<?php echo $top_menu; ?>

<!---*** BANDA PROMOTIONMACHINE ***-->
<section class="promotion-machine-background">
	<div class="row">
		<div class="small-12 text-center column">
		</div>
	</div>
</section>

<!---*** BREADCRUMBS ***-->
<section class="hrow">
	<div class="row">
		<div class="column">
	
			<ul class="breadcrumbs">
			  <li><a href="<?=site_url()?>admin/index">DASHBOARD</a></li>
			  <li class="unavailable"><a href="#">Admin</a></li>
			  <li><a href="<?=site_url().$this->uri->segment(1)."/".$this->uri->segment(2)?>">Nuovo Utente </a></li>
			</ul>
		
		</div> <!--chiudo column-->
	</div> <!--chiudo row-->
</section>





<section class="hrow">
	
	<!---*** TITOLO ***-->
	<div class="row">
		<div class="small-12 column">
				<h1 class="hrow-heading">Modifica Gruppo </h1>
				<p class="hrow-subHeading">Modifica un gruppo di utenti</p>
				<?php if($message): ?>
					<div id="infoMessage"><span class="alert label">Attenzione:</span><?php echo $message;?></div>
				<?php endif; ?>
		</div> <!--chiudo column-->
	</div> <!--chiudo row-->
	
	<?php echo form_open(current_url());?>
		<div class="row">
			<div class="small-12 column">
				<?php echo lang('edit_group_name_label', 'group_name');?>
	            <?php echo form_input($group_name);?>
			</div> <!--chiudo column-->
			
			<div class="small-12 column">
				<?php echo lang('edit_group_desc_label', 'description');?>
	            <?php echo form_input($group_description);?>
			</div> <!--chiudo column-->
			
			<div class="small-12 column">
				<?php echo form_submit('submit', lang('edit_group_submit_btn'), "class='button'");?>
			</div> <!--chiudo column-->
			
			
		</div> <!--chiudo row-->
	<?php echo form_close();?>
	

</section>