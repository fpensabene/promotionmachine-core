<body class="backend-page"> 
<?php echo $top_menu; ?>

<!---*** BANDA PROMOTIONMACHINE ***-->
<section class="promotion-machine-background">
	<div class="row">
		<div class="small-12 text-center column">
		</div>
	</div>
</section>

<!---*** BREADCRUMBS ***-->
<section class="hrow">
	<div class="row">
		<div class="column">
	
			<ul class="breadcrumbs">
			  <li><a href="<?=site_url()?>admin/index">DASHBOARD</a></li>
			  <li class="unavailable"><a href="#">Admin</a></li>
			  <li><a href="<?=site_url().$this->uri->segment(1)."/".$this->uri->segment(2)?>">Nuovo Utente </a></li>
			</ul>
		
		</div> <!--chiudo column-->
	</div> <!--chiudo row-->
</section>


<section class="hrow">
	
	<!---*** TITOLO ***-->
	<div class="row">
		<div class="small-12 column">
				<h1 class="hrow-heading">Modifica utente</h1>
				<p class="hrow-subHeading">Modifica i dati di un utente</p>
				<!--<div id="infoMessage"><?php echo $message;?></div>-->
				<?php if($message): ?>
					<div id="infoMessage"><span class="alert label">Attenzione:</span><?php echo $message;?></div>
				<?php endif; ?>
		</div> <!--chiudo column-->
	</div> <!--chiudo row-->
	
	<?php echo form_open(uri_string());?>
		<div class="row">
			<div class="small-12 large-6 column">
				<?php echo lang('edit_user_fname_label', 'first_name');?>
		        <?php echo form_input($first_name);?>
			</div> <!--chiudo column-->
			
			<div class="small-12 large-6 column">
				<?php echo lang('edit_user_lname_label', 'last_name');?>
		        <?php echo form_input($last_name);?>
			</div> <!--chiudo column-->
			
			<div class="small-12 large-6 column">
				<?php echo lang('edit_user_password_label', 'password');?>
		        <?php echo form_input($password);?>
			</div> <!--chiudo column-->
			
			<div class="small-12 large-6 column">
				<?php echo lang('edit_user_password_confirm_label', 'password_confirm');?>
		        <?php echo form_input($password_confirm);?>
			</div> <!--chiudo column-->
			
			<div class="small-12 column">
				<?php if ($this->ion_auth->amministrazione(2,0)): ?>
				
				  <label>Appartiene ai gruppi:</label><br>
				  
				  <?php foreach ($groups as $group):?>
				     
				      <label class="checkbox">
				      <?php
				          $gID=$group['id'];
				          $checked = null;
				          $item = null;
				          foreach($currentGroups as $grp) {
					          if ($selectedGroupsIDs) {
						          //entro in questa if solo se l'utente ha postato il form. questo array contiene gli id (del db) dei gruppi selezionati. Checko quelli che l'utente aveva precedentemente selezionato.
						          if (in_array($gID,$selectedGroupsIDs)) {
					                  $checked= ' checked="checked"';
						              break;
					              }
					          }
				              elseif ($gID == $grp->id) {
					              //altrimenti checko quelli attualmente impostati
				                  $checked= ' checked="checked"';
				              break;
				              }
				          }
				      ?>
				      <input type="checkbox" name="groups[]" value="<?php echo $group['id'];?>"<?php echo $checked;?>>
				      <?php echo htmlspecialchars($group['name'],ENT_QUOTES,'UTF-8');?>
				      </label>
				      
				  <?php endforeach ?>
				
				<?php endif ?>
				
			</div> <!--chiudo column-->
			
			
			<?php echo form_hidden('id', $user->id);?>
		      <?php echo form_hidden($csrf); ?>
			
			<div class="small-12 column">
				<?php echo form_submit('submit', lang('edit_user_submit_btn'), "class='button'");?>
			</div> <!--chiudo column-->
			
					
			
		</div> <!--chiudo row-->
	<?php echo form_close();?>
	
	
</section>


