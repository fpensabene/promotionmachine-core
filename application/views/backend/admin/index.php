<body class="backend-page"> 

<?php echo $top_menu; ?>

<!---*** BANDA PROMOTIONMACHINE ***-->
<section class="promotion-machine-background">
	<div class="row">
		<div class="small-12 text-center column">
		</div>
	</div>
</section>

<!---*** BREADCRUMBS ***-->
<section class="hrow">
	<div class="row">
		<div class="column">
	
			<ul class="breadcrumbs">
			  <li><a href="<?=site_url()?>admin/index">DASHBOARD</a></li>
			  <li class="unavailable"><a href="#">Admin</a></li>
			  <li><a href="<?=site_url().$this->uri->segment(1)."/".$this->uri->segment(2)?>">Gestione Utenti</a></li>
			</ul>
		
		</div> <!--chiudo column-->
	</div> <!--chiudo row-->
</section>

<!---*** MESSAGGI DEL SISTEMA ***-->
<?php if($message): ?>
<section class="hrow">
	<div class="row">
		<div class="small-12 column">
			<div id="infoMessage"><span class="alert label">Attenzione:</span><?php echo $message;?></div>
		</div> <!--chiudo column-->
	</div> <!--chiudo row-->
</section>
<?php endif; ?>

<section class="hrow">
	
	
	<!---*** TITOLO ***-->
	<div class="row">
		<div class="small-12 column">
				<h1 class="hrow-heading">Dashboard</h1>
				<p class="hrow-subHeading">Ecco una lista di tutte le funzioni disponibili nella piattaforma con una breve descrizione:</p>
				
		</div> <!--chiudo column-->
	</div> <!--chiudo row-->



	
	<div class="row" data-equalizer>
		
		<div class="small-12 medium-4 large-4 column end">
			<div class="hrow-box" data-equalizer-watch>
				<div class="hrow-box-headerbox"><h2 class="hrow-box-heading">Admin</h2></div>
				<ul class="side-nav">
					<li>
						<a href="<?=site_url()?>admin/gestione_utenti_backend">Gestione utenti Backend →</a>
						<p class="descrizione-funzionalita">Lista e gestione degli utenti che possono accedere al backend di promotionmachine per gestire i concorsi</p>
					</li>
					<li class="divider"></li>
		          	<li>
		          		<a href="<?=site_url()?>admin/nuovo_utente_backend">Nuovo utente Backend →</a>
		          		<p class="descrizione-funzionalita">Crea un nuovo utente nel gruppo admin oppure per il cliente che vuole visualizzare l'andamento del concorso</p>
		          	</li>
		          	<li class="divider"></li>
		          	<li>
		          		<a href="<?=site_url()?>admin/gestione_gruppi">Gestione gruppi →</a>
		          		<p class="descrizione-funzionalita">Lista e gestione dei gruppi che hanno accesso al backend di promotionmachine</p>
		          	</li>
		          	<li class="divider"></li>
		          	<li>
		          		<a href="<?=site_url()?>admin/crea_gruppo">Nuovo gruppo →</a>
		          		<p class="descrizione-funzionalita">Crea un nuovo gruppo di utenti che possono accedere al backend di Promotionmachine</p>
		          	</li>
				</ul>
			</div>
		</div> <!--chiudo column-->
		
		<div class="small-12 medium-4 large-4  column end">
			<div class="hrow-box" data-equalizer-watch>
				<div class="hrow-box-headerbox"><h2 class="hrow-box-heading">Impostazione concorso</h2></div>
				<ul class="side-nav">
					<li>
						<a href="<?echo site_url();?>concorso">Concorso →</a>
						<p class="descrizione-funzionalita">Impostazioni generali del concorso (data inizio e fine, nome ecc. ecc.)</p>
						</li>
					<li class="divider"></li>
			  		<li>
			  			<a href="<?echo site_url();?>impostazione_periodi_e_concorsi">Periodi & prove →</a>
			  			<p class="descrizione-funzionalita">Impostazione dei periodi del concorso e delle prove ad essi associate</p>
			  		</li>
			  		<li class="divider"></li>
			  		<li>
			  			<a href="<?echo site_url();?>tipo_prova">Tipi di prova →</a>
			  			<p class="descrizione-funzionalita">Gestione dei tipi di prove e (per ora) assegnazione del punteggio al tipo di prova</p>
			  		</li>
				</ul>
			</div>
		</div> <!--chiudo column-->
		
		<div class="small-12 medium-4 large-4  column end">
			<div class="hrow-box" data-equalizer-watch>
				<div class="hrow-box-headerbox"><h2 class="hrow-box-heading">Gestione Concorso</h2></div>
				<ul class="side-nav">
					<li>
						<a href="<?=site_url()?>gestione_concorso/periodi_e_prove">Periodi & prove →</a>
						<p class="descrizione-funzionalita">Naviga tra i periodi del concorso e visualizza le prove effettuate dai partecipanti (contestants)</p>
					</li>
					<li class="divider"></li>
				    <li>
				    	<a href="<?=site_url()?>gestione_concorso/gestione_partecipanti">Gestione partecipanti →</a>
						<p class="descrizione-funzionalita">Visualizza la lista dei partecipanti al concorso, controlla quanti si sono attivati modificane i dati.</p>
				    </li>
				</ul>
			</div>
		</div> <!--chiudo column-->
		
		<div class="small-12 medium-4 large-4 column end">
			<div class="hrow-box" data-equalizer-watch>
				<div class="hrow-box-headerbox"><h2 class="hrow-box-heading">Estrazioni</h2></div>
				<ul class="side-nav">
					<li>
						<a href="<?=site_url()?>">Estrazione →</a>
						<p class="descrizione-funzionalita"></p>
					</li>
					<li class="divider"></li>
		          	
				</ul>
			</div>
		</div> <!--chiudo column-->
		
		

		
		
	</div> <!--chiudo row-->
	
	
	


</section>