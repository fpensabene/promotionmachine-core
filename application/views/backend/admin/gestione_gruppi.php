<body class="backend-page"> 

<?php echo $top_menu; ?>

<!---*** BANDA PROMOTIONMACHINE ***-->
<section class="promotion-machine-background">
	<div class="row">
		<div class="small-12 text-center column">
		</div>
	</div>
</section>

<!---*** BREADCRUMBS ***-->
<section class="hrow">
	<div class="row">
		<div class="column">
	
			<ul class="breadcrumbs">
			  <li><a href="<?=site_url()?>admin/index">DASHBOARD</a></li>
			  <li class="unavailable"><a href="#">Admin</a></li>
			  <li><a href="<?=site_url().$this->uri->segment(1)."/".$this->uri->segment(2)?>">Gestione Gruppi</a></li>
			</ul>
		
		</div> <!--chiudo column-->
	</div> <!--chiudo row-->
</section>


<!---*** CANCELLARE!!!!! ***-->

<div class="row hrow">
	<div class="column">
		
		<div class="panel callout  radius">
		  <h5><span class="alert label">Attenzione:</span> PROBLEMI da risolvere</h5>
		  <ul>
		  <li>Attenzione perchè sui nomi dei gruppi ci facciamo della logica - facciamo che admin, moderator_group, memebers non possono essere modificati</li>
		 

		  
		  </ul>
		</div>
	</div> <!--chiudo column-->
</div> <!--chiudo row-->


<!---*** CANCELLARE!!!!! ***-->


<!---*** CONTENUTO DELLA PAGINA ***-->
<section class="hrow">
	<div class="row">
		
		<!--TITOLO DELLA PAGINA-->
		<div class="small-12 column">
			<h1 class="hrow-heading">Gestione Gruppi</h1>
			<p class="hrow-subHeading">Gestione Gruppi Utenti del Backend</p>
			
			
			<?php if($message): ?>
					<div id="infoMessage"></div>
					<div data-alert class="alert-box">
					  	<span class="alert label">Alert:</span>
					  	<?php echo $message;?>
					  	<a href="#" class="close">&times;</a>
					</div>
			<?php endif; ?>
			
		</div>
		
		<!--
		<div class="small-12 column">
			<?php echo anchor('admin/iscriviti', lang('index_iscriviti_link'), "class='button tiny'")?>
			<?php echo anchor('admin/create_group', lang('index_create_group_link'), "class='button tiny'")?></p>
		</div>
		-->
		
		<div class="small-12 column">
			<table class="hrow-table">
				<thead>
					<tr>
						<th>Id</th>
						<th><?php echo lang('list_groups_name_label');?></th>
						<th><?php echo lang('list_groups_desc_label');?></th>
						<th><?php echo lang('index_action_th');?></th>
					</tr>
				</thead>
				<tbody>
				<?php foreach ($gruppi as $gruppo):?>
					<tr>
			            <td><?php echo htmlspecialchars($gruppo->id,ENT_QUOTES,'UTF-8');?></td>
			            <td><?php echo htmlspecialchars($gruppo->name,ENT_QUOTES,'UTF-8');?></td>
			            <td><?php echo htmlspecialchars($gruppo->description,ENT_QUOTES,'UTF-8');?></td>
						<td><?php echo anchor("admin/modifica_gruppo/".$gruppo->id, 'Edit') ;?></td>
						<!--<td><?php echo anchor("admin/delete_group/".$gruppo->id, 'Delete') ;?></td>-->
						
					</tr>
				<?php endforeach;?>
				</tbody>
			</table>
		</div> <!--chiudo column-->
		
	</div> <!--chiudo row-->
</section>
