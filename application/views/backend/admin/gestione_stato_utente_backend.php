<body class="backend-page"> 

<?php echo $top_menu; ?>

<!---*** BANDA PROMOTIONMACHINE ***-->
<section class="promotion-machine-background">
	<div class="row">
		<div class="small-12 text-center column">
		</div>
	</div>
</section>

<!---*** BREADCRUMBS ***-->
<section class="hrow">
	<div class="row">
		<div class="column">
	
			<ul class="breadcrumbs">
			  <li><a href="<?=site_url()?>admin/index">DASHBOARD</a></li>
			  <li class="unavailable"><a href="#">Admin</a></li>
			  <li><a href="<?=site_url().$this->uri->segment(1)."/".$this->uri->segment(2)?>">Nuovo Utente </a></li>
			</ul>
		
		</div> <!--chiudo column-->
	</div> <!--chiudo row-->
</section>


<!---*** CANCELLARE!!!!! ***-->


<section class="hrow">
	
	<!---*** TITOLO ***-->
	<div class="row">
		<div class="small-12 column">
				<h1 class="hrow-heading">Modifica stato di un utente del backend </h1>
				<p class="hrow-subHeading">Seleziona lo stato dell'utente <strong>'<?php  echo $user->username ?>'</strong></p>
				<?php if($message): ?>
					<div id="infoMessage"><span class="alert label">Attenzione:</span><?php echo $message;?></div>
				<?php endif; ?>
		</div> <!--chiudo column-->
	</div> <!--chiudo row-->
	
	<?php echo form_open("admin/gestione_stato_utente_backend/".$user->id);?>
		<div class="row">
			<div class="small-12 medium-6 large-6 column">
				<?php echo lang('gestione_stato_utente_backend_attivo_label', 'confirm');?>
				<input type="radio" name="status" value="attivo" <? if($user->active) echo "checked='checked'";?>  />
			</div> <!--chiudo column-->
			
			<div class="small-12 medium-6 large-6 column">
				<?php echo lang('gestione_stato_utente_backend_disattivo_label', 'confirm');?>
				<input type="radio" name="status" value="disattivo" <? if(!$user->active) echo "checked='checked'";?>  />
			</div> <!--chiudo column-->
			
			<?php echo form_hidden($csrf); ?>
			<?php echo form_hidden(array('id'=>$user->id)); ?>
				
			<div class="small-12 column">
				<?php echo form_submit('submit', lang('gestione_stato_utente_backend_attivo_btn'), "class='button'");?>
			</div> <!--chiudo column-->
			
			
		</div> <!--chiudo row-->
	<?php echo form_close();?>
	
</section>

<!--
vecchia versione:

<div class="row">
	<h1><?php echo lang('deactivate_heading');?></h1>
	<p><?php echo sprintf(lang('deactivate_subheading'), $user->username);?></p>
	
	<?php echo form_open("admin/deactivate/".$user->id);?>
	
	  <p>
	  	<?php echo lang('deactivate_confirm_y_label', 'confirm');?>
	    <input type="radio" name="confirm" value="yes" checked="checked" />
	    <?php echo lang('deactivate_confirm_n_label', 'confirm');?>
	    <input type="radio" name="confirm" value="no" />
	  </p>
	
	  <?php echo form_hidden($csrf); ?>
	  <?php echo form_hidden(array('id'=>$user->id)); ?>
	
	  <p><?php echo form_submit('submit', lang('deactivate_submit_btn'));?></p>
	
	<?php echo form_close();?>
</div>
-->