<body class="login-page"> 

<?php echo $top_menu; ?>



<section class="hrow hrow--login">
	<?php echo form_open("admin/login");?>
	<div class="row">
		
		<div class="small-12 medium-8 large-4 small-centered medium-centered column">
			<img src="<?=site_url()?>assets/img-backend/promotion-machine-login.png" alt="benvenuto in promotionmachine by oltremedia">
		</div>
		
		<div class="small-12 medium-8 large-4 small-centered medium-centered column">
			<div class="login-box">
				<div class="row">

					<div class="small-12 column">
						<h1><?php echo lang('login_heading');?></h1>
						<!---*** <p><?php echo lang('login_subheading');?></p> ***-->
						
						<?php if($message): ?>
							<div id="infoMessage"><span class="alert label">Attenzione:</span><?php echo $message;?></div>
						<?php endif; ?>
						
						
					</div>
					
					<div class="small-12 column">
						<?php echo lang('login_identity_label', 'identity');?>
						<?php echo form_input($identity);?>
					</div> <!--chiudo column-->
					
					<div class="small-12 column">
						<?php echo lang('login_password_label', 'password');?>
						<?php echo form_input($password);?>
					</div>
					
					<div class="small-12 column">
						<?php echo lang('login_remember_label', 'remember');?>
						<?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
					</div>
					
					<div class="small-12 column">
						<?php $attributes = array('class' => 'email'); ?>
						<?php echo form_submit('submit', lang('login_submit_btn'), "class='button expand'");?>
					</div>
					
					<div class="small-12 column">
						<a href="recupera_password"><?php echo lang('login_recupera_password');?>
					</div>
					<?
						/*
					<div class="small-12 column">
						<!---******************************************************-->
						<!---*** ATTENZIONE QUESTO NON VA IN PRODUZIONE !!!!!!! ***-->
						<!---******************************************************-->
						<a href="nuovo_utente_backend"><?php echo lang('iscriviti_heading');?> 
					</div>	
					*/		
					?>	
				
				
				</div>
			</div>
		</div>
		
		

		
		
	</div> <!--chiudo row-->
	<?php echo form_close();?>
	
</section>


<!---*** FINE CONTENUTO DELLA PAGINA ***-->