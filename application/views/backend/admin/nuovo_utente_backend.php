<body class="backend-page"> 

<?php echo $top_menu; ?>

<!---*** BANDA PROMOTIONMACHINE ***-->
<section class="promotion-machine-background">
	<div class="row">
		<div class="small-12 text-center column">
		</div>
	</div>
</section>

<!---*** BREADCRUMBS ***-->
<section class="hrow">
	<div class="row">
		<div class="column">
	
			<ul class="breadcrumbs">
			  <li><a href="<?=site_url()?>admin/index">DASHBOARD</a></li>
			  <li class="unavailable"><a href="#">Admin</a></li>
			  <li><a href="<?=site_url().$this->uri->segment(1)."/".$this->uri->segment(2)?>">Nuovo Utente </a></li>
			</ul>
		
		</div> <!--chiudo column-->
	</div> <!--chiudo row-->
</section>



<!---*** CANCELLARE!!!!! ***-->

<div class="row hrow">
	<div class="column">
		
		<div class="panel callout  radius">
		  <h5><span class="alert label">Attenzione:</span> PROBLEMI da risolvere</h5>
		  <ul>
			  <li>Validazione della conferma email -> il messaggio è sbagliato</li>
			  <li>Qui inviare la mail è inutile!!!!</li>
			<li><strike>Se esito positivo -> vai alla lista "gestione utenti"</strike></li>
			<li><strike>Se esito negativo -> ritorna su questo form - stampa gli errori - mantieni i dati inseriti nel form</strike></li>	  
			<li>dovrebbe poi essere possibile attivarlo dalla pagina "gestione utenti"</li>
		
		  <li>ATTENZIONE: questo è anche la view che viene servita a chi non è loggato!!!! non va bene!!!</li>
		  
		  </ul>
		</div>
	</div> <!--chiudo column-->
</div> <!--chiudo row-->


<!---*** CANCELLARE!!!!! ***-->


<section class="hrow">
	
	
	<!---*** TITOLO ***-->
	<div class="row">
		<div class="small-12 column">
				<h1 class="hrow-heading">Nuovo utente</h1>
				<p class="hrow-subHeading">Crea un nuovo utente che può accedere al backend di Promotion machine</p>
				<!--<div id="infoMessage"><?php echo $message;?></div>-->
				<?php if($message): ?>
					<div id="infoMessage"><span class="alert label">Attenzione:</span><?php echo $message;?></div>
				<?php endif; ?>
		</div> <!--chiudo column-->
	</div> <!--chiudo row-->
	

	
	
	<?php echo form_open("admin/nuovo_utente_backend");?>
		<div class="row">
			<?php echo form_input($concorsoID);?>
			<div class="small-12 medium-6 large-6 column">
				<?php echo lang('iscriviti_fname_label', 'first_name');?>
		        <?php echo form_input($first_name);?>
			</div> <!--chiudo column-->
			<div class="small-12 medium-6 large-6 column">
			 	<?php echo lang('iscriviti_lname_label', 'last_name');?>
		        <?php echo form_input($last_name);?>
			</div> <!--chiudo column-->
			<div class="small-12 medium-6 large-6 column">
				<?php echo lang('iscriviti_email_label', 'email');?>
		        <?php echo form_input($email);?>
			</div> <!--chiudo column-->
			<div class="small-12 medium-6 large-6 column">
				<?php echo lang('iscriviti_email_confirm_label', 'email');?>
		        <?php echo form_input($email_confirm);?>
			</div> <!--chiudo column-->
			<div class="small-12 medium-6 large-6 column">
			<?php echo lang('iscriviti_password_label', 'password');?>
		        <?php echo form_input($password);?>
			</div> <!--chiudo column-->
			<div class="small-12 medium-6 large-6 column">
				<?php echo lang('iscriviti_password_confirm_label', 'password_confirm');?>
		        <?php echo form_input($password_confirm);?>
			</div> <!--chiudo column-->
			<div class="small-12 column">
				<?php if ($this->ion_auth->amministrazione(2,0)): ?>
				
				  <h3><?php echo lang('edit_user_groups_heading');?></h3>
				  <?php foreach ($groups as $group):?>
				      <label class="checkbox">
				      <?php
				          $gID=$group['id'];
				          $checked = null;
				          $item = null;
				          foreach($currentGroups as $grp) {
				              if ($gID == $grp) {
				                  $checked= ' checked="checked"';
				              break;
				              }
				          }
				      ?>
				      <input type="checkbox" name="groups[]" value="<?php echo $group['id'];?>"<?php echo $checked;?>>
				      <?php echo htmlspecialchars($group['name'],ENT_QUOTES,'UTF-8');?>
				      </label>
				  <?php endforeach?>
				
				<?php endif ?>
			</div> <!--chiudo column-->
			<div class="small-12 column">
				<?php echo form_submit('submit', lang('iscriviti_submit_btn'), "class='button'");?>
			</div> <!--chiudo column-->
		</div> <!--chiudo row-->
	<?php echo form_close();?>
	
</section>