<body class="backend-page"> 

<?php echo $top_menu; ?>

<!---*** BANDA PROMOTIONMACHINE ***-->
<section class="promotion-machine-background">
	<div class="row">
		<div class="small-12 text-center column">
		</div>
	</div>
</section>

<!---*** BREADCRUMBS ***-->
<section class="hrow">
	<div class="row">
		<div class="column">
	
			<ul class="breadcrumbs">
			  <li><a href="<?=site_url()?>admin/index">DASHBOARD</a></li>
			  <li class="unavailable"><a href="#">Admin</a></li>
			  <li><a href="<?=site_url().$this->uri->segment(1)."/".$this->uri->segment(2)?>">Nuovo Gruppo </a></li>
			</ul>
		
		</div> <!--chiudo column-->
	</div> <!--chiudo row-->
</section>

<!---*** CONTENUTO DELLA PAGINA ***-->
<section class="hrow">
	<div class="row">
		
		<!--TITOLO DELLA PAGINA-->
		<div class="small-12 column">
			<h1 class="hrow-heading">Nuovo gruppo</h1>
			<p class="hrow-subHeading">Crea un nuovo gruppo di utenti</p>
			<div id="infoMessage"><?php echo $message;?></div>
		</div>
		
		<div class="small-12 column">
			<?php echo form_open("admin/crea_gruppo");?>
				<div class="row">
					<div class="small-12 column">
						<?php echo lang('create_group_name_label', 'group_name');?>
			            <?php echo form_input($group_name);?>
					</div> <!--chiudo column-->
					<div class="small-12 column">
						<?php echo lang('create_group_desc_label', 'description');?>
			            <?php echo form_input($description);?>
					</div> <!--chiudo column-->
					<div class="small-12 column">
						<?php echo form_submit('submit', lang('create_group_submit_btn'), "class='button'");?>
					</div> <!--chiudo column-->
				</div> <!--chiudo row-->
			<?php echo form_close();?>
		</div>
		
	</div>
</section>