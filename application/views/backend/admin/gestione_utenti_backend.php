<body class="backend-page"> 

<?php echo $top_menu; ?>

<!---*** BANDA PROMOTIONMACHINE ***-->
<section class="promotion-machine-background">
	<div class="row">
		<div class="small-12 text-center column">
		</div>
	</div>
</section>

<!---*** BREADCRUMBS ***-->
<section class="hrow">
	<div class="row">
		<div class="column">
	
			<ul class="breadcrumbs">
			  <li><a href="<?=site_url()?>admin/index">DASHBOARD</a></li>
			  <li class="unavailable"><a href="#">Admin</a></li>
			  <li><a href="<?=site_url().$this->uri->segment(1)."/".$this->uri->segment(2)?>">Gestione Utenti</a></li>
			</ul>
		
		</div> <!--chiudo column-->
	</div> <!--chiudo row-->
</section>


<!---*** CANCELLARE!!!!! ***-->

<div class="row hrow">
	<div class="column">
		
		<div class="panel callout  radius">
		  <h5><span class="alert label">Attenzione:</span> PROBLEMI da risolvere</h5>
		  <ul>
		  <li>Rinominiamo il metodo e la view in gestione_utenti_backend</li>
		  <li>Aggiungere cancella utente! Prevedere un meccansimo per non far cancellare l'utente admin - non mi posso cancellare da solo</li>
		  <li>Prevediamo un modo per evidenziare la riga del proprio account</li>
			  
		  <li>Sistemare la questione attivazione/deattivazione utente: la funzione la chiamerei <strong>gestione_stato_utente_backend()</strong> idem la view che carico</li>
		  <li></li>


		  
		  </ul>
		</div>
	</div> <!--chiudo column-->
</div> <!--chiudo row-->


<!---*** CANCELLARE!!!!! ***-->



<!---*** CONTENUTO DELLA PAGINA ***-->
<section class="hrow">
	<div class="row">
		
		<!--TITOLO DELLA PAGINA-->
		<div class="small-12 column">
			<h1 class="hrow-heading">Gestione Utenti backend</h1>
			<p class="hrow-subHeading">Gestione utenti che hanno accesso al backend</p>
			
			
			<?php if($message): ?>
					<div id="infoMessage"></div>
					<div data-alert class="alert-box">
					  	<span class="alert label">Alert:</span>
					  	<?php echo $message;?>
					  	<a href="#" class="close">&times;</a>
					</div>
			<?php endif; ?>
			
		</div>
		
		<!--
		<div class="small-12 column">
			<?php echo anchor('admin/iscriviti', lang('index_iscriviti_link'), "class='button tiny'")?>
			<?php echo anchor('admin/create_group', lang('index_create_group_link'), "class='button tiny'")?></p>
		</div>
		-->
		
		<div class="small-12 column">
			<table class="hrow-table">
				<thead>
					<tr>
						<th><?php echo lang('index_fname_th');?></th>
						<th><?php echo lang('index_lname_th');?></th>
						<th><?php echo lang('index_email_th');?></th>
						<th><?php echo lang('index_groups_th');?></th>
						<th><?php echo lang('index_status_th');?></th>
						<th colspan="2"><?php echo lang('index_action_th');?></th>
					</tr>
				</thead>
				<tbody>
				<?php foreach ($users as $user):?>
					<tr<?if ($this->ion_auth->user()->row()->id ==$user->id) echo " class='myaccount'";?>>
			            <td><?php echo htmlspecialchars($user->nome,ENT_QUOTES,'UTF-8');?></td>
			            <td><?php echo htmlspecialchars($user->cognome,ENT_QUOTES,'UTF-8');?></td>
			            <td><?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?></td>
						<td>
							<?php foreach ($user->groups as $group):?>
								<?=$group->name?><br />
			                <?php endforeach?>
						</td>
						<td><?php echo ($user->active) ? anchor("admin/gestione_stato_utente_backend/".$user->id, lang('index_active_link')) : anchor("admin/gestione_stato_utente_backend/". $user->id, lang('index_inactive_link'));?></td>
						<td><?php echo anchor("admin/modifica_utente_backend/".$user->id, 'Edit') ;?></td>
						<td><?php if ($this->ion_auth->user()->row()->id !=$user->id) echo anchor("admin/cancellazione_utente_backend/".$user->id, 'Delete') ;?></td>
						
					</tr>
				<?php endforeach;?>
				</tbody>
			</table>
		</div> <!--chiudo column-->
		
	</div> <!--chiudo row-->
</section>
