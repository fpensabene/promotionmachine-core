<!doctype html>
<html class="no-js" lang="en">
  <head>
	  
	<!---**********************-->
	<!---*** HEADER BACKEND ***-->
	<!---**********************-->
	
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title><?echo $this->nome_concorso; ?> - <?php if(isset($titolo_pagina)) {echo $titolo_pagina;}?></title>
    
    <!---*** foundation css ***-->
    <link rel="stylesheet" href="<?echo site_url();?>assets/foundation/stylesheets/app.css" />    
    <link rel="stylesheet" href="<?echo site_url();?>assets/foundation-icons/foundation-icons.css" /> 
    
    <!---*** footable css ***-->  
    <link href="<?=site_url()?>assets/footable/css/footable.core.css" rel="stylesheet" type="text/css" />
    <link href="<?=site_url()?>assets/footable/css/footable.metro.css" rel="stylesheet" type="text/css" />

	<!---*** Jquery css ***-->
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/cupertino/jquery-ui.css"/> 

	<!--*** Datetimepicker css ***-->
	<link rel="stylesheet" href="http://cdn.jsdelivr.net/jquery.ui.timepicker.addon/1.4.5/jquery-ui-timepicker-addon.min.css"/>
	
	<!---*** Jquery ***-->
	<script src="<?=site_url()?>assets/foundation/bower_components/jquery/dist/jquery.min.js"></script>
	<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <script src="<?=site_url()?>assets/jquery-validation/dist/jquery.validate.min.js"></script> 
	<!-- manca la lingua -->
	<!---*** Foundation JS ***-->
	<script src="<?echo site_url();?>assets/foundation/bower_components/modernizr/modernizr.js"></script> 
    <script src="<?echo site_url();?>assets/foundation/bower_components/foundation/js/foundation.min.js"></script>

    <!---*** footable JS ***-->  
    <script src="<?echo site_url();?>assets/footable/js/footable.js"></script>
    <script src="<?echo site_url();?>assets/footable/js/footable.sort.js"></script>    
    <script src="<?echo site_url();?>assets/footable/js/footable.filter.js"></script>
    
    <!--*** Datetimepicker JS ***--> 
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.4.5/jquery-ui-timepicker-addon.min.js"></script>   
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.4.5/i18n/jquery-ui-timepicker-it.js"></script> 
    
    <style>
	    .respinto {background-color: gray !important}
	    .spinner {
		    position: relative;
		    margin-left: 135px; /* half width of the spinner gif */
		    margin-top: 10px; /* half height of the spinner gif */
		    text-align:center;
		    z-index:1234;
		    overflow: auto;
		    width: 100px; /* width of the spinner gif */
		    height: 102px; /*hight of the spinner gif +2px to fix IE8 issue */
		}
		.spinner_sollecito {
		    height: 35px;
		    margin-left: 141px;
		    margin-top: 6px;
		    overflow: auto;
		    position: relative;
		    text-align: center;
		    width: 35px;
		    z-index: 1234;
		}
		#button_go {
			float: left
		}
		.sollecito {
			float: left
		}
	</style>
    
  </head>  
  



   