		<!---***************-->
		<!---*** TOP BAR ***-->
		<!---***************-->
		<?php if($this->ion_auth->logged_in()):?>
			
				<nav class="top-bar" data-topbar role="navigation">
				  <ul class="title-area">
				    <li class="name">
				      <h1><a href="<?=site_url()?>admin/index">PromotionMachine</a></h1>
				    </li>
				     <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
				    <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
				  </ul>
				
				  <section class="top-bar-section">
		
				    <!-- Left Nav Section -->
				    <ul class="left">
				      <li class="has-dropdown">
				        <a href="#">Admin</a>
				        <ul class="dropdown">
				          <?php if($this->ion_auth->amministrazione(2,0)):?>
				          	<li><a href="<?=site_url()?>admin/gestione_utenti_backend">Gestione utenti Backend</a></li>
				          	<li><a href="<?=site_url()?>admin/nuovo_utente_backend">Nuovo utente Backend</a></li>
				          	<li><a href="<?=site_url()?>admin/gestione_gruppi">Gestione gruppi</a></li>
				          	<li><a href="<?=site_url()?>admin/crea_gruppo">Nuovo gruppo</a></li>
				          <?php endif; ?>
				        </ul>
				      </li>
				      <li class="has-dropdown">
				        <a href="#">Impostazione Concorso</a>
				        <ul class="dropdown">
				          <?php if($this->ion_auth->amministrazione(2,0)):?>
				          		<li><a href="<?echo site_url();?>concorso">Concorso</a></li>
						  		<li><a href="<?echo site_url();?>impostazione_periodi_e_concorsi">Periodi & prove</a></li>
						  		<li><a href="<?echo site_url();?>tipo_prova">Tipi di prova</a></li>
						  <?php endif; ?>
				        </ul>
				      </li>
				      <li class="has-dropdown">
				        <a href="#">Gestione Concorso</a>
				        <ul class="dropdown">
				          <?php if($this->ion_auth->amministrazione(2,2)):?>
				          		<li><a href="<?=site_url()?>gestione_concorso/periodi_e_prove">Periodi & prove</a></li>
				          		<li><a href="<?=site_url()?>gestione_concorso/gestione_partecipanti">Gestione partecipanti</a></li>
				          <?php endif; ?>
				        </ul>
				      </li>
				       <li class="has-dropdown">
				        <a href="#">Estrazioni</a>
				        <ul class="dropdown">
				          <?php if($this->ion_auth->amministrazione(2,2)):?>
				          		<li><a href="<?=site_url()?>estrazioni/index">Lista estrazioni</a></li>
				          <?php endif; ?>
				        </ul>
				      </li>
				      
				    </ul>
				    
				    <!-- Right Nav Section -->
				    <ul class="right">
					   <li><a href="#">Benvenuto, <?=$this->ion_auth->user()->row()->nome?></a></li>
					   <li><a href="<?=site_url()?>admin/logout">Logout</a></li>
				    </ul>
				    
				  </section>
				</nav>
			
		<?php endif; ?>
		<!---*** TOP BAR ***-->
		
		
	
	    <section class="main-section">