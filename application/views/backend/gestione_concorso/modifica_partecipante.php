<body class="backend-page"> 
<?php echo $top_menu; ?>

<!---*** BANDA PROMOTIONMACHINE ***-->
<section class="promotion-machine-background">
	<div class="row">
		<div class="small-12 text-center column">
		</div>
	</div>
</section>

<!---*** BREADCRUMBS ***-->
<section class="hrow">
	<div class="row">
		<div class="column">
	
			<ul class="breadcrumbs">
			  <li><a href="<?=site_url()?>admin/index">DASHBOARD</a></li>
			  <li class="unavailable"><a href="#">Admin</a></li>
			  <li><a href="<?=site_url().$this->uri->segment(1)."/".$this->uri->segment(2)?>">Nuovo Utente </a></li>
			</ul>
		
		</div> <!--chiudo column-->
	</div> <!--chiudo row-->
</section>
<section class="hrow">

	<div class="row">
		<h1><?php echo lang('edit_user_heading');?></h1>
		<p><?php echo lang('edit_user_subheading');?></p>
		
		<div id="infoMessage"><?php echo $message;?></div>
		
		<?php echo form_open(uri_string());?>
			<div class="row">
				<div class="small-12 large-6 column">
					<?php echo lang('edit_user_fname_label', 'first_name');?>
			        <?php echo form_input($first_name);?>
				</div> <!--chiudo column-->
				
				<div class="small-12 large-6 column">
					<?php echo lang('edit_user_lname_label', 'last_name');?>
			        <?php echo form_input($last_name);?>
				</div> <!--chiudo column-->
				
				<div class="small-12 large-6 column">
					<?php echo lang('edit_user_password_label', 'password');?>
			        <?php echo form_input($password);?>
				</div> <!--chiudo column-->
				
				<div class="small-12 large-6 column">
					<?php echo lang('edit_user_password_confirm_label', 'password_confirm');?>
			        <?php echo form_input($password_confirm);?>
				</div> <!--chiudo column-->
				
				<div class="small-12 large-12 column">
					<?php echo lang('edit_codice_fiscale_label', 'codice_fiscale');?>
			        <?php echo form_input($codice_fiscale);?>
				</div> <!--chiudo column-->
				
				<div class="small-12 large-12 column">
					<?php echo lang('edit_data_nascita_label', 'data_nascita');?>
			        <?php echo form_input($data_nascita);?>
				</div> <!--chiudo column-->
				
				<div class="small-12 large-4 column">
				    <label>
				    	<input type="checkbox" disabled="disabled" value="<?if(isset($informativa_a["value"])) {echo $informativa_a["value"];}?>" <?if(isset($informativa_a["value"])&&$informativa_a["value"]) echo "checked"?>>
				    	Informativa A accettata
				    </label>
				</div> <!--chiudo column-->
				<div class="small-12 large-4 column">     
				    <label>
				    	<input type="checkbox" disabled="disabled" value="<?if(isset($informativa_b["value"])) {echo $informativa_b["value"];}?>" <?if(isset($informativa_b["value"])&&$informativa_b["value"]) echo "checked"?>>
				    	Informativa B accettata
				    </label>
				</div> <!--chiudo column-->
				<div class="small-12 large-4 column">     
				    <label>
				    	<input type="checkbox" disabled="disabled" value="<?if(isset($informativa_c["value"])) {echo $informativa_c["value"];}?>" <?if(isset($informativa_c["value"])&&$informativa_c["value"]) echo "checked"?>>
				    	Informativa C accettata
				    </label>
				</div> <!--chiudo column-->
				
				<div class="small-12 column">
					<?php if ($this->ion_auth->amministrazione(2,0)): ?>
					
					  <label>Appartiene ai gruppi:</label><br>
					  
					  <?php foreach ($groups as $group):?>
					     
					      <label class="checkbox">
					      <?php
					          $gID=$group['id'];
					          $checked = null;
					          $item = null;
					          foreach($currentGroups as $grp) {
						          if ($selectedGroupsIDs) {
							          //entro in questa if solo se l'utente ha postato il form. questo array contiene gli id (del db) dei gruppi selezionati. Checko quelli che l'utente aveva precedentemente selezionato.
							          if (in_array($gID,$selectedGroupsIDs)) {
						                  $checked= ' checked="checked"';
							              break;
						              }
						          }
					              elseif ($gID == $grp->id) {
						              //altrimenti checko quelli attualmente impostati
					                  $checked= ' checked="checked"';
					              break;
					              }
					          }
					      ?>
					      <input type="checkbox" name="groups[]" value="<?php echo $group['id'];?>"<?php echo $checked;?>>
					      <?php echo htmlspecialchars($group['name'],ENT_QUOTES,'UTF-8');?>
					      </label>
					      
					  <?php endforeach ?>
					
					<?php endif ?>
					
				</div> <!--chiudo column-->
				
				
				<?php echo form_hidden('id', $user->id);?>
			      <?php echo form_hidden($csrf); ?>
				
				<div class="small-12 column">
					<?php echo form_submit('submit', lang('edit_user_submit_btn'), "class='button'");?>
				</div> <!--chiudo column-->
				
						
				
			</div> <!--chiudo row-->
		<?php echo form_close();?>
	</div>
</section>
<script>
	$("input[name='data_nascita']").datepicker({
			dateFormat: 'dd/mm/yy',
			changeMonth: true,
			changeYear: true,
			yearRange: "1900:2012"
		});	
</script>