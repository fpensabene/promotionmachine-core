<body class="backend-page"> 

<?php echo $top_menu; ?>

<!---*** BANDA PROMOTIONMACHINE ***-->
<section class="promotion-machine-background">
	<div class="row">
		<div class="small-12 text-center column">
		</div>
	</div>
</section>

<!---*** BREADCRUMBS ***-->
<section class="hrow">
	<div class="row">
		<div class="column">
	
			<ul class="breadcrumbs">
			  <li><a href="<?=site_url()?>admin/index">DASHBOARD</a></li>
			  <li class="unavailable"><a href="#">Admin</a></li>
			  <li><a href="<?=site_url().$this->uri->segment(1)."/".$this->uri->segment(2)?>">Gestione partecipanti</a></li>
			</ul>
		
		</div> <!--chiudo column-->
	</div> <!--chiudo row-->
</section>

<!---*** CANCELLARE!!!!! ***-->
<div class="row hrow">
	<div class="column">
		
		<div class="panel callout  radius">
		  <h5><span class="alert label">Attenzione:</span> PROBLEMI da risolvere</h5>
		  <ul>
			  <li><strike>Mi rendi disponibili i dati del periodo in cui siamo per compilare la tabella qui sotto?</strike></li>
		  </ul>
		</div>
	</div> <!--chiudo column-->
</div> <!--chiudo row-->
<!---*** CANCELLARE!!!!! ***-->



<!---*** BOX CON I DATI DEL PERIODO ***-->
<section class="hrow">
	<div class="row" data-equalizer>
		<div class="small-12 medium-4 large-4 column">
			<div class="dati-concorso-box" data-equalizer-watch>
				<h2 class="dati-concorso-title ">PERIODO</h2>
				<p class="dati-concorso-subtitle">Nome del perido</p>
				<p class="dati-concorso-value dati-concorso-value--title"><?=$dati_periodo->descrizione?></p>
			</div>
		</div> <!--chiudo column-->
		
		<div class="small-12 medium-4 large-4 column">
			<div class="dati-concorso-box" data-equalizer-watch>
				<h2 class="dati-concorso-title ">DATA INIZIO</h2>
				<p class="dati-concorso-subtitle">Data in cui inizia il periodo</p>
				<p class="dati-concorso-value dati-concorso-value--start"><?=DateTime::createFromFormat('Y-m-d G:i:s', $dati_periodo->data_inizio)->format('d/m/Y H:i:s')?></p>
			</div>
		</div> <!--chiudo column-->
		
		<div class="small-12 medium-4 large-4 column">
			<div class="dati-concorso-box" data-equalizer-watch>
				<h2 class="dati-concorso-title ">DATA FINE</h2>
				<p class="dati-concorso-subtitle">Data in cui finisce il periodo</p>
				<p class="dati-concorso-value dati-concorso-value--end"><?=DateTime::createFromFormat('Y-m-d G:i:s', $dati_periodo->data_fine)->format('d/m/Y H:i:s')?></p>
			</div>
		</div> <!--chiudo column-->
	</div> <!--chiudo row-->
</section>


<!---*** CONTENUTO DELLA PAGINA - LISTA DELLE PROVE ***-->
<section class="hrow">
	<div class="row">
		<div class="small-12 column">
			
			<div style="display:none" id="output" data-alert class="alert-box radius">
			  <a href="#" class="close">&times;</a>
			</div>	
		</div>
		
	</div>
	<div class="row">
		<div class="small-12 column">
			<h1>Prove</h1>
		</div>
	</div>
	<div class="row">
		<div class="small-12 column">
			<table id="prove" style="width: 100%;">
			</table>
		</div>
	</div>
</section>



<script>


function popolaProve(where,base_url){
	$.ajax({
	    type:'GET',
	    dataType: 'json',
	    url:base_url+'prova/get_dati_prove_json/Tutte/<?=$idperiodo?>',
	    data:{},
	    success:function(response){
	    //attenzione. success (questo sopra) vuol dire che la chiamata ha avuto successo, che il server ha risposto senza dare errori fatali, non vuol dire che la funzione abbia fatto quel che doveva fare
	    	if (response.result=='success'){
		    	var dati = response.output;
		    	var array_periodi = new Array();
		    	
		        $(where).html("<tr><th>ID</th><th>Nome</th><th>Descrizione</th><th>Data inizio</th><th>Data fine</th><th>Stato</th></tr>");
		        for (var i = 0; i < dati.length; i++) {
			        $(where).append("<tr></tr>");
			        $(where+" tr:last").append("<td>"+dati[i]['provaID']+"</td>");
			        if (dati[i]['admin_link'] !== undefined){
				        $(where+" tr:last").append("<td><a href='<?=site_url()?>"+dati[i]['admin_link']+"'>"+dati[i]['nome']+"</a></td>");
			        }
			        else {
				        $(where+" tr:last").append("<td>"+dati[i]['nome']+"</td>");
			        }
			        $(where+" tr:last").append("<td>"+dati[i]['descrizione']+"</td>");
			        $(where+" tr:last").append("<td>"+dati[i]['data_inizio']+"</td>");
			        $(where+" tr:last").append("<td>"+dati[i]['data_fine']+"</td>");
			        $(where+" tr:last").append("<td>"+dati[i]['desc_stato']+"</td>");
			    }
			        
				}
	    },
	    error:function(response){
	    }
	});		
}
//$("input[name='data_fine']").fdatetimepicker();

$(document).ready(function(){
	popolaProve("#prove","<?echo base_url();?>");		
});
</script>

