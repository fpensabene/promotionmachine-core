<body class="backend-page"> 

<?php echo $top_menu; ?>

<!---*** BANDA PROMOTIONMACHINE ***-->
<section class="promotion-machine-background">
	<div class="row">
		<div class="small-12 text-center column">
		</div>
	</div>
</section>

<!---*** BREADCRUMBS ***-->
<section class="hrow">
	<div class="row">
		<div class="column">
	
			<ul class="breadcrumbs">
			  <li><a href="<?=site_url()?>admin/index">DASHBOARD</a></li>
			  <li class="unavailable"><a href="#">Admin</a></li>
			  <li><a href="<?=site_url().$this->uri->segment(1)."/".$this->uri->segment(2)?>">Gestione partecipanti</a></li>
			</ul>
		
		</div> <!--chiudo column-->
	</div> <!--chiudo row-->
</section>




<!---*** BOX CON I DATI DEL CONCORSO ***-->
<section class="hrow">
	<div class="row">
		<div class="small-12 medium-4 large-4 column">
			<div class="dati-concorso-box">
				<h2 class="dati-concorso-title ">CONCORSO</h2>
				<p class="dati-concorso-subtitle">Nome del concorso</p>
				<p class="dati-concorso-value"><?php echo $concorso_dati["nome"]; ?></p>
			</div>
		</div> <!--chiudo column-->
		
		<div class="small-12 medium-4 large-4 column">
			<div class="dati-concorso-box">
				<h2 class="dati-concorso-title ">DATA INIZIO</h2>
				<p class="dati-concorso-subtitle">Data in cui inizia il concorso</p>
				<p class="dati-concorso-value dati-concorso-value--start"><?php echo DateTime::createFromFormat('Y-m-d G:i:s', $concorso_dati["data_inizio"])->format('d/m/Y H:i:s'); ?></p>
			</div>
		</div> <!--chiudo column-->
		
		<div class="small-12 medium-4 large-4 column">
			<div class="dati-concorso-box">
				<h2 class="dati-concorso-title ">DATA FINE</h2>
				<p class="dati-concorso-subtitle">Data in cui finisce il concorso</p>
				<p class="dati-concorso-value dati-concorso-value--end"><?php echo DateTime::createFromFormat('Y-m-d G:i:s', $concorso_dati["data_fine"])->format('d/m/Y H:i:s'); ?></p>
			</div>
		</div> <!--chiudo column-->
	</div> <!--chiudo row-->
</section>



<section class="hrow">

	<div class="row"> 
		<div class="small-12 column">
			<!---*** MESSAGGI DEL SISTEMA ***-->
			<div style="display:none" id="output" data-alert class="alert-box radius">
				<a href="#" class="close">&times;</a>
			</div>
		</div> 
	</div>

	<div class="row">
		<div class="small-12 column">
			<h1>Periodi</h1>
		</div>
	</div>
	<div class="row">
		<div class="small-12 column">
			<table id="periodi" style="width: 100%;">
			</table>
		</div>
	</div>	
</section>
<script>


function popolaPeriodi(where,base_url){
	$.ajax({
	    type:'GET',
	    dataType: 'json',
	    url:base_url+'impostazione_periodi_e_concorsi/get_dati_periodi/',
	    data:{},
	    success:function(response){
	    //attenzione. success (questo sopra) vuol dire che la chiamata ha avuto successo, che il server ha risposto senza dare errori fatali, non vuol dire che la funzione abbia fatto quel che doveva fare
	    	if (response.result=='success'){
		    	var dati = response.output;
		        //console.log(response.output); 
		        $(where).html("");
		        $(where).append("<tr><th>ID</th><th>Descrizione</th><th>Data inizio</th><th>Data fine</th></tr>");
		        for (var i = 0; i < dati.length; i++) {
			        $(where).append("<tr><td>"+dati[i]['periodoID']+"</td><td><a href='<?=site_url()?>gestione_concorso/prove/"+dati[i]['periodoID']+"'>"+dati[i]['descrizione']+"</a></td><td>"+dati[i]['data_inizio']+"</td><td>"+dati[i]['data_fine']+"</td></tr>");
			        
				}
	        }
	        else {
	        }
	    },
	    error:function(response){
	    }
	});		
}

$(document).ready(function(){
	popolaPeriodi("#periodi","<?echo base_url();?>");
	
});
</script>

