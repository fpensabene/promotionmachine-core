<?
	function stampaBottoniSollecito($user, $context){
		if(property_exists($user, $context)&&$user->$context){
		
		?>
		<button class="radius button small sollecito" id="button_sollecita_<?=$context?>_<?=$user->id?>" type="button" onclick="javascript:sollecitaContestant('<?=base_url()?>','<?=htmlspecialchars($user->id,ENT_QUOTES,'UTF-8')?>','<?=$context?>')">Sollecita <?=$context?></button>
		<div id="spinner_<?=$context?>_<?=$user->id?>" class="spinner_sollecito" style="display:none;">
			<img id="img-spinner" src="/assets/img-backend/ajax-loader.gif" alt="Loading"/>
		</div>
		<?
		}
	}
?>
<body class="backend-page"> 

<?php echo $top_menu; ?>

<!---*** BANDA PROMOTIONMACHINE ***-->
<section class="promotion-machine-background">
	<div class="row">
		<div class="small-12 text-center column">
		</div>
	</div>
</section>

<!---*** BREADCRUMBS ***-->
<section class="hrow">
	<div class="row">
		<div class="column">
	
			<ul class="breadcrumbs">
			  <li><a href="<?=site_url()?>admin/index">DASHBOARD</a></li>
			  <li class="unavailable"><a href="#">Admin</a></li>
			  <li><a href="<?=site_url().$this->uri->segment(1)."/".$this->uri->segment(2)?>">Gestione partecipanti</a></li>
			</ul>
		
		</div> <!--chiudo column-->
	</div> <!--chiudo row-->
</section>



<!---*** MESSAGGI DEL SISTEMA ***-->
<?php if($message): ?>
<section class="hrow">
	<div class="row">
		<div class="small-12 column">
			<div id="infoMessage"><span class="alert label">Attenzione:</span><?php echo $message;?></div>
		</div> <!--chiudo column-->
	</div> <!--chiudo row-->
</section>
<?php endif; ?>
<section class="hrow">
	<div class="row">
		<div class="small-12 medium-8 large-8 medium-centered large-centered column">
			<div style="display:none" id="output" data-alert class="alert-box radius">
			  <a href="#" class="close">&times;</a>
			</div>
		</div>
	</div>
</section>
<!---*** CANCELLARE!!!!! ***-->
<div class="row hrow">
	<div class="column">
		<div class="panel callout  radius">
		  <h5><span class="alert label">Attenzione:</span> PROBLEMI da risolvere</h5>
		  <ul>
		  	<li>Inseriamo una funzionalità per RIMANDARE la mail di attivazione agli utenti che non si sono attivati!</li>
		  	<li><strike>Inseriamo il numero dei partecipanti attivi</strike></li>
		  	<li><strike>Inseriamo il numero dei partecipanti che si devono ancora attivare</strike></li>
		  	<li><strike>Dobbiamo creare la funzione modifica_partecipante()</strike></li>
		  
		  </ul>
		</div>
	</div> <!--chiudo column-->
</div> <!--chiudo row-->
<!---*** CANCELLARE!!!!! ***-->


<section class="hrow">
	<div class="row" data-equalizer>
		<div class="small-12 medium-3 column">
			<div class="gestione-contestants-stat-box" data-equalizer-watch>
				<h3 class="stat-box-title">REGISTRATI</h3>
				<p class="stat-box-subtitle">Numero totale delle persone che hanno compilato il form di iscrizione</p>
				<p class="stat-box-number"><?=$tot_contestants?></p>
			</div>
		</div> <!--chiudo column-->
		<div class="small-12 medium-3 column">
			<div class="gestione-contestants-stat-box" data-equalizer-watch>
				<h3 class="stat-box-title">PENDING</h3>
				<p class="stat-box-subtitle">Numero di persone che non hanno confermato l'iscrizione nella mail</p>
				<p class="stat-box-number stat-box-number--pending"><?=$inactive_contestants?></p>
			</div>
		</div> <!--chiudo column-->
		<div class="small-12 medium-3 column">
			<div class="gestione-contestants-stat-box" data-equalizer-watch>
				<h3 class="stat-box-title">ISCRITTI </h3>
				<p class="stat-box-subtitle">Numero di persone che hanno attivato l'account dalla mail</p>
				<p class="stat-box-number stat-box-number--attivi"><?=$active_contestants?></p>
			</div>
		</div> <!--chiudo column-->

		
		<div class="small-12 medium-3 column">
			<div class="gestione-contestants-stat-box" data-equalizer-watch>
				<h3 class="stat-box-title">LAZY</h3>
				<p class="stat-box-subtitle">Numero di contestants attivi ma che non hanno svolto nessuna prova</p>
				<p class="stat-box-number stat-box-number--pending"><?=$lazy_contestants?></p>
			</div>
		</div> <!--chiudo column-->
	</div> <!--chiudo row-->
	
</section>


<section class="hrow">
	
	<!---*** TITOLO ***-->
	<div class="row">
		<div class="small-12 column">
				<h1 class="hrow-heading">Gestione partecipanti al concorso</h1>
				<p class="hrow-subHeading">Gestione degli utenti iscritti al concorso</p>
		</div> <!--chiudo column-->
	</div> <!--chiudo row-->


	<div class="row">
		<div class="small-12 column">
			
			<table class="hrow-table">
				<tr>
					<th><?php echo lang('index_fname_th');?></th>
					<th><?php echo lang('index_lname_th');?></th>
					<th><?php echo lang('index_email_th');?></th>
					<th><?php echo lang('index_groups_th');?></th>
					<th><?php echo lang('index_status_th');?></th>
					<th><?php echo lang('index_action_th');?></th>
					<th>Lazy</th>
					<th>Pending</th>
				</tr>
				<?php foreach ($users as $user):?>
					<tr>
			            <td><?php echo htmlspecialchars($user->nome,ENT_QUOTES,'UTF-8');?></td>
			            <td><?php echo htmlspecialchars($user->cognome,ENT_QUOTES,'UTF-8');?></td>
			            <td><?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?></td>
						<td>
							<?php foreach ($user->groups as $group):?>
								<?php echo htmlspecialchars($group->name,ENT_QUOTES,'UTF-8') ;?><br />
			                <?php endforeach?>
						</td>
						<td><?php echo ($user->active) ? anchor("gestione_concorso/gestione_stato_partecipante/".$user->id, lang('index_active_link')) : anchor("gestione_concorso/gestione_stato_partecipante/". $user->id, lang('index_inactive_link'));?></td>
						<td><?php echo anchor("gestione_concorso/modifica_partecipante/".$user->id, 'Edit') ;?></td>
						<td><?php stampaBottoniSollecito($user, 'lazy');?>
						<td><?php stampaBottoniSollecito($user, 'pending');?>
						</td>
					</tr>
				<?php endforeach;?>
			</table>
			
		</div> <!--chiudo column-->
	</div> <!--chiudo row-->

</section>

<!---*** Bottone pe creare un nuovo utente ma non ci serve ***-->
	
<!---*** <p><?php echo anchor('admin/iscriviti', lang('index_iscriviti_link'))?></p> ***-->

<script>
function sollecitaContestant(base_url, user, context){
	$.ajax({
	    type:'POST',
	    dataType: 'json',
	    beforeSend: function(){
		  $("#spinner_"+context+"_"+user).show();
		  $("#button_sollecita_"+context+"_"+user).attr("disabled","disabled");
		  $("#button_sollecita_"+context+"_"+user).addClass("disabled");
		},
		complete: function(){
		  $("#spinner_"+context+"_"+user).hide();
		  /*direi che qua i pulsanti NON vadano riattivati se l'esito e' positivo*/
		  /*$("#button_go").removeAttr("disabled");
		  $("#button_go").removeClass("disabled");*/
		},
	    url:base_url+'gestione_concorso/sollecita_contestant',
	    data: {		    
		    user:user,
		    context:context
	    },
	    success:function(response){
	    //attenzione. success (questo sopra) vuol dire che la chiamata ha avuto successo, che il server ha risposto senza dare errori fatali, non vuol dire che la funzione abbia fatto quel che doveva fare
	    	if (response.result=='success'){
		        printMsg('success',response.message); 
	        }
	        else {
		        printMsg('alert',response.message); 
		        $("#button_sollecita_"+context+"_"+user).removeAttr("disabled");
				$("#button_sollecita_"+context+"_"+user).removeClass("disabled");
	        }
	    },
	    error:function(response){
	        printMsg('alert','Errore durante la chiamata'); 
	    }
	});	
}
	
	
</script>
