<body class="backend-page"> 

	<?php echo $top_menu; ?>
	
	<!---*** BANDA PROMOTIONMACHINE ***-->
	<section class="promotion-machine-background">
		<div class="row">
			<div class="small-12 text-center column">
			</div>
		</div>
	</section>
	
	<!---*** BREADCRUMBS ***-->
	<section class="hrow">
		<div class="row">
			<div class="column">
		
				<ul class="breadcrumbs">
				  <li><a href="<?=site_url()?>admin/index">DASHBOARD</a></li>
				  <li class="unavailable"><a href="#">Admin</a></li>
				  <li><a href="<?=site_url().$this->uri->segment(1)."/".$this->uri->segment(2)?>">Gestione partecipanti</a></li>
				</ul>
			
			</div> <!--chiudo column-->
		</div> <!--chiudo row-->
	</section>
	<section class="hrow">
		<div class="row">
			<div style="display:none" id="output" data-alert class="alert-box radius">
			  <a href="#" class="close">&times;</a>
			</div>
		</div>
		<div class="row">
			<div class="small-12 column">
				<h1>Tipi prova</h1>
			</div>
		</div>
		<div class="row">
			<div class="small-12 column">
				<table id="tipi_prova" style="width: 100%;">
				</table>
			</div>
		</div>
		<div class="row">  
			<div class="small-12 columns">
				<button onclick="javascript:riportaDatiTipiProva()"  class="radius button" id="button_aggiungi" type="button" >Aggiungi un tipo prova</button>
			</div>
		</div>
		<form style="display: none" data-abide style="margin-top:120px;" action="" method="POST" id="gestione_tipi_prova" method="POST" enctype="multipart/form-data">
			<div class="row">  
				<input name="idtipo_prova" id="idtipo_prova" type="hidden" value="" required/>
				<div class="small-12 medium-4 large-4 column">			
					<label>Descrizione tipo prova
						<input name="descrizione" id="descrizione" type="text" value="" required/>
					<small class="error">Inserire la descrizione del tipo prova</small>	
					</label>
				</div>	
				<div class="small-12 medium-4 large-4 column">		
					<label for="periodi_disponibili">Punteggio
						<input name="punteggio" id="punteggio" type="number" min="0"  value="" required/>
				    </label>
				    <small class="error">Devi inserire un punteggio per il tipo prova</small>
				</div>	
				<div class="small-12 medium-4 large-4  columns">		
					<label for="controllers_disponibili">Controller frontend
						<!---*** Questa select viene popolata tramite una chiamata ajax ***-->
						<select id="controllers_disponibili" name="controllers_disponibili" class="medium" required>
						  <option value="">Seleziona un controller ...</option>
						</select>
				    </label>
				    <small class="error">Devi selezionare un controller</small>
				</div>	
			</div>
			<div class="row">  
				<div class="small-12 columns">
					<button class="radius button" id="button_go" type="button" onclick="javascript:gestioneTipiProva('<?echo base_url();?>', $('#idtipo_prova').val())">Procedi</button>
				</div>
			</div>	
		</form>	
	</section>
</body>

<script>

function riportaDatiTipiProva(dati_tipo_prova){
	if (dati_tipo_prova === undefined){
		var dati_tipo_prova = new Array();
		dati_tipo_prova['tipoProvaID']='_NEW';
	}
	var idtipo_prova = dati_tipo_prova["tipoProvaID"];
	var descrizione = dati_tipo_prova["descrizione"];
	var punteggio = dati_tipo_prova["punteggio"];
	var controller = dati_tipo_prova["FK_CORE_tipoProva_CORE_controllers"];
	
	$("#idtipo_prova").val(idtipo_prova);
	$("#descrizione").val(descrizione);
	$("#punteggio").val(punteggio);
	$("#controllers_disponibili").val(controller);
	$("#gestione_tipi_prova").show();
}	


function gestioneTipiProva(base_url, idtipo_prova){
	$.ajax({
	    type:'POST',
	    dataType: 'json',
	    url:base_url+'tipo_prova/gestione/'+idtipo_prova,
	    data:{
		    descrizione : $("#descrizione").val(),
		    punteggio: $("#punteggio").val(),
		    controller_frontend: $("#controllers_disponibili").val()
	    },
	    success:function(response){
	    //attenzione. success (questo sopra) vuol dire che la chiamata ha avuto successo, che il server ha risposto senza dare errori fatali, non vuol dire che la funzione abbia fatto quel che doveva fare
	    	if (response.result=='success'){
		        printMsg('success',response.message); 
		        popolaTipiProvaTable("#tipi_prova","<?echo base_url();?>");
		        $("#gestione_tipi_prova").hide();
		        
	        }
	        else {
		        printMsg('alert',response.message); 
	        }
	    },
	    error:function(response){
	        printMsg('alert','Errore durante la chiamata'); 
	    }
	});	
}


function popolaTipiProvaTable(where,base_url){
	$.ajax({
	    type:'GET',
	    dataType: 'json',
	    url:base_url+'tipo_prova/get_dati_tipi_prova/',
	    data:{},
	    success:function(response){
	    //attenzione. success (questo sopra) vuol dire che la chiamata ha avuto successo, che il server ha risposto senza dare errori fatali, non vuol dire che la funzione abbia fatto quel che doveva fare
	    	if (response.result=='success'){
		    	var dati = response.output;
		        $(where).html("");
		        $(where).append("<tr><th>ID</th><th>Descrizione</th><th>Punteggio</th><th>Controller frontend</th><th>Controller backend</th><th></th><th></th></tr>");
		        for (var i = 0; i < dati.length; i++) {
			        $(where).append("<tr><td>"+dati[i]['tipoProvaID']+"</td><td>"+dati[i]['descrizione']+"</td><td>"+dati[i]['punteggio']+"</td><td>"+dati[i]['nome_controller_frontend']+"</td><td>"+dati[i]['nome_controller_backend']+"</td><td><a class='edit_tipo_prova' id='riporta_"+dati[i]['tipoProvaID']+"'>M</a></td><td><a id='delete_"+dati[i]['tipoProvaID']+"'>D</a></td></tr>");
			        $("#riporta_"+dati[i]['tipoProvaID']).click({dati:dati[i]},
					    function(e){
						    var dati_tipo_prova = e.data.dati;
						    riportaDatiTipiProva(dati_tipo_prova);
					    }
					);
					$("#delete_"+dati[i]['tipoProvaID']).click({dati:dati[i]},
					    function(e){
						    var dati_tipo_prova = e.data.dati;
						    deleteEntry(dati_tipo_prova['tipoProvaID'],'tipo_prova','<?echo base_url();?>');

					    }
					);
			        
				}
	        }
	        else {
	        }
	    },
	    error:function(response){
	    }
	});		
}

function deleteEntry(id,context, base_url){
	$.ajax({
	    type:'POST',
	    dataType: 'json',
	    url:base_url+context+'/delete/'+id,
	    data:{},
	    success:function(response){
	    //attenzione. success (questo sopra) vuol dire che la chiamata ha avuto successo, che il server ha risposto senza dare errori fatali, non vuol dire che la funzione abbia fatto quel che doveva fare
	    	if (response.result=='success'){
		        printMsg('success',response.message); 
		        popolaTipiProvaTable("#tipi_prova","<?echo base_url();?>");
	        }
	        else {
		        printMsg('alert',response.message); 
	        }
	    },
	    error:function(response){
	        printMsg('alert','Errore durante la chiamata'); 
	    }
	});		
}

function popolaControllers(where,base_url){
	$.ajax({
	    type:'GET',
	    dataType: 'json',
	    async: false,
	    url:base_url+'controllers/get_dati_controllers_json/',
	    data:{},
	    success:function(response){
	    //attenzione. success (questo sopra) vuol dire che la chiamata ha avuto successo, che il server ha risposto senza dare errori fatali
	    	if (response.result=='success'){
		    	var dati = response.output;
		        for (var i = 0; i < dati.length; i++) {
			        $(where).append("<option value='"+dati[i]['controllerID']+"'>"+dati[i]['nome_controller_frontend']+"</option>");
				}
	        }
	        else {
	        }
	    },
	    error:function(response){
	    }
	});	
}

$(document).ready(function(){
	popolaTipiProvaTable("#tipi_prova","<?echo base_url();?>");
	popolaControllers("#controllers_disponibili","<?echo base_url();?>");
	$("input[name='data_inizio']").datetimepicker({
		showSecond: true,
		timeFormat: 'HH:mm:ss',
		dateFormat: 'dd/mm/yy'
	});
	$("input[name='data_fine']").datetimepicker({
		showSecond: true,
		timeFormat: 'HH:mm:ss',
		dateFormat: 'dd/mm/yy'
	});

});
</script>

