<!---*** IMPOSTAZIONE DATI DEL CONCORSO ***-->

<body class="backend-page"> 

<?php echo $top_menu; ?>

<!---*** BANDA PROMOTIONMACHINE ***-->
<section class="promotion-machine-background">
	<div class="row">
		<div class="small-12 text-center column">
		</div>
	</div>
</section>

<!---*** BREADCRUMBS ***-->
<section class="hrow">
	<div class="row">
		<div class="column">
	
			<ul class="breadcrumbs">
			  <li><a href="<?=site_url()?>admin/index">DASHBOARD</a></li>
			  <li class="unavailable"><a href="#">Admin</a></li>
			  <li><a href="<?=site_url().$this->uri->segment(1)."/".$this->uri->segment(2)?>">Gestione partecipanti</a></li>
			</ul>
		
		</div> <!--chiudo column-->
	</div> <!--chiudo row-->
</section>


<!---*** CANCELLARE!!!!! ***-->
<div class="row hrow">
	<div class="column">
		<div class="panel callout  radius">
		  <h5><span class="alert label">Attenzione:</span> PROBLEMI da risolvere</h5>
		  <ul>
		  	<li><strike>i feedback di azioni CRUD su questa pagina non sono ben chiari (sembra che non si aggiornino)</strike></li>
		  </ul>
		</div>
	</div> <!--chiudo column-->
</div> <!--chiudo row-->
<!---*** CANCELLARE!!!!! ***-->


<section class="hrow">
	
	
	<!---*** TITOLO ***-->
	<div class="row">
		<div class="small-12 column">
				<h1 class="hrow-heading">Gestione Concorso</h1>
				<p class="hrow-subHeading">In ogni instanza di promotionmachine gestiamo un unico concorso, qui gestiamo le impostazioni principali</p>
				<!--<div id="infoMessage"><?php echo $message;?></div>-->
				<?php if (isset($message) && $message): ?>
					<div id="infoMessage"><span class="alert label">Attenzione:</span><?php echo $message;?></div>
				<?php endif; ?>
		</div> <!--chiudo column-->
	</div> <!--chiudo row-->

	<div class="row">
		<div class="column">
			<!---*** QUESTO E' IL FEEDBACK AJAX ***-->
			<div id="output" data-alert class="alert-box radius" style="display: none">
				<a href="#" class="close">&times;</a>
			</div>
		</div> <!--chiudo column-->
	</div> <!--chiudo row-->


	
	<form data-abide  action="" method="POST" id="dati_concorso" method="POST" enctype="multipart/form-data">
		<div class="row">  
			
			<input name="idconcorso" id="idconcorso" type="hidden" value="" required/>
			
			<div class="small-12 medium-6 large-6  columns">			
				<label>Nome
				<input name="nome" id="nome" type="text" value="" required/>
				<small class="error">Inserire il nome del concorso</small>	
				</label>
			</div>
			<div class="small-12 medium-6 large-6  columns">		
				<label for="stati_disponibili">Stato
					<!---*** Questa select viene popolata tramite una chiamata ajax ***-->
					<select id="stati_disponibili" name="stati_disponibili" class="medium" required>
					  <option value="">Seleziona stato di apertura ...</option>
					</select>
			    </label>
			    <small class="error">Devi selezionare uno stato</small>
			</div>
		</div>
		<div class="row"> 
			<div class="small-12 large-6 columns">
				<label>Data inizio
				<input name="data_inizio" readonly="readonly" id="data_inizio" type="text" value="" required/>
				<small class="error">Inserire la data di inizio del concorso</small>	
				</label>
			</div>
			<div class="small-12 large-6 columns">			
				<label>Data fine
				<input name="data_fine" readonly="readonly"  id="data_fine" type="text" value="" required/>
				<small class="error">Inserire la data di fine del concorso</small>	
				</label>
			</div>
		</div>
		<div class="row"> 
			<div class="small-12 large-6 columns">
				<label>Codice Analytics
				<textarea  name="codice_analytics" id="codice_analytics" value=""></textarea>
				<small class="error"></small>	
				</label>
			</div>
			<div class="small-12 large-6 columns">			
				<label>Email dell'amministratore
				<input name="admin_email"  id="admin_email" type="text" value="" />
				<small class="error">Inserire una email valida</small>	
				</label>
			</div>
		</div>
		<div class="row">  
			<div class="small-12 large-6 columns">			
				<label>Regolamento
				<input name="regolamento" id="regolamento" type="file"/>
				<span id="regolamento_caricato"></span>
				<small class="error">Il regolamento &egrave; obbligatorio</small>	
				</label>
			</div>
			<div class="small-12 large-6 columns">			
				<label>Privacy
				<input name="privacy" id="privacy" type="file"/>
				<span id="privacy_caricata"></span>
				<small class="error">La privacy &egrave; obbligatoria</small>	
				</label>
			</div>
		</div>	
		<div class="row">  
			<div class="small-12 columns">
				<button class="radius button" id="button_go" type="button" onclick="javascript:gestioneConcorso('<?echo base_url();?>', $('#idconcorso').val())">Procedi</button>
				<div id="spinner" class="spinner" style="display:none;">
				    <img id="img-spinner" src="/assets/img-backend/ajax-loader.gif" alt="Loading"/>
				</div>
			</div>
			
		</div>	
	</form>	
</section>

<script>
	$(document).ready(function(){
			
			
		$("input[name='data_inizio']").datetimepicker({
			showSecond: true,
			timeFormat: 'HH:mm:ss',
			dateFormat: 'dd/mm/yy'
		});
		$("input[name='data_fine']").datetimepicker({
			showSecond: true,
			timeFormat: 'HH:mm:ss',
			dateFormat: 'dd/mm/yy'
		});	
			
		// popolaStati definita in app.js - restituisce un elenco della tabella stati (che dovrebbe chiamarsi CORE_stati) del db  
		popolaStati("#stati_disponibili","<?echo base_url();?>");
			
		popolaConcorso("<?echo base_url();?>");
		
	    
	});
			
	// funzione ajax chiamata nel momento in cui facciamo modifiche		
	function gestioneConcorso(base_url, idconcorso) {
		
		$.ajax( {
		  url:base_url+'concorso/gestione/'+idconcorso,
		  type: 'POST',
		  dataType: 'json',
		  beforeSend: function(){
			  $("#spinner").show();
			  $("#button_go").attr("disabled","disabled");
			  $("#button_go").addClass("disabled");
		  },
		  complete: function(){
			  $("#spinner").hide();
			  $("#button_go").removeAttr("disabled");
			  $("#button_go").removeClass("disabled");
		  },
		  data: new FormData( $("#dati_concorso")[0] ),
		  processData: false,
		  contentType: false,
		  success:function(response){
		    //attenzione. success (questo sopra) vuol dire che la chiamata ha avuto successo, che il server ha risposto senza dare errori fatali, non vuol dire che la funzione abbia fatto quel che doveva fare
		    	if (response.result=='success'){
			        printMsg('success',response.message); 
			        popolaConcorso("<?echo base_url();?>");
		        }
		        else {
			        printMsg('alert',response.message); 
		        }
		    },
		    error:function(response){
		        printMsg('alert','Errore durante la chiamata'); 
		    }
		} );
	}
	
	// Funzione ajax chiamata per popolare il concorso
	function popolaConcorso(base_url){
		$.ajax({
		    type:'GET',
		    dataType: 'json',
		    url:base_url+'concorso/get_dati_concorso_json/',
		    data:{},
		    success:function(response){
		    //attenzione. success (questo sopra) vuol dire che la chiamata ha avuto successo, che il server ha risposto senza dare errori fatali, non vuol dire che la funzione abbia fatto quel che doveva fare
		    	if (response.result=='success'){	    	
			    	var dati = response.output;
			    	$("#dati_concorso").attr('action','<?echo site_url();?>concorso/gestione/'+dati['concorsoID']);
			    	$("#idconcorso").val(dati['concorsoID']);
			    	$("#nome").val(dati['nome']);
			    	$("#data_inizio").val(dati['data_inizio']);
			    	$("#data_fine").val(dati['data_fine']);
			    	if (dati['regolamento']){
				    	$("#regolamento_caricato").html("Attuale: <a target='_blank' href='"+dati['regolamento']+"'>"+dati['regolamento']+"</a>");
				    }
				    if (dati['privacy']){
			    		$("#privacy_caricata").html("Attuale: <a target='_blank' href='"+dati['privacy']+"'>"+dati['privacy']+"</a>");
			    	}
			    	$("#stati_disponibili").val(dati['FK_concorsi_stati']);
			    	$("#codice_analytics").val(dati['codice_analytics']);
			    	$("#admin_email").val(dati['admin_email']);
			    }
		        else {
		        }
		    },
		    error:function(response){
		    }
		});		
	}
	

</script>