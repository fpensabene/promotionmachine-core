<body class="backend-page"> 

<?php echo $top_menu; ?>

<!---*** BANDA PROMOTIONMACHINE ***-->
<section class="promotion-machine-background">
	<div class="row">
		<div class="small-12 text-center column">
		</div>
	</div>
</section>

<!---*** BREADCRUMBS ***-->
<section class="hrow">
	<div class="row">
		<div class="column">
	
			<ul class="breadcrumbs">
			  <li><a href="<?=site_url()?>admin/index">DASHBOARD</a></li>
			  <li class="unavailable"><a href="#">Admin</a></li>
			  <li><a href="<?=site_url().$this->uri->segment(1)."/".$this->uri->segment(2)?>">Gestione partecipanti</a></li>
			</ul>
		
		</div> <!--chiudo column-->
	</div> <!--chiudo row-->
</section>


<!---*** CANCELLARE!!!!! ***-->
<div class="row hrow">
	<div class="column">
		<div class="panel callout  radius">
		  <h5><span class="alert label">Attenzione:</span> PROBLEMI da risolvere</h5>
		  <ul>
			  
		  </ul>
		</div>
	</div> <!--chiudo column-->
</div> <!--chiudo row-->
<!---*** CANCELLARE!!!!! ***-->




<!---*** BOX CON I DATI DEL PERIODO ***-->
<section class="hrow">
	<div class="row" data-equalizer>
		<div class="small-12 medium-4 large-4 column">
			<div class="dati-concorso-box" data-equalizer-watch>
				<h2 class="dati-concorso-title ">PERIODO</h2>
				<p class="dati-concorso-subtitle">Nome del periodo</p>
				<p class="dati-concorso-value dati-concorso-value--title"><?=$dati_periodo->descrizione?></p>
			</div>
		</div> <!--chiudo column-->
		
		<div class="small-12 medium-4 large-4 column">
			<div class="dati-concorso-box" data-equalizer-watch>
				<h2 class="dati-concorso-title ">DATA INIZIO</h2>
				<p class="dati-concorso-subtitle">Data in cui inizia il periodo</p>
				<p class="dati-concorso-value dati-concorso-value--start"><?=DateTime::createFromFormat('Y-m-d G:i:s', $dati_periodo->data_inizio)->format('d/m/Y H:i:s')?></p>
			</div>
		</div> <!--chiudo column-->
		
		<div class="small-12 medium-4 large-4 column">
			<div class="dati-concorso-box" data-equalizer-watch>
				<h2 class="dati-concorso-title ">DATA FINE</h2>
				<p class="dati-concorso-subtitle">Data in cui finisce il periodo</p>
				<p class="dati-concorso-value dati-concorso-value--end"><?=DateTime::createFromFormat('Y-m-d G:i:s', $dati_periodo->data_fine)->format('d/m/Y H:i:s')?></p>
			</div>
		</div> <!--chiudo column-->
	</div> <!--chiudo row-->
</section>








<section class="hrow">
	<div class="row"> 
		<div style="display:none" id="output" data-alert class="alert-box radius">
		  <a href="#" class="close">&times;</a>
		</div>
	</div>
	<div class="row">
		<div class="small-12 column">
			<h1 class="hrow-heading">Imposta e gestisci le prove in questo periodo: </h1>
			<p class="hrow-subHeading"></p>
		</div>
	</div>
	<div class="row">
		<div class="small-12 column">
			<!--QUI VIENE GENERATA LA TABELLA-->
			<div id="prove" style="width: 100%;">
			</div>
		</div>
	</div>
	<div class="row">  
		<div class="small-12 columns">
			<a href="<?=site_url()?>impostazione_periodi_e_concorsi" class="button radius">← Torna alla lista dei periodi</a>
			<button onclick="javascript:riportaDatiProve()"  class="radius button" id="button_aggiungi" type="button" >Aggiungi una prova</button>
		</div>
	</div>
	<form style="display: none" data-abide style="margin-top:120px;" action="" method="POST" id="gestione_prove" method="POST" enctype="multipart/form-data">
		<div class="row">  
			<input name="idprova" id="idprova" type="hidden" value="" required/>
			<div class="small-12 medium-6 large-6 columns">			
				<label>Nome prova
				<input name="nome" id="nome" type="text" value="" required/>
				<small class="error">Inserire il nome della prova</small>	
				</label>
			</div>
			<div class="small-12 medium-6 large-6  columns">		
				<label for="stati_disponibili">Stato
					<select id="stati_disponibili" class="medium" required>
					  <option value="">Seleziona stato di apertura</option>
					</select>
			    </label>
			    <small class="error">Devi selezionare uno stato</small>
			</div>
		</div>
		<div class="row">  
			<div class="small-12 medium-6 large-6  columns">			
				<label>Descrizione prova
				<textarea name="descrizione" id="descrizione" type="textarea" rows="4" cols="15" value="" required></textarea>
				<small class="error">Inserire una breve descrizione della prova</small>	
				</label>
			</div>	
			<div class="small-12 medium-6 large-6  columns">		
				<label for="tipi_prova_disponibili">Tipo prova
					<select id="tipi_prova_disponibili" class="medium" required>
					  <option value="">Seleziona tipo prova</option>
					</select>
			    </label>
			    <small class="error">Devi selezionare un tipo prova</small>
			</div>
		</div>
		<div class="row">  
		</div>		
		<div class="row">  	
			<div class="small-12 medium-6 large-6  columns" style="display: none">		
				<label for="periodi_disponibili">Periodo
					<input id="periodi_disponibili" type="hidden" required="" value="<?=$idperiodo?>"/>
			    </label>
			    <small class="error">Devi selezionare un periodo</small>
			</div>	
		</div>
		<div class="row">  	
			<div class="small-12 medium-4 large-4 columns">
				<label>
					<input id="utilizza_date_periodo" type="checkbox" checked="checked"/>	
					Utilizza data di inizio e fine uguali al periodo
				</label>
			</div>
			<div class="small-12 medium-4 large-4 columns">
				<input type="radio" id="ripetibile_no" name="ripetibile" value="0" checked="checked"/> No<br/>
		        <input type="radio" id="ripetibile_n_volte" name="ripetibile" value="N"/> N volte<br/>  
		        <div id="ripetibile_quante_volte_wrapper" style="display: none">
			        <label for="ripetibile_quante_volte">Quante volte?
			        	<input id="ripetibile_quante_volte" type="text" value=""/>
				    </label>
				    <small class="error">Devi inserire un valore</small>
		        </div>
		        <input type="radio" id="ripetibile_inf" name="ripetibile" value="INF"/> &infin;
			</div>
			<div class="small-12 medium-4 large-4 columns" id="div_solo_maggiorenni" style="display: none">
				<label>
					<input id="solo_maggiorenni" type="checkbox" checked="checked"/>	
					Solo maggiorenni?
				</label>
			</div>
		</div>
		<div class="row">
			<div class="small-12 medium-6 large-6 columns" id="data_inizio_div" style="display: none">
				<label>Data inizio
				<input name="data_inizio" readonly="readonly" id="data_inizio" type="text" value=""/>
				<small class="error">Inserire la data di inizio della prova</small>	
				</label>
			</div>
			<div class="small-12 medium-6 large-6 columns" id="data_fine_div" style="display: none">			
				<label>Data fine
				<input name="data_fine" readonly="readonly"  id="data_fine" type="text" value=""/>
				<small class="error">Inserire la data di fine della prova</small>	
				</label>
			</div>
		</div>
		<div class="row">  
			<div class="small-12 columns">
				<button class="radius button" id="button_go" type="button" onclick="javascript:gestioneProve('<?echo base_url();?>', $('#idprova').val())">Procedi</button>
				<div id="spinner" class="spinner" style="display:none;">
				    <img id="img-spinner" src="/assets/img-backend/ajax-loader.gif" alt="Loading"/>
				</div>
			</div>
		</div>	
	</form>	
</section>
<script>

function riportaDatiProve(dati_prova){
	if (dati_prova === undefined){
		var dati_prova = new Array();
		dati_prova['provaID']='_NEW';
	}
	var idprova = dati_prova['provaID'];
	var descrizione = dati_prova['descrizione'];
	var nome = dati_prova['nome'];
	var data_inizio = dati_prova['data_inizio'];
	var data_fine = dati_prova['data_fine'];
	var stato = dati_prova['FK_prove_stati'];
	var tipo_prova = dati_prova['FK_prove_tipiProve'];
	var ripetibile = dati_prova['ripetibile'];
	var date_custom = dati_prova['date_custom'];
	var solo_maggiorenni = dati_prova['solo_maggiorenni'];
	$("#idprova").val(idprova);
	$("#descrizione").val(descrizione);
	$("#nome").val(nome);	
	
	if (stato){
		$("#stati_disponibili").val(stato);
	}
	else {
		$("#stati_disponibili").val($('#stati_disponibili option').filter(function () { return $(this).html() == "Attivo"; }).val());
	}
	$("#tipi_prova_disponibili").val(tipo_prova);
	
	$("#gestione_prove").show();
	if(ripetibile=='0'){
		$("#ripetibile_no").prop('checked',true);
	}
	else if(ripetibile=='INF'){
		$("#ripetibile_inf").prop('checked',true);
	}
	else {
		$("#ripetibile_n_volte").prop('checked',true);
		$("#ripetibile_quante_volte").val(ripetibile);
		$("#ripetibile_quante_volte_wrapper").show();
	}
	if (!date_custom){
		$("#utilizza_date_periodo").prop('checked',true);
		$("#utilizza_date_periodo").trigger("change");
		$("#data_inizio").val("");
		$("#data_fine").val("");	
			}
	else {
		$("#utilizza_date_periodo").prop('checked',false);
		$("#utilizza_date_periodo").trigger("change");
		$("#data_inizio").val(data_inizio);
		$("#data_fine").val(data_fine);	

	}
	$("#tipi_prova_disponibili").trigger('change');
	if (solo_maggiorenni == 0){
		$("#solo_maggiorenni").prop('checked', false)
	}
}	


function gestioneProve(base_url, idprova){
	var idperiodo = $("#periodi_disponibili").val();
	var ripetibile = 0;
	var solo_maggiorenni = 0;
	if($("#ripetibile").prop('checked')){
		ripetibile = 1;
	}
	if($("#solo_maggiorenni").prop('checked')){
		solo_maggiorenni = 1;
	}
	
	$.ajax({
	    type:'POST',
	    dataType: 'json',
	    beforeSend: function(){
			$("#spinner").show();
			$("#button_go").attr("disabled","disabled");
			$("#button_go").addClass("disabled");
		},
		complete: function(){
			$("#spinner").hide();
			$("#button_go").removeAttr("disabled");
			$("#button_go").removeClass("disabled");
		},
	    url:base_url+'prova/gestione/'+idperiodo+"/"+idprova,
	    data:{
		    descrizione : $("#descrizione").val(),
		    nome : $("#nome").val(),		    
		    data_inizio : $("#data_inizio").val(),
		    data_fine : $("#data_fine").val(),
		    idstato : $("#stati_disponibili").val(),
		    idperiodo : idperiodo,
		    ripetibile : $('input[name=ripetibile]:checked', '#gestione_prove').val(),
		    ripetibile_quante_volte: $("#ripetibile_quante_volte").val(),
		    tipo_prova:$("#tipi_prova_disponibili").val(),
		    solo_maggiorenni: solo_maggiorenni
	    },
	    success:function(response){
	    //attenzione. success (questo sopra) vuol dire che la chiamata ha avuto successo, che il server ha risposto senza dare errori fatali, non vuol dire che la funzione abbia fatto quel che doveva fare
	    	if (response.result=='success'){
		        printMsg('success',response.message); 
		        popolaProve("#prove","<?echo base_url();?>");
		        $("#gestione_prove").hide();
		        
	        }
	        else {
		        printMsg('alert',response.message); 
	        }
	    },
	    error:function(response){
	        printMsg('alert','Errore durante la chiamata'); 
	    }
	});	
}


function popolaProve(where,base_url){
	$.ajax({
	    type:'GET',
	    dataType: 'json',
	    url:base_url+'prova/get_dati_prove_json/Tutte/'+<?=$idperiodo?>,
	    data:{},
	    success:function(response){
	    //attenzione. success (questo sopra) vuol dire che la chiamata ha avuto successo, che il server ha risposto senza dare errori fatali, non vuol dire che la funzione abbia fatto quel che doveva fare
	    	if (response.result=='success'){
		    	var dati = response.output;
		    	var array_periodi = new Array();
		    	
		        $(where).html("");
		        for (var i = 0; i < dati.length; i++) {
			        
			        if (array_periodi.indexOf(dati[i]['FK_prove_periodi']) == -1){
				        /*
					    in array_periodi salvo gli id dei periodi che hanno almeno una prova gia' stampata.
				        quindi controllo se l'id periodo corrente e' presente in array_periodi				        
				        */
				        array_periodi.push(dati[i]['FK_prove_periodi']);
				        //se non c'e, devo aggiungere una nuova tabella con una riga unica (colspan 8) con il nome del periodo
				        $(where).append("<table style=\"width:100%\" id=\"periodo"+dati[i]['FK_prove_periodi']+"_subtable\"></table>");				        
				        $("#periodo"+dati[i]['FK_prove_periodi']+"_subtable").append("<tr><td colspan='8'>Periodo "+dati[i]['desc_periodo']+"</td></tr>");
				        //oltre alle intestazioni
				        $("#periodo"+dati[i]['FK_prove_periodi']+"_subtable").append("<tr><th>ID</th><th>Nome</th><th>Descrizione</th><th>Data inizio</th><th>Data fine</th><th>Stato</th><th>Ripetibile</th><th></th><th></th></tr>");
			        }	
			        var ripetibile_descr;
			        if (dati[i]['ripetibile']==0){
				        ripetibile_descr = 'No';
			        }
			        else if (dati[i]['ripetibile']=='INF'){
				        ripetibile_descr = 'Sempre';
				    }
				    else {
					    ripetibile_descr = dati[i]['ripetibile']+' volte';
				    }
				    
			        //in ogni caso, aggiungo poi le righe rappresentanti le prove.
				    $("#periodo"+dati[i]['FK_prove_periodi']+"_subtable").append("<tr><td>"+dati[i]['provaID']+"</td><td>"+dati[i]['nome']+"</td><td>"+dati[i]['descrizione']+"</td><td>"+dati[i]['data_inizio']+"</td><td>"+dati[i]['data_fine']+"</td><td>"+dati[i]['desc_stato']+"</td><td>"+ripetibile_descr+"</td><td><a id='riporta_"+dati[i]['provaID']+"'>M</a></td><td><a id='delete_"+dati[i]['provaID']+"'>D</a></td></tr>");
				    
				    //bindo le funzioni di modifica e cancellazione agli anchor che ho creato poco sopra. cosi' facendo il codice e' molto piu' leggibile e non ho problemi di escaping
				    $("#riporta_"+dati[i]['provaID']).click({dati:dati[i]},
					    function(e){
						    var dati_prova = e.data.dati;
						    riportaDatiProve(dati_prova);
					    }
					);
					$("#delete_"+dati[i]['provaID']).click({dati:dati[i]},
					    function(e){
						    var dati_prova = e.data.dati;
						    deleteEntry(dati_prova['provaID'],'prova','<?echo base_url();?>');

					    }
					);
				}
	        }
	        else {
	        }
	    },
	    error:function(response){
	    }
	});		
}

function deleteEntry(id,context, base_url){
	$("#gestione_prove").hide();
	if (confirm("Sei sicuro di voler rimuovere questa prova?")){
		$.ajax({
		    type:'POST',
		    dataType: 'json',
		    url:base_url+context+'/delete/'+id,
		    data:{},
		    success:function(response){
		    //attenzione. success (questo sopra) vuol dire che la chiamata ha avuto successo, che il server ha risposto senza dare errori fatali, non vuol dire che la funzione abbia fatto quel che doveva fare
		    	if (response.result=='success'){
			        printMsg('success',response.message); 
			        popolaProve("#prove","<?echo base_url();?>");
		        }
		        else {
			        printMsg('alert',response.message); 
		        }
		    },
		    error:function(response){
		        printMsg('alert','Errore durante la chiamata'); 
		    }
		});		
	}
}

$("input[name='data_inizio']").datetimepicker({
				showSecond: true,
				timeFormat: 'HH:mm:ss',
				dateFormat: 'dd/mm/yy',
				minDate: '<?=DateTime::createFromFormat('Y-m-d H:i:s', $dati_periodo->data_inizio)->format('d/m/Y')?>',
				maxDate: '<?=DateTime::createFromFormat('Y-m-d H:i:s', $dati_periodo->data_fine)->format('d/m/Y')?>'
			});
$("input[name='data_fine']").datetimepicker({
				showSecond: true,
				timeFormat: 'HH:mm:ss',
				dateFormat: 'dd/mm/yy',
				minDate: '<?=DateTime::createFromFormat('Y-m-d H:i:s', $dati_periodo->data_inizio)->format('d/m/Y')?>',
				maxDate: '<?=DateTime::createFromFormat('Y-m-d H:i:s', $dati_periodo->data_fine)->format('d/m/Y')?>'
			});
//$("input[name='data_fine']").fdatetimepicker();

$(document).ready(function(){
	popolaStati("#stati_disponibili","<?echo base_url();?>");	
	popolaTipiProva("#tipi_prova_disponibili","<?echo base_url();?>");		
	popolaProve("#prove","<?echo base_url();?>");	
	$("#utilizza_date_periodo").change(function(){
		if($(this).prop('checked'))
		{
			$("#data_inizio").val("");
			$("#data_inizio_div").hide();
			$("#data_fine").val("");	
			$("#data_fine_div").hide();
				
		}
		else {
			$("#data_inizio").val('<?=DateTime::createFromFormat('Y-m-d H:i:s', $dati_periodo->data_inizio)->format('d/m/Y H:i:s')?>');
			$("#data_fine").val('<?=DateTime::createFromFormat('Y-m-d H:i:s', $dati_periodo->data_fine)->format('d/m/Y H:i:s')?>');
			$("#data_inizio_div").show();
			$("#data_fine_div").show();
			
		}
	});	
	$("#tipi_prova_disponibili").change(function(){
		if ($(this).val() == <?=$idTipoProvaIscrizione?>) {
			$("#div_solo_maggiorenni").show();
			$(this).prop('checked',true);
		}
		else {
			$("#div_solo_maggiorenni").hide();
			$(this).prop('checked',false);
		}
		
		
	});
	$("input[name='ripetibile']").change(function(){
		if($(this).val()=="N"){
			$("#ripetibile_quante_volte").val("");
			$("#ripetibile_quante_volte_wrapper").show();
			$("#ripetibile_quante_volte").attr('required','required');		
		}
		else {
			$("#ripetibile_quante_volte").val("");
			$("#ripetibile_quante_volte_wrapper").hide();
			$("#ripetibile_quante_volte").attr('required','required');	
		}
	});
});
</script>

