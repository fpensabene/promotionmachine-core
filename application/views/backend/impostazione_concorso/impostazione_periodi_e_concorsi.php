<!---*** LISTA DEI PERIODI NEL CONCORSO ***-->

<body class="backend-page"> 

<?php echo $top_menu; ?>

<!---*** BANDA PROMOTIONMACHINE ***-->
<section class="promotion-machine-background">
	<div class="row">
		<div class="small-12 text-center column">
		</div>
	</div>
</section>

<!---*** BREADCRUMBS ***-->
<section class="hrow">
	<div class="row">
		<div class="column">
	
			<ul class="breadcrumbs">
			  <li><a href="<?=site_url()?>admin/index">DASHBOARD</a></li>
			  <li class="unavailable"><a href="#">Admin</a></li>
			  <li><a href="<?=site_url().$this->uri->segment(1)."/".$this->uri->segment(2)?>">Gestione partecipanti</a></li>
			</ul>
		
		</div> <!--chiudo column-->
	</div> <!--chiudo row-->
</section>

<!---*** CANCELLARE!!!!! ***-->
<div class="row hrow">
	<div class="column">
		<div class="panel callout  radius">
		  <h5><span class="alert label">Attenzione:</span> PROBLEMI da risolvere</h5>
		  <ul>

		  </ul>
		  
		</div>
	</div> <!--chiudo column-->
</div> <!--chiudo row-->
<!---*** CANCELLARE!!!!! ***-->


<!---*** BOX CON I DATI DEL CONCORSO ***-->
<section class="hrow">
	<div class="row" data-equalizer>
		<div class="small-12 medium-4 large-4 column">
			<div class="dati-concorso-box" data-equalizer-watch>
				<h2 class="dati-concorso-title ">CONCORSO</h2>
				<p class="dati-concorso-subtitle">Nome del concorso</p>
				<p class="dati-concorso-value dati-concorso-value--title"><?php echo $concorso_dati["nome"]; ?></p>
			</div>
		</div> <!--chiudo column-->
		
		<div class="small-12 medium-4 large-4 column">
			<div class="dati-concorso-box" data-equalizer-watch>
				<h2 class="dati-concorso-title ">DATA INIZIO</h2>
				<p class="dati-concorso-subtitle">Data in cui inizia il concorso</p>
				<p class="dati-concorso-value dati-concorso-value--start"><?php echo DateTime::createFromFormat('Y-m-d G:i:s', $concorso_dati["data_inizio"])->format('d/m/Y H:i:s'); ?></p>
			</div>
		</div> <!--chiudo column-->
		
		<div class="small-12 medium-4 large-4 column">
			<div class="dati-concorso-box" data-equalizer-watch>
				<h2 class="dati-concorso-title ">DATA FINE</h2>
				<p class="dati-concorso-subtitle">Data in cui finisce il concorso</p>
				<p class="dati-concorso-value dati-concorso-value--end"><?php echo DateTime::createFromFormat('Y-m-d G:i:s', $concorso_dati["data_fine"])->format('d/m/Y H:i:s'); ?></p>
			</div>
		</div> <!--chiudo column-->
	</div> <!--chiudo row-->
</section>



<section class="hrow">

	<div class="row"> 
		<!---*** MESSAGGI DEL SISTEMA ***-->
		<div class="small-12 column">
			<div style="display:none" id="output" data-alert class="alert-box radius">
			  <a href="#" class="close">&times;</a>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="small-12 column">
			<h1 class="hrow-heading">Gestione Periodi e Prove</h1>
			<p class="hrow-subHeading">Qui sotto trovi la lista dei periodi creati, per aggiungere delle prove clicca sul periodo nel quale vuoi crearla.</p>
		</div>
	</div>
	<div class="row">
		<div class="small-12 column">
			<!---*** FORM PER LA AGGIUNTA DI UN NUOVO PERIODO - E' NASCOSTO DI DEFAULT ***-->
			<form style="display:none;" data-abide action="" method="POST" id="js-edit-perido" method="POST" enctype="multipart/form-data">
				<div class="row">  
					<input name="idperiodo" id="idperiodo" type="hidden" value="" required/>
					<div class="small-12 medium-6 large-6  columns">			
						<label>Descrizione periodo
						<input name="descrizione" id="descrizione" type="text" value="" required/>
						<small class="error">Inserire una breve descrizione del periodo</small>	
						</label>
					</div>
					<div class="small-12 medium-6 large-6  columns">		
						<label for="stati_disponibili">Stato
							<select id="stati_disponibili" class="medium" required>
							  <option value="">Seleziona stato di apertura</option>
							</select>
					    </label>
					    <small class="error">Devi selezionare uno stato</small>
					</div>
					<div class="small-12 medium-6 large-6 columns">
						<label>Data inizio
						<input name="data_inizio" readonly="readonly" id="data_inizio" type="text" value="" required/>
						<small class="error">Inserire la data di inizio del concorso</small>	
						</label>
					</div>
					<div class="small-12 medium-6 large-6 columns">			
						<label>Data fine
						<input name="data_fine" readonly="readonly"  id="data_fine" type="text" value="" required/>
						<small class="error">Inserire la data di fine del concorso</small>	
						</label>
					</div>
				</div>
				<div class="row">  
					<div class="small-12 columns">
						<button class="radius button" id="button_go" type="button" onclick="javascript:gestionePeriodi('<?echo base_url();?>', $('#idperiodo').val())">Procedi</button>
						<div id="spinner" class="spinner" style="display:none;">
						    <img id="img-spinner" src="/assets/img-backend/ajax-loader.gif" alt="Loading"/>
						</div>
					</div>
				</div>	
			</form>	
			<!---*** FORM PER LA AGGIUNTA DI UN NUOVO PERIODO - E' NASCOSTO DI DEFAULT ***-->
		</div>
	</div>
	<div class="row">
		<div class="small-12 column">
			<table id="periodi" style="width: 100%;">
			</table>
		</div>
	</div>
	<div class="row">  
		<div class="small-12 columns">
			<button onclick="javascript:editPeriodo('_NEW')"  class="radius button" id="button_aggiungi" type="button" >Aggiungi un periodo</button>
		</div>
	</div>
</section>
<script>

/* crea un nuovo periodo o edita un periodo */
function editPeriodo(idperiodo, descrizione, data_inizio, data_fine, idstato){
	$("#idperiodo").val(idperiodo);
	$("#descrizione").val(descrizione);
	$("#data_inizio").val(data_inizio);
	$("#data_fine").val(data_fine);	
	if (idstato){
		$("#stati_disponibili").val(idstato);
	}
	else {
		$("#stati_disponibili").val($('#stati_disponibili option').filter(function () { return $(this).html() == "Attivo"; }).val());
	}
	$("#js-edit-perido").toggle(200);
	//$("#js-edit-perido").attr('action','<?echo site_url()."periodo/gestione/".$concorso_dati['concorsoID']?>/'+idperiodo);
}	

function popolaPeriodi(where,base_url){
	$.ajax({
	    type:'GET',
	    dataType: 'json',
	    url:base_url+'impostazione_periodi_e_concorsi/get_dati_periodi/',
	    data:{},
	    success:function(response){
	    //attenzione. success (questo sopra) vuol dire che la chiamata ha avuto successo, che il server ha risposto senza dare errori fatali, non vuol dire che la funzione abbia fatto quel che doveva fare
	    	if (response.result=='success'){
		    	var dati = response.output;
		        //console.log(response.output); 
		        $(where).html("");
		        $(where).append("<thead><tr><th>ID</th><th>Descrizione</th><th>Data inizio</th><th>Data fine</th><th></th><th></th></tr></thead>");
		        for (var i = 0; i < dati.length; i++) {
			        $(where).append("<tr><td>"+dati[i]['periodoID']+"</td><td><a href='<?=site_url()?>prova/"+dati[i]['periodoID']+"'>"+dati[i]['descrizione']+"</a></td><td>"+dati[i]['data_inizio']+"</td><td>"+dati[i]['data_fine']+"</td><td><a onclick=\"javascript:editPeriodo("+dati[i]['periodoID']+",'"+dati[i]['descrizione']+"','"+dati[i]['data_inizio']+"','"+dati[i]['data_fine']+"','"+dati[i]['FK_periodi_stati']+"')\" class=\"edit_periodo\"><i class=\"fi-wrench\"></i></a></td><td><a onclick=\"javascript:deleteEntry("+dati[i]['periodoID']+",'impostazione_periodi_e_concorsi','<?echo base_url();?>')\"><i class=\"fi-trash\"></i></a></td></tr>");
			        
				}
	        }
	        else {
	        }
	    },
	    error:function(response){
	    }
	});		
}

function deleteEntry(id,context, base_url){
	if (confirm("Sei sicuro di voler rimuovere questo periodo?")){
		$.ajax({
		    type:'POST',
		    dataType: 'json',
		    url:base_url+context+'/delete/'+id,
		    data:{},
		    success:function(response){
		    //attenzione. success (questo sopra) vuol dire che la chiamata ha avuto successo, che il server ha risposto senza dare errori fatali, non vuol dire che la funzione abbia fatto quel che doveva fare
		    	if (response.result=='success'){
			        printMsg('success',response.message); 
			        popolaPeriodi("#periodi","<?echo base_url();?>");
		        }
		        else {
			        printMsg('alert',response.message); 
		        }
		    },
		    error:function(response){
		        printMsg('alert','Errore durante la chiamata'); 
		    }
		});	
	}	
}

function gestionePeriodi(base_url, idperiodo){
	$.ajax({
	    type:'POST',
	    dataType: 'json',
		beforeSend: function(){
			  $("#spinner").show();
			  $("#button_go").attr("disabled","disabled");
			  $("#button_go").addClass("disabled");
		},
		complete: function(){
		  $("#spinner").hide();
		  $("#button_go").removeAttr("disabled");
		  $("#button_go").removeClass("disabled");
		},
	    url:base_url+'impostazione_periodi_e_concorsi/gestione_periodi/<?=$concorso_dati['concorsoID']?>/'+idperiodo,
	    data:{
		    descrizione : $("#descrizione").val(),
		    data_inizio : $("#data_inizio").val(),
		    data_fine : $("#data_fine").val(),
		    idstato: $("#stati_disponibili").val()
	    },
	    success:function(response){
	    //attenzione. success (questo sopra) vuol dire che la chiamata ha avuto successo, che il server ha risposto senza dare errori fatali, non vuol dire che la funzione abbia fatto quel che doveva fare
	    	if (response.result=='success'){
		        printMsg('success',response.message); 
		        popolaPeriodi("#periodi","<?echo base_url();?>");
		        $("#js-edit-perido").hide();
		        
	        }
	        else {
		        printMsg('alert',response.message); 
	        }
	    },
	    error:function(response){
	        printMsg('alert','Errore durante la chiamata'); 
	    }
	});	
}

$("input[name='data_inizio']").datetimepicker({
				showSecond: true,
				timeFormat: 'HH:mm:ss',
				dateFormat: 'dd/mm/yy',
				minDate: '<?=DateTime::createFromFormat('Y-m-d G:i:s', $concorso_dati["data_inizio"])->format('d/m/Y')?>',			
				maxDate: '<?=DateTime::createFromFormat('Y-m-d G:i:s', $concorso_dati["data_fine"])->format('d/m/Y')?>'
			});
$("input[name='data_fine']").datetimepicker({
				showSecond: true,
				timeFormat: 'HH:mm:ss',
				dateFormat: 'dd/mm/yy',
				minDate: '<?=DateTime::createFromFormat('Y-m-d G:i:s', $concorso_dati["data_inizio"])->format('d/m/Y')?>',			
				maxDate: '<?=DateTime::createFromFormat('Y-m-d G:i:s', $concorso_dati["data_fine"])->format('d/m/Y')?>'
			});


$(document).ready(function(){
	popolaPeriodi("#periodi","<?echo base_url();?>");
	popolaStati("#stati_disponibili","<?echo base_url();?>");		
});
</script>

