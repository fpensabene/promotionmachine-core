<?php

// To use reCAPTCHA, you need to sign up for an API key pair for your site.
// link: http://www.google.com/recaptcha/admin
$config['recaptcha_site_key'] = '6LcT6QkTAAAAAA-tyFPjDkGaXLykrr8pqwgMmobS';
$config['recaptcha_secret_key'] = '6LcT6QkTAAAAAOX2ONH-kPxjp73d261kn9Ze6XiQ';

// reCAPTCHA supported 40+ languages listed here:
// https://developers.google.com/recaptcha/docs/language
$config['recaptcha_lang'] = 'it';

/* End of file recaptcha.php */
/* Location: ./application/config/recaptcha.php */
