<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Estrazioni extends MY_Controller_backend {

	public function __construct()
    {	
        parent::__construct();
        // Your own constructor code
        if (!$this->ion_auth->logged_in() OR !$this->ion_auth->amministrazione(2,2))
		{
		    return show_error('You must be an administrator to view this page.');
		}		
        $this->load->database();
    } 
    
	public function index(){
		
		// Qui andiamo a chiamare il controller con la lista delle estrazioni per ogni specifica istanza
		
		
		// concorso UHU Renature
		redirect(CLASSE_ESTRAZIONE);
	}
		
	
}
