<?php defined('BASEPATH') OR exit('No direct script access allowed');
	/****************************************************************************************************** 
	 * CLASSE: Partecipa
	 * 
	 * Gestione del frontend di promotion machine
	 *
	 * Gestione_utenti: CLASSE DEFINITE DENTRO  /application/core/MY_Controller.php
	 ***************************************************************************************************** */
class Partecipa extends MY_Controller_frontend {
	
	function __construct()
	{
		parent::__construct();
		$this->load->database(); 
		$this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url','language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
		$this->lang->load('auth');
		$this->data['nome'] = $this->nome_concorso;
		$this->permessiVisualizzazione(TRUE);

        // serve per dare una classe al body
		$this->data['body_class'] = 'testata-big';

	}


	/****************************************************************************************************** 
	 * CLASSE: index()
	 
	 E' la pagina personale: 
	 
	 * se non sei loggato fai il login
	 ** se sei loggato fai vedere una lista delle prove disponibili
	 
	 ***************************************************************************************************** */

	//redirect if needed, otherwise display the user list
	function index($message='')
	{
		if (!$this->ion_auth->logged_in())
		{
			
			/*UTENTE NON HA ESEGUITO ACCESSO*/
			
			//TITOLO PAGINA
			$this->data['titolo_pagina'] = "Partecipa";
	
			//DEFINISCO LE REGOLE DI VALIDAZIONE
			$this->form_validation->set_rules('identity', 'Identity', 'required');
			$this->form_validation->set_rules('password', 'Password', 'required');

			// CONTROLLO L'ESITO DEL FORM DI LOGIN
			
			if ($this->form_validation->run() == true)
			{
				
				// STO FACENDO IL LOGIN

				
				$remember = (bool) $this->input->post('remember'); //check for "remember me"
	
				if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember))
				{
					// LOGIN ESEGUITO CON SUCCESSO
					
					$this->session->set_flashdata('message', $this->ion_auth->messages());
					//
					/*
						vedi function redirect($uri = '', $method = 'auto', $code = NULL) in system/helpers/url_helper.
						se utilizzo come secondo parametro "refresh", la pagina viene semplicemente postata,
						altrimenti utilizza http://en.wikipedia.org/wiki/Post/Redirect/Get
						
					*/
					redirect('partecipa', 'location');
					
				}
				else
				{
					// LOGIN ERRATO!
					
					//redirect them back to the login page
					$this->session->set_flashdata('message', $this->ion_auth->errors());
					//
					redirect('partecipa', 'refresh'); //use redirects instead of loading views for compatibility with MY_Controller libraries
				}
			}
			else
			{
				
				// NON STO FACENDO IL LOGIN OPPURE HO SBAGLIATO LE CREDENZIALI - RITORNO ALLA VIEW CON IL LOGIN
				

				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
	
				$this->data['identity'] = array('name' => 'identity',
					'id' => 'identity',
					'type' => 'text',
					'value' => $this->form_validation->set_value('identity'),
				);
				$this->data['password'] = array('name' => 'password',
					'id' => 'password',
					'type' => 'password',
				);		
				
				//$this->data['urls'] = $this->url;		
				
				$this->load->view('frontend/includes/header', $this->data);
				$this->load->view('frontend/partecipa/login', $this->data);
				$this->load->view('frontend/includes/footer',$this->data);
				
			}
		}
		else
		{
			
			// HO GIA' ESEGUITO L'ACCESSO
			
			// serve per dare una classe al body
			$this->data['body_class'] = 'testata-small';
			
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			/*devo farmi restituire tutte le prove 
			
			- attive
			- di tutti i periodi attuali
			- che non siano di iscrizione 
			- solo quelle passate e presenti
			- skippando quelle mai svolte
			*/
			$dati_prove = $this->get_dati_prove('Attive', NULL, 'iscrizione', TRUE, TRUE);
			
			$this->load->view('frontend/includes/header', $this->data);
			if ($dati_prove['size'] < 1){
				
				/* NON CI SONO PROVE PRESENTI O PASSATE */
				$this->load->view('frontend/partecipa/nessuna-prova', $this->data);
				
			}
			else {
				$this->data['dati_prove_size'] = sizeof($dati_prove['output']);
				$this->data['dati_prove'] = json_encode($dati_prove['output']);
				$this->data['message'] = $message;
				$this->load->view('frontend/partecipa/lista-prove', $this->data);
				
			}
			
			$this->load->view('frontend/includes/footer', $this->data);
			
		}
	}
	
	function iscrizione_confermata(){
		
		//TITOLO PAGINA
		$this->data['titolo_pagina'] = "Account attivato";
		// serve per dare una classe al body
		$this->data['body_class'] = 'testata-small';
		
		$this->load->view('frontend/includes/header', $this->data);
		$this->load->view('frontend/partecipa/iscrizione-confermata', $this->data);
		$this->load->view('frontend/includes/footer', $this->data);
		
	}

	/****************************************************************************************************** 
	 CLASSE: Partecipa
	 METODO: logout()
	 ***************************************************************************************************** */

	//log the user out
	function logout()
	{
		
		//TITOLO PAGINA
		$this->data['titolo_pagina'] = "Logout";


		//log the user out
		$logout = $this->ion_auth->logout();

		//redirect them to the login page
		$this->session->set_flashdata('message', $this->ion_auth->messages());
		
		redirect('partecipa', 'refresh');
	}

	/****************************************************************************************************** 
	 CLASSE: Partecipa
	 METODO: change_password()
	 ***************************************************************************************************** */
	function change_password()
	{
		$this->form_validation->set_rules('old', $this->lang->line('change_password_validation_old_password_label'), 'required');
		$this->form_validation->set_rules('new', $this->lang->line('change_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
		$this->form_validation->set_rules('new_confirm', $this->lang->line('change_password_validation_new_password_confirm_label'), 'required');

		if (!$this->ion_auth->logged_in())
		{
			redirect('partecipa', 'refresh');
		}

		$user = $this->ion_auth->user()->row();

		if ($this->form_validation->run() == false)
		{
			//display the form
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			$this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
			$this->data['old_password'] = array(
				'name' => 'old',
				'id'   => 'old',
				'type' => 'password',
			);
			$this->data['new_password'] = array(
				'name' => 'new',
				'id'   => 'new',
				'type' => 'password',
				'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
			);
			$this->data['new_password_confirm'] = array(
				'name' => 'new_confirm',
				'id'   => 'new_confirm',
				'type' => 'password',
				'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
			);
			$this->data['user_id'] = array(
				'name'  => 'user_id',
				'id'    => 'user_id',
				'type'  => 'hidden',
				'value' => $user->id,
			);
			//$this->data['urls'] = $this->url;
			//render
			$this->_render_page('frontend/partecipa/change_password', $this->data);
		}
		else
		{
			$identity = $this->session->userdata('identity');

			$change = $this->ion_auth->change_password($identity, $this->input->post('old'), $this->input->post('new'));

			if ($change)
			{
				//if the password was successfully changed
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				
				$this->logout();
			}
			else
			{
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				
				redirect('partecipa/change_password', 'refresh');
			}
		}
	}
	/****************************************************************************************************** 
	 
	 CLASSE: Partecipa
	 METODO: recupera_password()
	 
	 ***************************************************************************************************** */
	function recupera_password()
	{
		//setting validation rules by checking wheather identity is username or email
		if($this->config->item('identity', 'ion_auth') == 'username' )
		{
		   $this->form_validation->set_rules('email', $this->lang->line('recupera_password_username_identity_label'), 'required');
		}
		else
		{
		   $this->form_validation->set_rules('email', $this->lang->line('recupera_password_validation_email_label'), 'required|valid_email');
		}


		if ($this->form_validation->run() == false)
		{
			//setup the input
			$this->data['email'] = array('name' => 'email',
				'id' => 'email',
			);

			if ( $this->config->item('identity', 'ion_auth') == 'username' ){
				$this->data['identity_label'] = $this->lang->line('recupera_password_username_identity_label');
			}
			else
			{
				$this->data['identity_label'] = $this->lang->line('recupera_password_email_identity_label');
			}

			//set any errors and display the form
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			//$this->data['urls'] = $this->url;
			
			// serve per dare una classe al body
			$this->data['body_class'] = 'testata-small';
			
			$this->load->view('frontend/includes/header', $this->data);
			$this->load->view('frontend/partecipa/recupera_password', $this->data);
			//$this->_render_page('frontend/partecipa/recupera_password', $this->data);
			$this->load->view('frontend/includes/footer', $this->data);
			
			
		}
		else
		{
			// get identity from username or email
			if ( $this->config->item('identity', 'ion_auth') == 'username' ){
				$identity = $this->ion_auth->where('username', strtolower($this->input->post('email')))->users()->row();
			}
			else
			{
				$identity = $this->ion_auth->where(array(
						'email' => strtolower($this->input->post('email')), 
						'active' => 1)
					)->users()->row();
				
			}
	            	if(empty($identity)) {

	            		if($this->config->item('identity', 'ion_auth') == 'username')
		            	{
                                   $this->ion_auth->set_message('recupera_password_username_not_found');
		            	}
		            	else
		            	{
		            	   $this->ion_auth->set_message('recupera_password_email_not_found');
		            	}

		                $this->session->set_flashdata('message', $this->ion_auth->messages());
		                
                		redirect("auth/recupera_password", 'refresh');
            		}
			
			//run the forgotten password method to email an activation code to the user
			$forgotten = $this->ion_auth->forgotten_password($identity->{$this->config->item('identity', 'ion_auth')},'frontend');
			if ($forgotten)
			{
				//if there were no errors
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				
				redirect("partecipa", 'refresh'); //we should display a confirmation page here instead of the login page
			}
			else
			{
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				
				redirect("partecipa/recupera_password", 'refresh');
			}
		}
	}
	/****************************************************************************************************** 
	 
	 CLASSE: Partecipa
	 METODO: reset_password($code = NULL)
	 	//reset password - final step for forgotten password
	 	
	 ***************************************************************************************************** */
	public function reset_password($code = NULL)
	{
		if (!$code)
		{
			show_404();
		}

		$user = $this->ion_auth->forgotten_password_check($code);

		if ($user)
		{
			//if the code is valid then display the password reset form

			$this->form_validation->set_rules('new', $this->lang->line('reset_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
			$this->form_validation->set_rules('new_confirm', $this->lang->line('reset_password_validation_new_password_confirm_label'), 'required');

			if ($this->form_validation->run() == false)
			{
				//display the form

				//set the flash data error message if there is one
				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

				$this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
				$this->data['new_password'] = array(
					'name' => 'new',
					'id'   => 'new',
				'type' => 'password',
					'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
				);
				$this->data['new_password_confirm'] = array(
					'name' => 'new_confirm',
					'id'   => 'new_confirm',
					'type' => 'password',
					'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
				);
				$this->data['user_id'] = array(
					'name'  => 'user_id',
					'id'    => 'user_id',
					'type'  => 'hidden',
					'value' => $user->id,
				);
				$this->data['csrf'] = $this->_get_csrf_nonce();
				$this->data['code'] = $code;
				//$this->data['urls'] = $this->url;
				//render
				$this->_render_page('frontend/partecipa/reset_password', $this->data);
			}
			else
			{
				// do we have a valid request?
				if ($this->_valid_csrf_nonce() === FALSE || $user->id != $this->input->post('user_id'))
				{

					//something fishy might be up
					$this->ion_auth->clear_forgotten_password_code($code);

					show_error($this->lang->line('error_csrf'));

				}
				else
				{
					// finally change the password
					$identity = $user->{$this->config->item('identity', 'ion_auth')};

					$change = $this->ion_auth->reset_password($identity, $this->input->post('new'));

					if ($change)
					{
						//if the password was successfully changed
						$this->session->set_flashdata('message', $this->ion_auth->messages());
						
						redirect("partecipa", 'refresh');
					}
					else
					{
						$this->session->set_flashdata('message', $this->ion_auth->errors());
						
						redirect('partecipa/reset_password/' . $code, 'refresh');
					}
				}
			}
		}
		else
		{
			//if the code is invalid then send them back to the forgot password page
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			
			redirect("partecipa/recupera_password", 'refresh');
		}
	}
	/****************************************************************************************************** 
	 
	 CLASSE: Partecipa
	 METODO: activate($id, $code=false)
	 
	 Attiva un utente che ha cliccato sul link nella mail 
	 
	 ***************************************************************************************************** */

	//activate the user
	function activate($id, $code=false)
	{
		if ($code !== false)
		{
			$activation = $this->ion_auth->activate($id, $code);
		}
		else if ($this->ion_auth->amministrazione(2,0))
		{
			//vuol dire che, se sono loggato con un account amministratore, posso attivare un account semplicemente da activate/$id
			$activation = $this->ion_auth->activate($id);
		}

		if ($activation)
		{
			//redirect them to the auth page
			$this->session->set_flashdata('message', $this->ion_auth->messages());
			
			if ($this->ion_auth->is_contestant($id)){
				$this->attribuisciPunteggioIscrizione($id);
			}
			redirect('partecipa/iscrizione_confermata', 'refresh');;
		}
		else
		{
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			
			redirect("partecipa", 'refresh');
		}
		
	}
	
	/****************************************************************************************************** 
	 * CLASSE: deactivate($id = NULL)
	 

	 
	 ***************************************************************************************************** */
	function deactivate($id = NULL)
	{
		redirect('partecipa', 'refresh');
	}
	
	/****************************************************************************************************** 
	 * CLASSE: iscriviti()
	 
	 
	 ***************************************************************************************************** */
	function iscriviti()
	{
		
		// serve per dare una classe al body
		$this->data['body_class'] = 'testata-small';
		
		
		$id_prova_iscrizione = $this->getIDProvaIscrizione();
		$this->load->model('Prove');
		if (!$id_prova_iscrizione){
			$this->session->set_flashdata('message', 'Le iscrizioni sono chiuse.');
			redirect('partecipa','refresh');
		}
		$dati_prova_iscrizione = $this->Prove->get($id_prova_iscrizione);
		
		if ($dati_prova_iscrizione->FK_prove_stati != $this->getStatoIDbyDescrizione('Attivo')){

			$this->session->set_flashdata('message', 'Le iscrizioni sono chiuse.');
			redirect('partecipa','refresh');	

		}
		
		//TITOLO PAGINA
		$this->data['titolo_pagina'] = "Iscriviti a UHU RECYCLE";
		
		/*
		if (!$this->ion_auth->logged_in() || !$this->ion_auth->amministrazione(2,0))
		{
			redirect('auth', 'refresh');
		}
		*/
		$tables = $this->config->item('tables','ion_auth');

		//validate form input
		$this->form_validation->set_rules('first_name', $this->lang->line('iscriviti_validation_fname_label'), 'required');
		$this->form_validation->set_rules('last_name', $this->lang->line('iscriviti_validation_lname_label'), 'required');
		$this->form_validation->set_rules('email', $this->lang->line('iscriviti_validation_email_label'), 'required|valid_email|matches[email_confirm]');
		$this->form_validation->set_rules('email_confirm', $this->lang->line('iscriviti_validation_email_confirm_label'), 'required|valid_email');
		$this->form_validation->set_rules('password', $this->lang->line('iscriviti_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
		$this->form_validation->set_rules('password_confirm', $this->lang->line('iscriviti_validation_password_confirm_label'), 'required');
		$this->form_validation->set_rules('codice_fiscale', $this->lang->line('iscriviti_validation_codice_fiscale_label'), 'required|callback_check_codice_fiscale');
		$this->form_validation->set_rules('data_nascita', $this->lang->line('iscriviti_validation_data_nascita_label'), 'required|callback_check_data_nascita');
		$this->form_validation->set_rules('informativa_b', $this->lang->line('iscriviti_validation_informativa_b'), 'required');
		
		$iscrizione_univoca = TRUE;
		
		if ($this->form_validation->run() == true)
		{
			$username = strtolower($this->input->post('first_name')) . ' ' . strtolower($this->input->post('last_name'));
			$email    = strtolower($this->input->post('email'));
			$password = $this->input->post('password');
			$data_nascita = DateTime::createFromFormat('d/m/Y', $_POST['data_nascita']);
			$additional_data = array(
				'nome' => $this->input->post('first_name'),
				'cognome'  => $this->input->post('last_name'),
				'FK_user_concorso' => $this->input->post('idconcorso'),
				'cf' => $this->input->post('codice_fiscale'),
				'data_nascita' => $data_nascita->format('Y-m-d 00:00:00'),
				'informativa_a' => $this->input->post('informativa_a'),
				'informativa_b' => $this->input->post('informativa_b'),
				'informativa_c' => $this->input->post('informativa_c')	
			);
			
			// controlliamo di non avere altri contestants con lo stesso codice fiscale oppure email
			$iscrizione_univoca = $this->checkUnivocitaIscrizione($this->input->post('codice_fiscale'),$this->input->post('email'));
			
			// se è tutto ok faccio l'iscrizione
			if ($iscrizione_univoca){
				if ($this->ion_auth->register($username, $password, $email, $additional_data, NULL, 'frontend')){
					$this->session->set_flashdata('message', $this->ion_auth->messages());
					redirect("partecipa", 'refresh');
				}
			}
		}	
		/*
		display the create user form
		set the flash data error message if there is one
		questa combinazione di operatori ternari corrisponde a:
		c'e' un validation_errors()? 
			se SI, assegnalo a $this->data['message']
			se NO, c'e' un $this->ion_auth->errors()?
				se SI, assegnalo a $this->data['message']
				se NO, l'iscrizione NON e' univoca?
					se SI, assegna a $this->data['message'] il messaggio di errore corrispondente
					se NO, assegna a $this->data['message'] $this->session->flashdata('message') (a questo punto e' $this->ion_auth->messages(), vedi sopra)
		*/
		$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : (!$iscrizione_univoca ? "ATTENZIONE! Utente gi&agrave; iscritto con questo codice fiscale o email." :$this->session->flashdata('message'))));
		
		$this->data['first_name'] = array(
			'name'  => 'first_name',
			'id'    => 'first_name',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('first_name'),
		);
		$this->data['last_name'] = array(
			'name'  => 'last_name',
			'id'    => 'last_name',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('last_name'),
		);
		$this->data['email'] = array(
			'name'  => 'email',
			'id'    => 'email',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('email'),
		);
		$this->data['email_confirm'] = array(
			'name'  => 'email_confirm',
			'id'    => 'email_confirm',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('email_confirm'),
		);
		$this->data['password'] = array(
			'name'  => 'password',
			'id'    => 'password',
			'type'  => 'password',
			'value' => $this->form_validation->set_value('password'),
		);
		$this->data['password_confirm'] = array(
			'name'  => 'password_confirm',
			'id'    => 'password_confirm',
			'type'  => 'password',
			'value' => $this->form_validation->set_value('password_confirm'),
		);
		$this->data['codice_fiscale'] = array(
			'name'  => 'codice_fiscale',
			'id'    => 'codice_fiscale',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('codice_fiscale'),
		);
		$this->data['data_nascita'] = array(
			'name'  => 'data_nascita',
			'id'    => 'data_nascita',
			'type'  => 'text',
			'readonly' => 'readonly',
			'value' => $this->form_validation->set_value('data_nascita'),
		);
		$this->data['informativa_a'] = array(
			'name' => 'informativa_a',
			'id'   => 'informativa_a',
			'type' => 'checkbox',
			'value' => 1
		);
		$this->data['informativa_b'] = array(
			'name' => 'informativa_b',
			'id'   => 'informativa_b',
			'type' => 'checkbox',
			'value' => 1

		);
		$this->data['informativa_c'] = array(
			'name' => 'informativa_c',
			'id'   => 'informativa_c',
			'type' => 'checkbox',
			'value' => 1

		);
		if ($this->input->post('informativa_a')){
			$this->data['informativa_a']['checked'] = 'checked';
		}
		if ($this->input->post('informativa_b')){
			$this->data['informativa_b']['checked'] = 'checked';
		}
		if ($this->input->post('informativa_c')){
			$this->data['informativa_c']['checked'] = 'checked';
		}
		$this->load->model('Concorsi');
		$data = $this->Concorsi->get_all();
		$obj_dati_concorso = $data[0];	
		
		$this->data['concorsoID'] = array(
			'name'  => 'idconcorso',
			'id'    => 'idconcorso',
			'type'  => 'hidden',
			'value' => $obj_dati_concorso->concorsoID,
		);
		//$this->data['urls'] = $this->url;
		$this->load->view('frontend/includes/header', $this->data);
		$this->_render_page('frontend/partecipa/iscriviti', $this->data);
		$this->load->view('frontend/includes/footer');
	}
	
	//edit a user
	function edit_user($id)
	{
		
		$this->data['title'] = "Edit User";

		if (!$this->ion_auth->logged_in() || (!$this->ion_auth->amministrazione(2,0) && !($this->ion_auth->user()->row()->id == $id)))
		{	
			
			redirect('partecipa', 'refresh');
		}

		$user = $this->ion_auth->user($id)->row();
		$groups=$this->ion_auth->groups()->result_array();
		$currentGroups = $this->ion_auth->get_users_groups($id)->result();

		//validate form input
		$this->form_validation->set_rules('first_name', $this->lang->line('edit_user_validation_fname_label'), 'required');
		$this->form_validation->set_rules('last_name', $this->lang->line('edit_user_validation_lname_label'), 'required');
		$this->form_validation->set_rules('codice_fiscale', $this->lang->line('iscriviti_validation_codice_fiscale_label'), 'required|callback_check_codice_fiscale');
		$this->form_validation->set_rules('data_nascita', $this->lang->line('iscriviti_validation_data_nascita_label'), 'required|callback_check_data_nascita');
		if (isset($_POST) && !empty($_POST))
		{
			// do we have a valid request?
			if ($this->_valid_csrf_nonce() === FALSE || $id != $this->input->post('id'))
			{
				show_error($this->lang->line('error_csrf'));
			}

			//update the password if it was posted
			if ($this->input->post('password'))
			{
				$this->form_validation->set_rules('password', $this->lang->line('edit_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
				$this->form_validation->set_rules('password_confirm', $this->lang->line('edit_user_validation_password_confirm_label'), 'required');
			}

			if ($this->form_validation->run() === TRUE)
			{
				$data_nascita = DateTime::createFromFormat('d/m/Y', $_POST['data_nascita']);
				$data = array(
					'nome' => $this->input->post('first_name'),
					'cognome'  => $this->input->post('last_name'),
					'cf' => $this->input->post('codice_fiscale'),
					'data_nascita' => $data_nascita->format('Y-m-d 00:00:00')
				);

				//update the password if it was posted
				if ($this->input->post('password'))
				{
					$data['password'] = $this->input->post('password');
				}



				// Only allow updating groups if user is admin
				if ($this->ion_auth->amministrazione(2,0))
				//$this->ion_auth->amministrazione(2,0)
				{
					//Update the groups user belongs to
					$groupData = $this->input->post('groups');

					if (isset($groupData) && !empty($groupData)) {

						$this->ion_auth->remove_from_group('', $id);

						foreach ($groupData as $grp) {
							$this->ion_auth->add_to_group($grp, $id);
						}

					}
				}

			//check to see if we are updating the user
			   if($this->ion_auth->update($user->id, $data))
			    {
			    	//redirect them back to the admin page if admin, or to the base url if non admin
				    $this->session->set_flashdata('message', $this->ion_auth->messages() );
				    
				    if ($this->ion_auth->amministrazione(2,0))
					{
						redirect('partecipa/gestione', 'refresh');
					}
					else
					{
						redirect('/', 'refresh');
					}

			    }
			    else
			    {
			    	//redirect them back to the admin page if admin, or to the base url if non admin
				    $this->session->set_flashdata('message', $this->ion_auth->errors() );
				    
				    if ($this->ion_auth->amministrazione(2,0))
					{
						redirect('auth', 'refresh');
					}
					else
					{
						redirect('/', 'refresh');
					}

			    }

			}
		}

		//display the edit user form
		$this->data['csrf'] = $this->_get_csrf_nonce();

		//set the flash data error message if there is one
		$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

		//pass the user to the view
		$this->data['user'] = $user;
		$this->data['groups'] = $groups;
		$this->data['currentGroups'] = $currentGroups;

		$this->data['first_name'] = array(
			'name'  => 'first_name',
			'id'    => 'first_name',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('first_name', $user->nome),
		);
		$this->data['last_name'] = array(
			'name'  => 'last_name',
			'id'    => 'last_name',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('last_name', $user->cognome),
		);
		$this->data['password'] = array(
			'name' => 'password',
			'id'   => 'password',
			'type' => 'password'
		);
		$this->data['password_confirm'] = array(
			'name' => 'password_confirm',
			'id'   => 'password_confirm',
			'type' => 'password'
		);
		$this->data['codice_fiscale'] = array(
			'name'  => 'codice_fiscale',
			'id'    => 'codice_fiscale',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('codice_fiscale'),
		);
		$this->data['data_nascita'] = array(
			'name'  => 'data_nascita',
			'id'    => 'data_nascita',
			'type'  => 'text',
			'readonly' => 'readonly',
			'value' => $this->form_validation->set_value('data_nascita'),
		);		
		$this->data['informativa_a'] = array(
			'name' => 'informativa_a',
			'id'   => 'informativa_a',
			'type' => 'checkbox'
		);
		$this->data['informativa_b'] = array(
			'name' => 'informativa_b',
			'id'   => 'informativa_b',
			'type' => 'checkbox'
		);
		$this->data['informativa_c'] = array(
			'name' => 'informativa_c',
			'id'   => 'informativa_c',
			'type' => 'checkbox'
		);
		//$this->data['urls'] = $this->url;
		$this->load->view('frontend/includes/header', $this->data);
		$this->_render_page('frontend/partecipa/edit_user', $this->data);
		$this->load->view('frontend/includes/footer', $this->data);
	}

	// create a new group
	function create_group()
	{
		redirect('partecipa', 'refresh');
	}

	//edit a group
	function edit_group($id)
	{
		redirect('partecipa', 'refresh');
	}

	function gestione()
	{
		redirect('partecipa', 'refresh');
	}
	
	function contestants()
	{
		redirect('partecipa', 'refresh');
	}

}
