<?php defined('BASEPATH') OR exit('No direct script access allowed');

	/****************************************************************************************************** 
	 * CLASSE: Admin
	 * 
	 * Sezione Admin del backend di promotion machine
	 *
	 ***************************************************************************************************** */

class Admin extends MY_Controller_backend {

	/****************************************************************************************************** 
	 * Constructor
	 * 
	 *
	 *
	 ***************************************************************************************************** */
	 
	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url','language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');		
		
		
		
	}
	
	
	/****************************************************************************************************** 
	 * index()
	 * redirect if needed, otherwise display the user list
	 *
	 *
	 ***************************************************************************************************** */

	function index()
	{

		if (!$this->ion_auth->logged_in())
		{
			//redirect them to the login page
			redirect('admin/login', 'refresh');
		}
		elseif (!$this->ion_auth->amministrazione(2,2)) //remove this elseif if you want to enable this for non-admins
		{
			//redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			$this->data['users'] = $this->ion_auth->users()->result();
			foreach ($this->data['users'] as $k => $user)
			{
				$this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
			}
			$this->load->view('backend/includes/header');
			$this->_render_page('backend/admin/index', $this->data);
			$this->load->view('backend/includes/footer');
		}
	}
	
	/******************************************************************************************************
	 * login()
	 * log the user in
	 *
	 *
	 ***************************************************************************************************** */

	function login()
	{
		$this->data['title'] = "Login";
		
		

		//validate form input
		// Imposta le regole di validazione
		$this->form_validation->set_rules('identity', 'Identity', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() == true)
		{
			//check to see if the user is logging in
			//check for "remember me"
			$remember = (bool) $this->input->post('remember');

			if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember))
			{
				//if the login is successful
				//redirect them back to the home page
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				
				
				// se l'utente appartiene al gruppo admin oppure moderators entra nel backend
				if ($this->ion_auth->amministrazione(2,2)){
					redirect('admin', 'refresh');
				}
				else {
					redirect('partecipa', 'refresh');
				}
			}
			else
			{
				//if the login was un-successful
				//redirect them back to the login page
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				
				redirect('admin/login', 'refresh'); //use redirects instead of loading views for compatibility with MY_Controller libraries
			}
		}
		else
		{
			//the user is not logging in so display the login page
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			$this->data['identity'] = array('name' => 'identity',
				'id' => 'identity',
				'type' => 'text',
				'value' => $this->form_validation->set_value('identity'),
			);
			$this->data['password'] = array('name' => 'password',
				'id' => 'password',
				'type' => 'password',
			);
			
			$this->load->view('backend/includes/header');
			$this->_render_page('backend/admin/login', $this->data);
			$this->load->view('backend/includes/footer');
		}
	}
	
	/******************************************************************************************************
	 * METODO: logout()
	 * 
	 *
	 *
	 ***************************************************************************************************** */
	 
	function logout()
	{
		$this->data['title'] = "Logout";

		//log the user out
		$logout = $this->ion_auth->logout();

		//redirect them to the login page
		$this->session->set_flashdata('message', $this->ion_auth->messages());
		
		redirect('admin/login', 'refresh');
	}
	
	/******************************************************************************************************
	 * METODO: change_password()
	 * 
	 *
	 *
	 ***************************************************************************************************** */

	function change_password()
	{
		$this->form_validation->set_rules('old', $this->lang->line('change_password_validation_old_password_label'), 'required');
		$this->form_validation->set_rules('new', $this->lang->line('change_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
		$this->form_validation->set_rules('new_confirm', $this->lang->line('change_password_validation_new_password_confirm_label'), 'required');

		if (!$this->ion_auth->logged_in())
		{
			redirect('admin/login', 'refresh');
		}

		$user = $this->ion_auth->user()->row();

		if ($this->form_validation->run() == false)
		{
			//display the form
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			$this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
			$this->data['old_password'] = array(
				'name' => 'old',
				'id'   => 'old',
				'type' => 'password',
			);
			$this->data['new_password'] = array(
				'name' => 'new',
				'id'   => 'new',
				'type' => 'password',
				'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
			);
			$this->data['new_password_confirm'] = array(
				'name' => 'new_confirm',
				'id'   => 'new_confirm',
				'type' => 'password',
				'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
			);
			$this->data['user_id'] = array(
				'name'  => 'user_id',
				'id'    => 'user_id',
				'type'  => 'hidden',
				'value' => $user->id,
			);

			//render
			$this->_render_page('backend/admin/change_password', $this->data);
		}
		else
		{
			$identity = $this->session->userdata('identity');

			$change = $this->ion_auth->change_password($identity, $this->input->post('old'), $this->input->post('new'));

			if ($change)
			{
				//if the password was successfully changed
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				
				$this->logout();
			}
			else
			{
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				
				redirect('admin/change_password', 'refresh');
			}
		}
	}
	
	/******************************************************************************************************
	 * METODO: recupera_password()
	 * 
	 *
	 *
	 ***************************************************************************************************** */
	 
	function recupera_password()
	{
		//setting validation rules by checking wheather identity is username or email
		if($this->config->item('identity', 'ion_auth') == 'username' )
		{
		   $this->form_validation->set_rules('email', $this->lang->line('recupera_password_username_identity_label'), 'required');
		}
		else
		{
		   $this->form_validation->set_rules('email', $this->lang->line('recupera_password_validation_email_label'), 'required|valid_email');
		}


		if ($this->form_validation->run() == false)
		{
			//setup the input
			$this->data['email'] = array('name' => 'email',
				'id' => 'email',
			);

			if ( $this->config->item('identity', 'ion_auth') == 'username' ){
				$this->data['identity_label'] = $this->lang->line('recupera_password_username_identity_label');
			}
			else
			{
				$this->data['identity_label'] = $this->lang->line('recupera_password_email_identity_label');
			}

			//set any errors and display the form
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->_render_page('backend/admin/recupera_password', $this->data);
		}
		else
		{
			// get identity from username or email
			if ( $this->config->item('identity', 'ion_auth') == 'username' ){
				$identity = $this->ion_auth->where('username', strtolower($this->input->post('email')))->users()->row();
			}
			else
			{
				$identity = $this->ion_auth->where('email', strtolower($this->input->post('email')))->users()->row();
			}
	            	if(empty($identity)) {

	            		if($this->config->item('identity', 'ion_auth') == 'username')
		            	{
                                   $this->ion_auth->set_message('recupera_password_username_not_found');
		            	}
		            	else
		            	{
		            	   $this->ion_auth->set_message('recupera_password_email_not_found');
		            	}

		                $this->session->set_flashdata('message', $this->ion_auth->messages());
		                
                		redirect("admin/recupera_password", 'refresh');
            		}

			//run the forgotten password method to email an activation code to the user
			$forgotten = $this->ion_auth->forgotten_password($identity->{$this->config->item('identity', 'ion_auth')}, 'backend');

			if ($forgotten)
			{
				//if there were no errors
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				
				redirect("admin/login", 'refresh'); //we should display a confirmation page here instead of the login page
			}
			else
			{
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				
				redirect("admin/recupera_password", 'refresh');
			}
		}
	}
	/******************************************************************************************************
	 * METODO: reset_password($code = NULL)
	 * 
	 * 	//reset password - final step for forgotten password
	 *
	 ***************************************************************************************************** */

	public function reset_password($code = NULL)
	{
		if (!$code)
		{
			show_404();
		}

		$user = $this->ion_auth->forgotten_password_check($code);

		if ($user)
		{
			//if the code is valid then display the password reset form

			$this->form_validation->set_rules('new', $this->lang->line('reset_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
			$this->form_validation->set_rules('new_confirm', $this->lang->line('reset_password_validation_new_password_confirm_label'), 'required');

			if ($this->form_validation->run() == false)
			{
				//display the form

				//set the flash data error message if there is one
				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

				$this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
				$this->data['new_password'] = array(
					'name' => 'new',
					'id'   => 'new',
				'type' => 'password',
					'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
				);
				$this->data['new_password_confirm'] = array(
					'name' => 'new_confirm',
					'id'   => 'new_confirm',
					'type' => 'password',
					'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
				);
				$this->data['user_id'] = array(
					'name'  => 'user_id',
					'id'    => 'user_id',
					'type'  => 'hidden',
					'value' => $user->id,
				);
				$this->data['csrf'] = $this->_get_csrf_nonce();
				$this->data['code'] = $code;

				//render
				$this->_render_page('backend/admin/reset_password', $this->data);
			}
			else
			{
				// do we have a valid request?
				if ($this->_valid_csrf_nonce() === FALSE || $user->id != $this->input->post('user_id'))
				{

					//something fishy might be up
					$this->ion_auth->clear_forgotten_password_code($code);

					show_error($this->lang->line('error_csrf'));

				}
				else
				{
					// finally change the password
					$identity = $user->{$this->config->item('identity', 'ion_auth')};

					$change = $this->ion_auth->reset_password($identity, $this->input->post('new'));

					if ($change)
					{
						//if the password was successfully changed
						$this->session->set_flashdata('message', $this->ion_auth->messages());
						
						redirect("admin/login", 'refresh');
					}
					else
					{
						$this->session->set_flashdata('message', $this->ion_auth->errors());
						
						redirect('admin/reset_password/' . $code, 'refresh');
					}
				}
			}
		}
		else
		{
			//if the code is invalid then send them back to the forgot password page
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			
			redirect("admin/recupera_password", 'refresh');
		}
	}

	/******************************************************************************************************
	 * METODO: activate($id, $code=false)
	 * 
	 * 	//activate the user
	 *
	 ***************************************************************************************************** */
	
	function activate($id, $code=false)
	{
		if ($code !== false)
		{
			$activation = $this->ion_auth->activate($id, $code);
		}
		else if ($this->ion_auth->amministrazione(2,0))
		{
			$activation = $this->ion_auth->activate($id);
		}

		if ($activation)
		{
			//redirect them to the auth page
			$this->session->set_flashdata('message', $this->ion_auth->messages());
				
			if ($this->ion_auth->is_contestant($id)){
				$this->attribuisciPunteggioIscrizione($id);
			}
			redirect("admin/gestione_utenti_backend", 'refresh');
		}
		else
		{
			//redirect them to the forgot password page
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			
			redirect("admin/recupera_password", 'refresh');
		}
	}

	/******************************************************************************************************
	 * METODO: deactivate($id = NULL)
	 * 
	 * 	
	 *
	 ***************************************************************************************************** */
	 	
	function deactivate($id = NULL)
	{
		if (!$this->ion_auth->logged_in() || !$this->ion_auth->amministrazione(2,0))
		{
			//redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}

		$id = (int) $id;

		$this->load->library('form_validation');
		$this->form_validation->set_rules('confirm', $this->lang->line('deactivate_validation_confirm_label'), 'required');
		$this->form_validation->set_rules('id', $this->lang->line('deactivate_validation_user_id_label'), 'required|alpha_numeric');
		
		if ($this->form_validation->run() == FALSE)
		{
			// insert csrf check
			$this->data['csrf'] = $this->_get_csrf_nonce();
			$this->data['user'] = $this->ion_auth->user($id)->row();
		}
		else
		{
			// do we really want to deactivate?
			if ($this->input->post('confirm') == 'yes')
			{
				// do we have a valid request?
				if ($this->_valid_csrf_nonce() === FALSE || $id != $this->input->post('id'))
				{
					show_error($this->lang->line('error_csrf'));
				}

				// do we have the right userlevel?
				if ($this->ion_auth->logged_in() && $this->ion_auth->amministrazione(2,0))
				{
					$deactivation = $this->ion_auth->deactivate($id);
				}
			}

			//se sto modificando un member, ridireziono a gestione contestants, altrimenti a gestione utenti
			
			if ($deactivation) {
				$this->session->set_flashdata('message', $this->ion_auth->messages());	
				$this->annullaPunteggioIscrizione($id);				
			}
			else {
				$this->session->set_flashdata('message', $this->ion_auth->errors());
			}
			
			if ($this->ion_auth->amministrazione(2,2,$id)){
				redirect('admin/gestione_utenti_backend', 'refresh');
			}
			else {
				redirect('admin/gestione_partecipanti', 'refresh');
			}

		}
	}

	/******************************************************************************************************
	 * METODO: activate_simple($id, $code=false)
	 * 
	 * 	
	 *
	 ***************************************************************************************************** */

	function activate_simple($id, $code=false)
	{
		if ($code !== false)
		{
			$activation = $this->ion_auth->activate($id, $code);
		}
		else if ($this->ion_auth->amministrazione(2,0))
		{
			$activation = $this->ion_auth->activate($id);
		}

		if ($activation)
		{
			$this->session->set_flashdata('message', $this->ion_auth->messages());
			
		}
		else
		{
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			
		}
		redirect("admin/gestione_utenti_backend", 'refresh');
	}

	/******************************************************************************************************
	 * METODO: deactivate_simple($id)
	 * //deactivate the user
	 * 	
	 *
	 ***************************************************************************************************** */
	 
	
	function deactivate_simple($id)
	{

		$id = (int) $id;
		if ($this->ion_auth->amministrazione(2,0))
		{
			$deactivation = $this->ion_auth->deactivate($id);
		}

		if ($deactivation)
		{
			$this->session->set_flashdata('message', $this->ion_auth->messages());
			
		}
		else
		{
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			
		}	
		redirect("admin/gestione_utenti_backend", 'refresh');
	}

	/******************************************************************************************************
	 * METODO: gestione_stato_utente_backend()
	 * Serve per gestire lo stato attivo/disattivo di un utente che può accedere al backend
	 *
	 *
	 ***************************************************************************************************** */

	function gestione_stato_utente_backend($id)
	{
		if (!$this->ion_auth->logged_in() || !$this->ion_auth->amministrazione(2,0))
		{
			//redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else {		
			if (isset($_POST) && !empty($_POST))
			{	
				if ($this->input->post('status') == 'attivo')
				{
					redirect('admin/activate_simple/'.$id, 'refresh');
				}
				elseif ($this->input->post('status') == 'disattivo')
				{
					redirect('admin/deactivate_simple/'.$id, 'refresh');
				}
				else {
					
				}
			}
			else 
			{	
				$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
				$this->data['user'] = $this->ion_auth->user($id)->row();
				$this->load->view('backend/includes/header');
				$this->_render_page('backend/admin/gestione_stato_utente_backend', $this->data);
				$this->load->view('backend/includes/footer');
			}
		}
	}
	
	/******************************************************************************************************
	 * METODO: cancellazione_utente_backend()
	 * Serve per cancellare un utente che può accedere al backend
	 *
	 *
	 ***************************************************************************************************** */
	
	function cancellazione_utente_backend($id)
	{
		if (!$this->ion_auth->logged_in() || !$this->ion_auth->amministrazione(2,0))
		{
			//redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else {		
			if (isset($_POST) && !empty($_POST))
			{	
				if ($this->input->post('confirm') == 'yes')
				{
					$delete = $this->ion_auth->delete_user($id);
					
					if ($delete) {
						$this->session->set_flashdata('message', $this->ion_auth->messages());					
					}
					else {
						$this->session->set_flashdata('message', $this->ion_auth->errors());
					}
									
					
				}
					
				redirect('admin/gestione_utenti_backend', 'refresh');		
			}
			else 
			{	
				$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
				$this->data['user'] = $this->ion_auth->user($id)->row();
				$this->load->view('backend/includes/header');
				$this->_render_page('backend/admin/cancellazione_utente_backend', $this->data);
				$this->load->view('backend/includes/footer');
			}
		}
	}

	/******************************************************************************************************
	 * METODO: nuovo_utente_backend()
	 * Serve per iscrivere un utente che può accedere al backend
	 *
	 *
	 ***************************************************************************************************** */

	 /*
		 - QUI ENTRANO SOLO GLI ADMIN
		 - cambiare il nome della funzione
		 - gestire solo i campi giusti idem la validazione
		 
		 */

	function nuovo_utente_backend()
	{
		// TITOLO DELLA PAGINA
		$this->data['title'] = "Nuovo utente backed";
		
		
		if (!$this->ion_auth->logged_in() || !$this->ion_auth->amministrazione(2,0))
		{
			//redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		
		$groups=$this->ion_auth->groups()->result_array();
		
		$tables = $this->config->item('tables','ion_auth');

		//Regole di validazione per i campi del form
		$this->form_validation->set_rules('first_name', $this->lang->line('iscriviti_validation_fname_label'), 'required');
		$this->form_validation->set_rules('last_name', $this->lang->line('iscriviti_validation_lname_label'), 'required');
		$this->form_validation->set_rules('email', $this->lang->line('iscriviti_validation_email_label'), 'required|valid_email|is_unique['.$tables['users'].'.email]|matches[email_confirm]');
		$this->form_validation->set_rules('email_confirm', $this->lang->line('iscriviti_validation_email_confirm_label'), 'required|valid_email|is_unique['.$tables['users'].'.email]');
		$this->form_validation->set_rules('password', $this->lang->line('iscriviti_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
		$this->form_validation->set_rules('password_confirm', $this->lang->line('iscriviti_validation_password_confirm_label'), 'required');


		// se la validazione ha avuto esito positivo setta delle variabili
		if ($this->form_validation->run() == true)
		{
			$username = strtolower($this->input->post('first_name')) . ' ' . strtolower($this->input->post('last_name'));
			$email    = strtolower($this->input->post('email'));
			$password = $this->input->post('password');

			$additional_data = array(
				'nome' => $this->input->post('first_name'),
				'cognome'  => $this->input->post('last_name'),
				'FK_user_concorso' => $this->input->post('idconcorso')
			);
			$groupData = $this->input->post('groups');
			$id = $this->ion_auth->register($username, $password, $email, $additional_data, $groupData, 'backend');	
			
			/*
				perche' assegniamo i gruppi qui, dato che basterebbe passarli alla funzione register? 
					forse per evitare l'assegnazione al gruppo di default?	
					no, se alla funzione del model passo i gruppi, non assegna quello di default
				per ora lo tolgo
			*/		
			if($id)
			{	
			/*$groupData = $this->input->post('groups');
			if (isset($groupData) && !empty($groupData)) {
				//rimuovo l'utente da tutti i gruppi
				$this->ion_auth->remove_from_group('', $id);
				//e lo assegno a quelli passati in post
				foreach ($groupData as $grp) {
					$this->ion_auth->add_to_group($grp, $id);
				}	
			}	*/							
			$this->session->set_flashdata('message', $this->ion_auth->messages());
			
			redirect('admin/gestione_utenti_backend', 'refresh');
			}

		}
		else
		{
			//display the create user form
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

			$this->data['first_name'] = array(
				'name'  => 'first_name',
				'id'    => 'first_name',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('first_name'),
			);
			$this->data['last_name'] = array(
				'name'  => 'last_name',
				'id'    => 'last_name',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('last_name'),
			);
			$this->data['email'] = array(
				'name'  => 'email',
				'id'    => 'email',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('email'),
			);
			$this->data['email_confirm'] = array(
				'name'  => 'email_confirm',
				'id'    => 'email_confirm',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('email_confirm'),
			);
			$this->data['password'] = array(
				'name'  => 'password',
				'id'    => 'password',
				'type'  => 'password',
				'value' => $this->form_validation->set_value('password'),
			);
			$this->data['password_confirm'] = array(
				'name'  => 'password_confirm',
				'id'    => 'password_confirm',
				'type'  => 'password',
				'value' => $this->form_validation->set_value('password_confirm'),
			);
			$this->data['groups'] = $groups;			
			$gruppi_postati = ($this->input->post('groups')?$this->input->post('groups'):array());
			$this->data['currentGroups'] = $gruppi_postati ;
			$this->load->model('Concorsi');
			$data = $this->Concorsi->get_all();
			$obj_dati_concorso = $data[0];	
			
			$this->data['concorsoID'] = array(
				'name'  => 'idconcorso',
				'id'    => 'idconcorso',
				'type'  => 'hidden',
				'value' => $obj_dati_concorso->concorsoID,
			);

						
			$this->load->view('backend/includes/header', $this->data);
			$this->_render_page('backend/admin/nuovo_utente_backend', $this->data);
			$this->load->view('backend/includes/footer');
	
		}
	}

	/****************************************************************************************************** 
	 * METODO: modifica_utente_backend()
	 * 
	 * edita i dati di un utente
	 *
	 ***************************************************************************************************** */

	function modifica_utente_backend($id)
	{
		$this->data['title'] = "Edit User";

		// controllo che l'utente sia loggato e sia admin
		if (!$this->ion_auth->logged_in() || (!$this->ion_auth->amministrazione(2,0)))
		{
			redirect('admin', 'refresh');
		}

		$user = $this->ion_auth->user($id)->row();
		$groups=$this->ion_auth->groups()->result_array();
		$currentGroups = $this->ion_auth->get_users_groups($id)->result();

		//Regole di validazione
		$this->form_validation->set_rules('first_name', $this->lang->line('edit_user_validation_fname_label'), 'required');
		$this->form_validation->set_rules('last_name', $this->lang->line('edit_user_validation_lname_label'), 'required');
		
		if (isset($_POST) && !empty($_POST))
		{
			// do we have a valid request?
			if ($this->_valid_csrf_nonce() === FALSE || $id != $this->input->post('id'))
			{
				show_error($this->lang->line('error_csrf'));
			}

			//Se l'utente del backend ha inserito una password allora setto le validation rules per la password - COSI POSSO EVITARE DI CAMBIARE LA PASSWORD
			if ($this->input->post('password'))
			{
				$this->form_validation->set_rules('password', $this->lang->line('edit_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
				$this->form_validation->set_rules('password_confirm', $this->lang->line('edit_user_validation_password_confirm_label'), 'required');
			}
			
			// ora faccio la validazione
			if ($this->form_validation->run() === TRUE)
			{
				$data = array(
					'nome' => $this->input->post('first_name'),
					'cognome'  => $this->input->post('last_name')
				);

				//update the password if it was posted
				if ($this->input->post('password'))
				{
					$data['password'] = $this->input->post('password');
				}

				// ESEGUE LA MODIFICA E CONTROLLA L'ESITO
			   if($this->ion_auth->update($user->id, $data))
			    {				    
				    // ESITO POSITIVO DELLA MODIFICA DEI DATI UTENTE
				    
				    $groupData = $this->input->post('groups');

					if (isset($groupData) && !empty($groupData)) {
	
						$this->ion_auth->remove_from_group('', $id);
	
						foreach ($groupData as $grp) {
							$this->ion_auth->add_to_group($grp, $id);
						}
	
					}
				    
			    	//redirect them back to the admin page if admin, or to the base url if non admin
				    $this->session->set_flashdata('message', $this->ion_auth->messages() );
				    
				    redirect('admin/gestione_utenti_backend', 'refresh');
			    }
			    else
			    {
			    	//redirect them back to the admin page if admin, or to the base url if non admin
				    $this->session->set_flashdata('message', $this->ion_auth->errors() );
				    
					redirect('admin', 'refresh');
			    }

			}
		}
		
		
		
		// AZIONE DI DEFAULT
		

		//display the edit user form
		$this->data['csrf'] = $this->_get_csrf_nonce();

		//set the flash data error message if there is one
		$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

		//pass the user to the view
		$this->data['user'] = $user;
		$this->data['groups'] = $groups;
		$this->data['currentGroups'] = $currentGroups;
		$this->data['selectedGroupsIDs'] = $this->input->post('groups');
		$this->data['first_name'] = array(
			'name'  => 'first_name',
			'id'    => 'first_name',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('first_name', $user->nome),
		);
		$this->data['last_name'] = array(
			'name'  => 'last_name',
			'id'    => 'last_name',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('last_name', $user->cognome),
		);
		$this->data['password'] = array(
			'name' => 'password',
			'id'   => 'password',
			'type' => 'password'
		);
		$this->data['password_confirm'] = array(
			'name' => 'password_confirm',
			'id'   => 'password_confirm',
			'type' => 'password'
		);

		$this->load->view('backend/includes/header', $this->data);
		$this->_render_page('backend/admin/modifica_utente_backend', $this->data);
		$this->load->view('backend/includes/footer');
	}

	/****************************************************************************************************** 
	 * 
	 *
	 * Gestione gruppi / lista dei gruppi
	 *
	 * 
	 *
	 ***************************************************************************************************** */	
	function gestione_gruppi()
	{
		//var_dump($this->ion_auth->amministrazione(2,0));
		//if (!$this->ion_auth->logged_in()||!$this->ion_auth->is_admin())
		if (!$this->ion_auth->logged_in()||!$this->ion_auth->amministrazione(2,0))
		{
			//redirect them to the login page
			redirect('admin/login', 'refresh');
		}		
		else
		{
			//Assegno il titolo della pagina
			$this->data['title'] = 'Gestione Gruppi';
			
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			$this->data['gruppi'] = $this->ion_auth->groups()->result();
			
			$this->load->view('backend/includes/header', $this->data);
			$this->_render_page('backend/admin/gestione_gruppi', $this->data);
			$this->load->view('backend/includes/footer');
		}
	}

	/****************************************************************************************************** 
	 * METODO: crea_gruppo()
	 * 
	 * crea un nuovo gruppo di utenti
	 *
	 ***************************************************************************************************** */
	function crea_gruppo()
	{
		$this->data['title'] = $this->lang->line('create_group_title');

		if (!$this->ion_auth->logged_in() || !$this->ion_auth->amministrazione(2,0))
		{
			redirect('admin', 'refresh');
		}

		//validate form input
		$this->form_validation->set_rules('group_name', $this->lang->line('create_group_validation_name_label'), 'required|alpha_dash');

		if ($this->form_validation->run() == TRUE)
		{
			$new_group_id = $this->ion_auth->create_group($this->input->post('group_name'), $this->input->post('description'));
			if($new_group_id)
			{
				// check to see if we are creating the group
				// redirect them back to the admin page
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				
				redirect("admin/gestione_gruppi", 'refresh');
			}
		}
		else
		{
			//display the create group form
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

			$this->data['group_name'] = array(
				'name'  => 'group_name',
				'id'    => 'group_name',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('group_name'),
			);
			$this->data['description'] = array(
				'name'  => 'description',
				'id'    => 'description',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('description'),
			);
			$this->load->view('backend/includes/header');
			$this->_render_page('backend/admin/crea_gruppo', $this->data);
			$this->load->view('backend/includes/footer');
		}
	}

	/****************************************************************************************************** 
	 * METODO: modifica_gruppo($id)
	 * $id: immagino sia l'id del gruppo da modificare 
	 *
	 * modifica un gruppo di utenti
	 *
	 ***************************************************************************************************** */

	function modifica_gruppo($id)
	{
		// bail if no group id given
		if(!$id || empty($id))
		{
			redirect('admin', 'refresh');
		}

		$this->data['title'] = $this->lang->line('edit_group_title');

		if (!$this->ion_auth->logged_in() || !$this->ion_auth->amministrazione(2,0))
		{
			redirect('admin', 'refresh');
		}
		$tables = $this->config->item('tables','ion_auth');
		$group = $this->ion_auth->group($id)->row();

		//validate form input
		//|is_unique['.$tables['groups'].'.name]
		$this->form_validation->set_rules('group_name', $this->lang->line('edit_group_validation_name_label'), 'required|alpha_dash');

		if (isset($_POST) && !empty($_POST))
		{
			if ($this->form_validation->run() === TRUE)
			{
				$group_update = $this->ion_auth->update_group($id, $_POST['group_name'], $_POST['group_description']);

				if($group_update)
				{
					$this->session->set_flashdata('message', $this->lang->line('edit_group_saved'));
					
					redirect("admin/gestione_gruppi/", 'refresh');
				}
				else
				{
					$this->session->set_flashdata('message', $this->ion_auth->errors());
				}
				
			}
		}

		//set the flash data error message if there is one
		$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

		//pass the user to the view
		$this->data['group'] = $group;

		$readonly = $this->config->item('admin_group', 'ion_auth') === $group->name ? 'readonly' : '';

		$this->data['group_name'] = array(
			'name'  => 'group_name',
			'id'    => 'group_name',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('group_name', $group->name),
			$readonly => $readonly,
		);
		$this->data['group_description'] = array(
			'name'  => 'group_description',
			'id'    => 'group_description',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('group_description', $group->description),
		);
		$this->load->view('backend/includes/header');
		$this->_render_page('backend/admin/modifica_gruppo', $this->data);
		$this->load->view('backend/includes/footer');
	}

	

	/****************************************************************************************************** 
	 * 
	 *
	 * Gestione utenti backend / lista degli utenti che hanno accesso al backend 
	 *
	 * 
	 *
	 ***************************************************************************************************** */	
	function gestione_utenti_backend()
	{
		//var_dump($this->ion_auth->amministrazione(2,0));
		//if (!$this->ion_auth->logged_in()||!$this->ion_auth->is_admin())
		if (!$this->ion_auth->logged_in()||!$this->ion_auth->amministrazione(2,0))
		{
			//redirect them to the login page
			redirect('admin/login', 'refresh');
		}		
		else
		{
			//Assegno il titolo della pagina
			$this->data['title'] = 'Gestione Utenti Backend';
			
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			$this->data['users'] = $this->ion_auth->users(array('moderator_group','admin'))->result();
			foreach ($this->data['users'] as $k => $user)
			{
				$this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
			}
			$this->load->view('backend/includes/header', $this->data);
			$this->_render_page('backend/admin/gestione_utenti_backend', $this->data);
			$this->load->view('backend/includes/footer');
		}
	}
	
	
	
	/****************************************************************************************************** 
	 * METODO: check_codice_fiscale($p)
	 * 
	 *
	 *
	 ***************************************************************************************************** */	
	 
	function check_codice_fiscale($p){ 
		$p = $this->input->post('codice_fiscale');
		if (preg_match('/^[a-z]{6}[0-9]{2}[a-z][0-9]{2}[a-z][0-9]{3}[a-z]$/i', $p)) return true;
		// it matched, see <ul> below for interpreting this regex   
		else {
			$this->form_validation->set_message('check_codice_fiscale','Errore codice fiscale');
			return false;
		} 
	}

}
