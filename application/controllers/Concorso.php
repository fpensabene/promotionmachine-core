<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	/****************************************************************************************************** 
	 * CLASSE: Concorso
	 * 
	 * Gestione dei parametri principali del concorso
	 *
	 ***************************************************************************************************** */

class Concorso extends MY_Controller {

	/****************************************************************************************************** 
	 * METODO: __construct()
	 * 
	 * 
	 *
	 ***************************************************************************************************** */
	 
	public function __construct()
    {	
        parent::__construct();      
        if (!$this->ion_auth->logged_in() OR !$this->ion_auth->amministrazione(2,2))
		{
		    return show_error('You must be an administrator to view this page.');
		}
		$this->load->model('Concorsi');  
		
		// Creo una variabile disponibile in tutti i metodi con il topNav
		$this->data['top_menu'] = $this->load->view('backend/includes/top_menu', NULL, TRUE);
				
    } 

	/****************************************************************************************************** 
	 * METODO:index()
	 * 
	 *
	 *
	 ***************************************************************************************************** */
	
	public function index()
	{
		// Controllo quanti concorsi abbiamo
		$totale = $this->Concorsi->count_all();
		
		
		$this->load->view('backend/includes/header');
		
		if ($totale){
			// Abbiamo un concorso...
			$this->load->view('backend/impostazione_concorso/concorso',$this->data); // qui non uso $this->data perchè?
		}
		else {
			// Non abbiamo un concorso ...
			$data['concorsoID'] = '_NEW';
			$this->load->view('backend/impostazione_concorso/concorso',$this->data); // qui non uso $this->data perchè?
		}
		$this->load->view('backend/includes/footer');
	}

	/****************************************************************************************************** 
	 * METODO: gestione($idConcorso)
	 *  ho dovuto rinominare la funzione, dato che ovviamente continuava a chiamare se stessa in loop.
	 *	get_dati_concorso() => funzione del controller MY_Controller
	 *
	 *
	 ***************************************************************************************************** */

	public function get_dati_concorso_json(){		
		$arr = $this->dati_concorso; // dati_concorso è un Array che contiene tutti i dati del concorso, viene generato in MY_Controller
		
		if($this->input->is_ajax_request()) {
			echo json_encode($arr); // Trasformo
		} else {
			echo 'non è una chiamata ajax';
		}
	}

	/****************************************************************************************************** 
	 * METODO: gestione($idConcorso)
	 * 
	 *
	 *
	 ***************************************************************************************************** */	
	
	public function gestione($idConcorso){
		$nome = $_POST['nome'];
		$data_inizio = DateTime::createFromFormat('d/m/Y G:i:s', $_POST['data_inizio']);
		$data_fine = DateTime::createFromFormat('d/m/Y G:i:s', $_POST['data_fine']);		
		$idstato = $_POST['stati_disponibili'];		
		$codice_analytics = $_POST['codice_analytics'];
		$admin_email = $_POST['admin_email'];
		if (!$nome OR !$data_inizio OR !$data_fine OR !$idstato OR !$admin_email ){
			$arr = array('result' => "alert", 'message' => "Uno o pi&ugrave; campi non sono stati validati.");
			echo json_encode($arr);
			return FALSE;
		}
		if ($data_inizio >= $data_fine){
			$arr = array('result' => "alert", 'message' => "La data di inizio deve essere precedente a quella di fine.");
			echo json_encode($arr);
			return FALSE;
		}
		$regolamento = "";
		$privacy = "";
		$errore_upload = false;
		$desc_errore_upload = "";
		$data;		
		$arr_update = array(
				'nome' => $nome,
				'data_inizio' => $data_inizio->format('Y-m-d G:i:s'),
				'data_fine' => $data_fine->format('Y-m-d G:i:s'),
				'FK_concorsi_stati' => 	$idstato,
				'codice_analytics' => $codice_analytics,
				'admin_email' => $admin_email
			);
		if (!(empty($_FILES['regolamento']['name'])&&empty($_FILES['privacy']['name']))){					
				$this->load->library('upload');					
				$config['allowed_types'] = 'pdf|doc|docx';
				$date = new DateTime();
				$now = $date->getTimestamp();
				if (!empty($_FILES['regolamento']['name'])){
					$config['file_name'] = str_replace(' ','_',$now."-".$_FILES['regolamento']['name']);
					$config['upload_path'] = './uploads/regolamenti/';
					$regolamento = $config['upload_path'].$config['file_name'];
					$arr_update['regolamento'] = $regolamento;
					$this->upload->initialize($config);
					if ( ! $this->upload->do_upload('regolamento')){
						$desc_errore_upload .= "Regolamento: " . $this->upload->display_errors() . "<br/>";						
						$errore_upload = true;						
					}					
						
				}
				if (!empty($_FILES['privacy']['name'])){
					$config['upload_path'] = './uploads/privacy/';
					$config['file_name'] = str_replace(' ','_',$now."-".$_FILES['privacy']['name']);
					$privacy = $config['upload_path'].$config['file_name'];
					$arr_update['privacy'] = $privacy;
					$this->upload->initialize($config);
					if ( ! $this->upload->do_upload('privacy')){
						$desc_errore_upload .= "Privacy: " . $this->upload->display_errors();	
						$errore_upload = true;
					}
				}
			}		
		else {
			
		}
		
		if (!$errore_upload){	
			if ($idConcorso!='_NEW'){
				$this->Concorsi->update($idConcorso, $arr_update);				
			}
			else {
				
				$idConcorso = $this->Concorsi->insert($arr_update);
			}
			$arr = array('result' => "success", 'message' => "Concorso aggiunto/modificato con successo");
		}
		else {
			$arr = array('result' => "alert", 'message' => $desc_errore_upload);
		}
		
		$dati_concorso = (array) $this->Concorsi->get($idConcorso);	
		if (isset($data)){
			$dati_concorso = array_merge($data, $dati_concorso);
		}
		echo json_encode($arr);
		return TRUE;
	}
}
