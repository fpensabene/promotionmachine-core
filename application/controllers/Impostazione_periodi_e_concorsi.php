<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Impostazione_periodi_e_concorsi extends MY_Controller_backend {

	/****************************************************************************************************** 
	 * Constructor
	 * 
	 *
	 *
	 ***************************************************************************************************** */
	public function __construct()
    {	
        parent::__construct();
        // Your own constructor code
        if (!$this->ion_auth->logged_in() OR !$this->ion_auth->amministrazione(2,2))
		{
		    return show_error('You must be an administrator to view this page.');
		}
        $this->load->database();        
        $this->load->model('Concorsi');
        $this->load->model('Periodi');
        //$this->output->enable_profiler(TRUE);	
    } 
	/****************************************************************************************************** 
	 * get_dati_periodi()
	 * 
	 *
	 *
	 ***************************************************************************************************** */	
	public function get_dati_periodi(){
		$arr_periodi = $this->Periodi->get_all();	
		
		for ($i = 0; $i<count($arr_periodi); $i++){	
			$arr_periodi[$i]->data_inizio = DateTime::createFromFormat('Y-m-d G:i:s', $arr_periodi[$i]->data_inizio)->format('d/m/Y H:i:s');
			$arr_periodi[$i]->data_fine = DateTime::createFromFormat('Y-m-d G:i:s', $arr_periodi[$i]->data_fine)->format('d/m/Y H:i:s');			
		}
		$arr = array('result' => 'success', 'output' => (array) $arr_periodi);
		echo json_encode($arr);
	}
	/****************************************************************************************************** 
	 * index()
	 * 
	 *
	 *
	 ***************************************************************************************************** */	
	public function index()
	{
		$this->data['nome'] = $this->nome_concorso;
		$this->load->view('backend/includes/header',$this->data);
		// controllo che ci sia un solo concorso
		$totale = $this->Concorsi->count_all();		
		if ($totale == 1 ){
			// ok abbiamo un solo concorso
			$arr_multidim_concorsi = $this->Concorsi->get_all();	
			$arr_concorso = $arr_multidim_concorsi[0];
			$this->data['concorso_dati'] = (array) $arr_concorso;
			$arr_periodi = $this->Periodi->get_all();	
			$this->data['periodi_dati'] = (array) $arr_periodi;
			$this->load->view('backend/impostazione_concorso/impostazione_periodi_e_concorsi',$this->data);
		}
		else {
			// c'è un errore abbiamo più di un concorso ...
			$this->data['msgtype'] = "alert";
			$this->data['output'] = "Errore durante il caricamento dei concorsi";
			$this->load->view('backend/impostazione_concorso/impostazione_periodi_e_concorsi',$this->data);
		}
		$this->load->view('backend/includes/footer');
	}
	/****************************************************************************************************** 
	 * gestione($idConcorso, $idPeriodo)
	 * 
	 *
	 *
	 ***************************************************************************************************** */
	public function gestione_periodi($idConcorso, $idPeriodo){
		$errore = false;
		if (!$_POST['descrizione']||!$_POST['data_inizio']||!$_POST['data_fine']||!$_POST['idstato']){
			$errore = true;
			$arr = array('result' => "alert", 'message' => "Uno o pi&ugrave; campi non sono stati compilati.");
		}
		if (!$errore){
			$descrizione = $_POST['descrizione'];
			$data_inizio_periodo = DateTime::createFromFormat('d/m/Y G:i:s', $_POST['data_inizio']);
			$data_fine_periodo = DateTime::createFromFormat('d/m/Y G:i:s', $_POST['data_fine']);	
			
			$arr_multidim_concorsi = $this->Concorsi->get_all();	
			$arr_concorso = $arr_multidim_concorsi[0];
			$data_inizio_concorso = DateTime::createFromFormat('Y-m-d G:i:s',$arr_concorso->data_inizio);
			$data_fine_concorso = DateTime::createFromFormat('Y-m-d G:i:s',$arr_concorso->data_fine);
	
			if (($data_inizio_periodo->getTimestamp() < $data_inizio_concorso->getTimestamp())||($data_inizio_periodo->getTimestamp() > $data_fine_concorso->getTimestamp())||($data_fine_periodo->getTimestamp() < $data_inizio_concorso->getTimestamp())||($data_fine_periodo->getTimestamp() > $data_fine_concorso->getTimestamp())){
				$errore = true;
				$arr = array('result' => "alert", 'message' => "Le date di inizio e fine concorso devono essere comprese nel range<br/>che va dal ".$data_inizio_concorso->format('d/m/Y G:i:00')." al ".$data_fine_concorso->format('d/m/Y G:i:00').".");
			}
			if ($data_inizio_periodo->getTimestamp()>=$data_fine_periodo->getTimestamp()){
				$errore = true;
				$arr = array('result' => "alert", 'message' => "La data di inizio periodo deve essere precedente rispetto alla data di fine periodo.");
			}		
		}
		if ($errore){
			echo json_encode($arr);
			return false;
		}
		
		$data;		
		$arr_gestione = array(
				'descrizione' => $descrizione,
				'data_inizio' => $data_inizio_periodo->format('Y-m-d G:i:s'),
				'data_fine' => $data_fine_periodo->format('Y-m-d G:i:s'),
				'FK_periodi_concorsi' => $idConcorso,
				'FK_periodi_stati' => $_POST['idstato']			
			);
		if ($idPeriodo!='_NEW'){
			$this->db->select('*');
			$this->db->from('CORE_prove');
			$this->db->where('FK_prove_periodi',$idPeriodo);
			$this->db->where("((data_inizio < '".$arr_gestione['data_inizio']."' OR data_inizio > '".$arr_gestione['data_fine']."') OR (data_fine < '".$arr_gestione['data_inizio']."' OR data_fine >'".$arr_gestione['data_fine']."'))", null, false);
			
			$query = $this->db->get();
			$prove_fuori_range = $query->result();
			$prove_fuori_range_nomi = "";
			if ($prove_fuori_range){
				foreach ($prove_fuori_range as $row){
					$prove_fuori_range_nomi.=$row->nome."<br/>";					 
				}
			}
			if(!$this->Periodi->update($idPeriodo, $arr_gestione))
				$errore = true;			
		}
		else {		
			$idPeriodo = $this->Periodi->insert($arr_gestione);	
			if (!$idPeriodo)
				$errore = true;
		}
		if (!$errore){
			$msgtype	= "success";
			$message = "Periodo aggiunto/modificato con successo.";
			if (isset($prove_fuori_range_nomi) AND $prove_fuori_range_nomi) {
				$message .= "<br/>Le date di inizio e/o fine delle seguenti prove sono state modificate:<br/><strong>".$prove_fuori_range_nomi."</strong>";
			}
		}
		else {
			$msgtype	= "alert";
			$message = "Errore durante l\'aggiunta/modifica del periodo";
		}
		
		$arr_periodi = $this->Periodi->get_all();	
		$data['periodi_dati'] = (array) $arr_periodi;		
		
		$arr = array('result' => $msgtype, 'message' => $message, 'output' => $data);
		echo json_encode($arr);
		return true;
	}
	/****************************************************************************************************** 
	 * index()
	 * 
	 *
	 *
	 ***************************************************************************************************** */	
	public function delete($idConcorso){				
		if($this->Periodi->delete($idConcorso)){
			$arr = array('result' => 'success', 'message' => 'Periodo rimosso con successo');
		}
		else {
			$arr = array('result' => 'error', 'message' => 'Errore durante la cancellazione del periodo');
		}
		echo json_encode($arr);	
	}
}