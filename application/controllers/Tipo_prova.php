<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tipo_prova extends MY_Controller {

	public function __construct()
    {	
        parent::__construct();
        if (!$this->ion_auth->logged_in() OR !$this->ion_auth->amministrazione(2,2))
		{
		    return show_error('You must be an administrator to view this page.');
		}
        // Your own constructor code
        $this->load->database();   
        $this->load->model('tipi_prova');     
        //$this->output->enable_profiler(TRUE);	
        // Creo una variabile disponibile in tutti i metodi con il topNav
		$this->data['top_menu'] = $this->load->view('backend/includes/top_menu', NULL, TRUE);	
    } 
	
	public function index()
	{
		$this->load->view('backend/includes/header');
		$this->load->view('backend/tipo_prova/index',$this->data);
		$this->load->view('backend/includes/footer');
	}
	
	public function get_dati_tipi_prova(){		
		$arr_tipi_prova = $this->tipi_prova->get_all();
		$this->load->model('Controllers_model');
		
		for ($i = 0; $i<count($arr_tipi_prova); $i++){
			//$this->Controllers_model->get(1);
			$arr_tipi_prova[$i]->nome_controller_frontend = $this->Controllers_model->get($arr_tipi_prova[$i]->FK_CORE_tipoProva_CORE_controllers)->nome_controller_frontend;
			$arr_tipi_prova[$i]->nome_controller_backend = $this->Controllers_model->get($arr_tipi_prova[$i]->FK_CORE_tipoProva_CORE_controllers)->nome_controller_backend;
			
		}
		$arr = array('result' => 'success', 'output' => (array) $arr_tipi_prova);
		echo json_encode($arr);
	}
	
	public function gestione($idTipoProva){
		if (!$_POST['descrizione']||$_POST['punteggio']==null||!$_POST['controller_frontend']){
			$arr = array('result' => "alert", 'message' => "Uno o pi&ugrave; campi non sono stati compilati.");
		}
		else {
			$descrizione = $_POST['descrizione'];
			$punteggio = $_POST['punteggio'];
			$controller = $_POST['controller_frontend'];
			$errore = true;
			$data;
			$arr_gestione = array(
					'punteggio' => $punteggio,
					'descrizione' => $descrizione,
					'FK_CORE_tipoProva_CORE_controllers' => $controller
			);
			if ($idTipoProva!='_NEW'){
				if($this->tipi_prova->update($idTipoProva, $arr_gestione))
					$errore = false;			
			}
			else {			
				if ($idTipoProva = $this->tipi_prova->insert($arr_gestione))
					$errore = false;
			}
			if (!$errore){
				$msgtype	= "success";
				$message = "Tipo prova aggiunto/modificato con successo";
			}
			else {
				$msgtype	= "alert";
				$message = "Errore durante l\'aggiunta/modifica del tipo prova";
			}   
			$arr = array('result' => $msgtype, 'message' => $message);  
		}
		
		echo json_encode($arr);
	}
	
	public function delete($idTipoProva){				
		if($this->tipi_prova->delete($idTipoProva)){
			$arr = array('result' => 'success', 'message' => 'Tipo prova rimossa con successo');
		}
		else {
			$arr = array('result' => 'error', 'message' => 'Errore durante la cancellazione del tipo prova');
		}
		echo json_encode($arr);	
	}


}