<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stato extends MY_Controller {

	public function __construct()
    {	
        parent::__construct();
        if (!$this->ion_auth->logged_in() OR !$this->ion_auth->amministrazione(2,2))
		{
		    return show_error('You must be an administrator to view this page.');
		}
        // Your own constructor code
        $this->load->database();  
        $this->load->model('Stati');      
        //$this->output->enable_profiler(TRUE);		
    } 
	
	public function index()
	{
		$this->load->view('backend/includes/header');
		$this->load->view('stato/index');
		$this->load->view('backend/includes/footer');
	}
	
	// Restituisce lista degli stati nella tabella stati del db
	public function get_dati_stati_json(){		
		$arr_stati = $this->Stati->get_all();
		$arr = array('result' => 'success', 'output' => (array) $arr_stati);
		// stampa un json con la lista degli stati
		echo json_encode($arr);
	}

}