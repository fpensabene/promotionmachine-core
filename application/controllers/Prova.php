<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Prova extends MY_Controller_backend {

	public function __construct()
    {	
        parent::__construct();
        // Your own constructor code
        if (!$this->ion_auth->logged_in() OR !$this->ion_auth->amministrazione(2,2))
		{
		    return show_error('You must be an administrator to view this page.');
		}
        $this->load->database();        
        $this->load->model('Prove');
        $this->load->model('Periodi');           
        //$this->output->enable_profiler(TRUE);	
        	
    } 
	
	public function index($idPeriodo)
	{
		$this->data["idperiodo"] = $idPeriodo;
		$this->data["dati_periodo"] = $this->Periodi->get($idPeriodo);
		$this->data["idTipoProvaIscrizione"] = $this->getIDTipoProvaIscrizione();
		$this->load->view('backend/includes/header');
		$this->load->view('backend/impostazione_concorso/prove',$this->data);
		$this->load->view('backend/includes/footer');
	}

	/****************************************************************************************************** 
	 * METODO: get_dati_prove_json($attuali, $idPeriodo)
	 * 
	 *	$attuali -> flag per indicare se le prove devono essere attive
	 *	$idPeriodo -> il periodo a cui ci riferiamo 
	 * 	restituisce la lista di prove nel periodo
	 *	
	 *	la funzione get_dati_prove() è definita in MY_Controller
	 *
	 ***************************************************************************************************** */
	
	
	function get_dati_prove_json($statoProva, $idPeriodo){
		
		// restituisco un json
		
		if($this->input->is_ajax_request()) {			
			echo json_encode($this->get_dati_prove($statoProva, $idPeriodo));
		} else {
			echo 'non è una chiamata ajax';
		}
		
	}
	
	public function gestione($idPeriodo, $idProva){
		$errore = false;
		$arr = array();
		if (!$_POST['nome']||!$_POST['descrizione']||!$_POST['idstato']||!$_POST['tipo_prova']||$_POST['ripetibile']=="N"&&!$_POST['ripetibile_quante_volte']){
			$errore = true;
			$arr = array('result' => "alert", 'message' => "Uno o pi&ugrave; campi non sono stati compilati.");
		}
		if (!$errore){
			$descrizione = $_POST['descrizione'];
			if ($_POST['data_inizio']&&$_POST['data_fine']){
				$data_inizio_prova = DateTime::createFromFormat('d/m/Y G:i:s', $_POST['data_inizio']);
				$data_fine_prova = DateTime::createFromFormat('d/m/Y G:i:s', $_POST['data_fine']);			
				
				$dati_periodo = $this->Periodi->get($idPeriodo);	
				$data_inizio_periodo = DateTime::createFromFormat('Y-m-d G:i:s',$dati_periodo->data_inizio);
				$data_fine_periodo = DateTime::createFromFormat('Y-m-d G:i:s',$dati_periodo->data_fine);
				
				if (($data_inizio_prova->getTimestamp() < $data_inizio_periodo->getTimestamp())||($data_inizio_prova->getTimestamp() > $data_fine_periodo->getTimestamp())||($data_fine_prova->getTimestamp() < $data_inizio_periodo->getTimestamp())||($data_fine_prova->getTimestamp() > $data_fine_periodo->getTimestamp())){
					$arr = array('result' => "alert", 'message' => "Le date di inizio e fine prova devono essere comprese nel range<br/>che va dal ".$data_inizio_periodo->format('d/m/Y G:i:s')." al ".$data_fine_periodo->format('d/m/Y G:i:s').".");
					$errore = true;
				}
				
				if ($data_inizio_prova->getTimestamp()>=$data_fine_prova->getTimestamp()){
					$arr = array('result' => "alert", 'message' => "La data di inizio prova deve essere precedente rispetto alla data di fine.");
					$errore = true;
				}
			}
		}	
		if ($errore){
			echo json_encode($arr);
			return false;
		}		
		$ripetibile = 0;
		if ($_POST['ripetibile'] == "N"){
			$ripetibile = $_POST["ripetibile_quante_volte"];
		}
		else {
			$ripetibile = $_POST['ripetibile'];
		}
		$data;
		$arr_gestione = array(
				'FK_prove_periodi' => $idPeriodo,
				'FK_prove_stati' => $_POST['idstato'],
				'nome' => $_POST['nome'],
				'descrizione' => $_POST['descrizione'],
				'FK_prove_tipiProve' => $_POST['tipo_prova'],
				'ripetibile' => $ripetibile,
				'solo_maggiorenni' => $_POST['solo_maggiorenni']
			);
		if ($_POST['data_inizio']&&$_POST['data_fine']){
			$arr_gestione['data_inizio'] = $data_inizio_prova->format('Y-m-d G:i:s');
			$arr_gestione['data_fine'] = $data_fine_prova->format('Y-m-d G:i:s');
		}
		else {
			$arr_gestione['data_inizio'] = NULL;
			$arr_gestione['data_fine'] = NULL;
		}
		if ($idProva!='_NEW'){
			if(!$this->Prove->update($idProva, $arr_gestione))
				$errore = true;			
		}
		else {	
			$idProva = $this->Prove->insert($arr_gestione);	
			
			if (!$idProva)
				$errore = true;
		}
		if (!$errore){
			$msgtype	= "success";
			$message = "Prova aggiunta/modificata con successo";
		}
		else {
			$msgtype	= "alert";
			$message = "Errore durante l'aggiunta/modifica della prova";
		}
		
		$arr = array('result' => $msgtype, 'message' => $message);
		echo json_encode($arr);
	}
	
	public function delete($idProva){		
		//oggetto con la prova con id passato
		$prova_obj = $this->Prove->get($idProva);		
		$this->load->model('Punteggio');
		//oggetti punteggio che fanno riferimento alla prova passata
		$punteggi_obj = $this->Punteggio->get_many_by(array('FK_punteggio_prova'),$prova_obj->provaID);
		//array con tutti gli id dei punteggi in oggetto
		$output = array_map(function ($object) { return $object->punteggioID; }, $punteggi_obj);	
		//ora devo prendere tutti gli upload con FK_upload_punteggio nel range di $output
		//id separati da virgola. X, Y, Z
		$csv_punteggi = implode(', ', $output);	
		$this->db->select('*');
		$this->db->from('upload');
		$this->db->where("FK_upload_punteggio in (".$csv_punteggi.")", null, false);
		$query = $this->db->get();
		if ($query) {
			$files_obj = $query->result();			
			if ($files_obj){
				$output = array_map(function ($object) { return $object->filecol_1; }, $files_obj);
			}
		}
		
		if($this->Prove->delete($idProva)){
			$arr = array('result' => 'success', 'message' => 'Prova rimossa con successo');
			foreach($output as $elemento){
				unlink("../httpdocs/".substr($elemento, 1));
			}
		}
		else {
			$arr = array('result' => 'error', 'message' => 'Errore durante la cancellazione della prova');
		}
		echo json_encode($arr);	
	}
	
	public function gioca($slug,$idprova){
		$prova = $this->Prove->get($idprova);
		
		$this->load->model('Tipi_prova');			
		$tipo_prova = $this->Tipi_prova->get($prova->FK_prove_tipiProve);
		
		$this->load->model('Controllers_model');	
		$dati_controller = $this->Controllers_model->get($tipo_prova->FK_CORE_tipoProva_CORE_controllers);
		$controller_name = $dati_controller->nome_controller_frontend;
		
		redirect($controller_name.'/'.$slug);
	}

}