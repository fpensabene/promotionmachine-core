<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Assistenza extends MY_Controller_frontend {	
	
	
	function __construct()
	{
		parent::__construct();
		$this->load->database(); 
		$this->load->library(array('ion_auth','form_validation','recaptcha', 'email'));
		$this->load->helper(array('url','language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
		$this->data['nome'] = $this->nome_concorso;
		if($this->dati_concorso['output']['stato'] != 'attivo'){
			redirect('');
		}
		
		// serve per dare una classe al body
		$this->data['body_class'] = 'testata-small';
		
	}
	
	
	
	public function index()
	{
		//TITOLO PAGINA
		$this->data['titolo_pagina'] = "Richiedi Assistenza";
		$this->data['recaptcha_html'] = $this->recaptcha->getWidget();;
		$this->load->view('frontend/includes/header', $this->data);
		$this->load->view('frontend/assistenza/richiesta-assistenza', $this->data);
		$this->load->view('frontend/includes/footer', $this->data);
	}
	
	public function invia_richiesta(){
		$from = $_POST['from_address'];
		$from_conferma = $_POST['from_conferma'];
		$testo = $_POST['assistenza_body'];
		if (!filter_var($from, FILTER_VALIDATE_EMAIL)&&!filter_var($from_conferma, FILTER_VALIDATE_EMAIL)){
			$arr = array('result' => 'error', 'message' => 'L\'indirizzo mail inserito non &egrave; valido.', 'details' => 'email');
			echo json_encode($arr);
			return FALSE;
		}
		if ($from != $from_conferma){
			$arr = array('result' => 'error', 'message' => 'L\'indirizzo email non &egrave; stato confermato correttamente.', 'details' => 'email_confirm');
			echo json_encode($arr);
			return FALSE;
		}
		if (!filter_var($testo, FILTER_SANITIZE_STRING)){
			$arr = array('result' => 'error', 'message' => 'Devi inserire un messaggio.', 'details' => 'messaggio');
			echo json_encode($arr);
			return FALSE;
		}
		$testo = filter_var($testo, FILTER_SANITIZE_STRING);
		$recaptcha = $this->input->post('risposta_captcha');
		$response = $this->recaptcha->verifyResponse($recaptcha);
		//$this->Recaptcha->getIsValid()
		if (isset($response['success']) and $response['success'] === true) {
			$this->email->clear();
			$this->email->from($from);
			$this->email->to($this->dati_concorso["output"]["admin_email"]);
			$this->email->subject($this->dati_concorso["output"]["nome"] . ' - ' . 'Richiesta di assistenza');
			$this->email->message($testo);
			if ($this->email->send()){
				$arr = array('result' => 'success', 'message' => 'Email inviata.');
				echo json_encode($arr);
				return TRUE;
			}
			else {
				$arr = array('result' => 'alert', 'message' => 'Errore durante l\'invio della mail.');
				echo json_encode($arr);
				return FALSE;
			}
		}
		else {
			$arr = array('result' => 'alert', 'message' => 'Errore nel Captcha.', 'details' => 'captcha');
			echo json_encode($arr);
			return TRUE;
		}
		
		
		
		
		return false;
	}
}