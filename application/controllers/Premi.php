<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Premi extends MY_Controller_frontend  {	
	public function index()
	{
		
		//TITOLO PAGINA
		$this->data['titolo_pagina'] = "Premi in palio";
		
		// serve per dare una classe al body
		$this->data['body_class'] = 'testata-small';
		
		
		$this->load->view('frontend/includes/header', $this->data);
		$this->load->view('frontend/premi/index', $this->data);
		$this->load->view('frontend/includes/footer', $this->data);
	}
}