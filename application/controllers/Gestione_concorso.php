<?php defined('BASEPATH') OR exit('No direct script access allowed');

	/****************************************************************************************************** 
	 * CLASSE: gestione_concorso
	 * 
	 * Gestione partecipanti tramite backend
	 *
	 ***************************************************************************************************** */

class Gestione_concorso extends MY_Controller_backend {

	/****************************************************************************************************** 
	 * Constructor
	 * 
	 *
	 *
	 ***************************************************************************************************** */
	 
	function __construct()
	{
		parent::__construct();
		if (!$this->ion_auth->logged_in() OR !$this->ion_auth->amministrazione(2,2))
		{
		    return show_error('You must be an administrator to view this page.');
		}
		$this->load->database();
		$this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url','language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');		
		
		
		
	}
	
	/****************************************************************************************************** 
	 * 
	 * 
	 * gestione partecipanti al concorso
	 *
	 ***************************************************************************************************** */
	 
	function gestione_partecipanti()
	{

		if (!$this->ion_auth->logged_in()||!$this->ion_auth->amministrazione(2,2))
		{
			//redirect them to the login page
			redirect('admin/login', 'refresh');
		}		
		else
		{
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			$this->data['users'] = $this->ion_auth->users('contestants')->result();
			foreach ($this->data['users'] as $k => $user)
			{
				$this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
			}
			
			//recupero i dati statistici degli utenti
			//$this->load->model('Users');
			//$this->Users->get_many_by();
			$this->load->model('Punteggio');
			$this->data['tot_contestants'] = sizeof($this->data['users']);
			$this->data['active_contestants'] = 0;
			$this->data['inactive_contestants'] = 0;
			$this->data['lazy_contestants'] = 0;
			$idProva_iscrizione = $this->getIDProvaIscrizione();
			foreach($this->data['users'] as $user){
				if ($user->active == 1){
					$this->data['active_contestants']++;	
					$punteggio_obj = $this->Punteggio->get_by(array('FK_punteggio_user' => $user->id, 'FK_punteggio_prova !=' => $idProva_iscrizione));	
					if (!$punteggio_obj){
						$this->data['lazy_contestants']++;
						$user->lazy = 1;
					}
				}
				else {
					/*
						se l'utente non e' attivo, non e' detto che non si sia mai attivato, possiamo averlo disattivato noi.
						in questo caso pero' l'utente ha sicuramente una entry nella tabella punteggio che gli attribuisce il punteggio della prova di iscrizione,
						ottenuto al momento dell'attivazione. Dunque, chi e' disattivato MA ha questa entry nella tabella punteggio, non puo' ricevere la mail
					*/
					$punteggio_obj = $this->Punteggio->get_by(array('FK_punteggio_user' => $user->id, 'FK_punteggio_prova' => $idProva_iscrizione));
					if (!$punteggio_obj){
						$user->pending = 1;
					}
				}
			}
			
			foreach($this->data['users'] as $user){
				if ($user->active == 0){
					$this->data['inactive_contestants']++;
				}
			}
			
			$this->load->view('backend/includes/header');
			$this->_render_page('backend/gestione_concorso/gestione_partecipanti', $this->data);
			$this->load->view('backend/includes/footer');
		}
	}
	
	function modifica_partecipante($id)
	{
		
		$this->data['title'] = "Edit User";

		if (!$this->ion_auth->logged_in() || (!$this->ion_auth->amministrazione(2,0) && !($this->ion_auth->user()->row()->id == $id)))
		{	
			
			redirect('partecipa', 'refresh');
		}

		$user = $this->ion_auth->user($id)->row();
		$groups=$this->ion_auth->groups()->result_array();
		$currentGroups = $this->ion_auth->get_users_groups($id)->result();

		//validate form input
		$this->form_validation->set_rules('first_name', $this->lang->line('edit_user_validation_fname_label'), 'required');
		$this->form_validation->set_rules('last_name', $this->lang->line('edit_user_validation_lname_label'), 'required');
		$this->form_validation->set_rules('codice_fiscale', $this->lang->line('iscriviti_validation_codice_fiscale_label'), 'required|callback_check_codice_fiscale');
		$this->form_validation->set_rules('data_nascita', $this->lang->line('iscriviti_validation_data_nascita_label'), 'required|callback_check_data_nascita');
		if (isset($_POST) && !empty($_POST))
		{
			// do we have a valid request?
			if ($this->_valid_csrf_nonce() === FALSE || $id != $this->input->post('id'))
			{
				show_error($this->lang->line('error_csrf'));
			}

			//update the password if it was posted
			if ($this->input->post('password'))
			{
				$this->form_validation->set_rules('password', $this->lang->line('edit_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
				$this->form_validation->set_rules('password_confirm', $this->lang->line('edit_user_validation_password_confirm_label'), 'required');
			}
			
			if ($this->form_validation->run() === TRUE)
			{	
				$data_nascita = DateTime::createFromFormat('d/m/Y', $_POST['data_nascita']);
				$data = array(
					'nome' => $this->input->post('first_name'),
					'cognome'  => $this->input->post('last_name'),
					'cf' => $this->input->post('codice_fiscale'),
					'data_nascita' => $data_nascita->format('Y-m-d 00:00:00')
				);
				
				//update the password if it was posted
				if ($this->input->post('password'))
				{
					$data['password'] = $this->input->post('password');
				}

				// Only allow updating groups if user is admin
				if ($this->ion_auth->amministrazione(2,0))
				{
					$this->load->model('Groups');
					$this->load->model('Punteggio');
					//Update the groups user belongs to					
					$groupData = $this->input->post('groups');
					if (isset($groupData) && !empty($groupData)) {

						$this->ion_auth->remove_from_group('', $id);

						foreach ($groupData as $grp) {
							$this->ion_auth->add_to_group($grp, $id);
							$dati_gruppo = $this->Groups->get($grp);
							//se sto bannando un utente, devo riportare le sue prove a "In attesa di validazione"
							if ($dati_gruppo->name == 'banned'){
								$punteggi = $this->Punteggio->get_many_by(array('FK_punteggio_user' => $id, 'FK_punteggio_validitaProva' => $this->getValiditaProvaIDbyDescrizione('Valida')));
								$arr_id_punteggio = array();
								foreach ($punteggi as $punteggio){
									array_push($arr_id_punteggio, $punteggio->punteggioID);
								}
								if ($arr_id_punteggio){
									$this->Punteggio->update_many($arr_id_punteggio, array('FK_punteggio_user' => $id,'FK_punteggio_validitaProva' => $this->getValiditaProvaIDbyDescrizione('In attesa di validazione')));
								}
							}
							
						}

					}
				}

			//check to see if we are updating the user
			   if($this->ion_auth->update($user->id, $data))
			    {
			    	//redirect them back to the admin page if admin, or to the base url if non admin
				    $this->session->set_flashdata('message', $this->ion_auth->messages() );
				    
				    if ($this->ion_auth->amministrazione(2,0))
					{
						redirect('gestione_concorso/gestione_partecipanti', 'refresh');
					}
					else
					{
						redirect('/', 'refresh');
					}

			    }
			    else
			    {
			    	//redirect them back to the admin page if admin, or to the base url if non admin
				    $this->session->set_flashdata('message', $this->ion_auth->errors() );				    
				    if ($this->ion_auth->amministrazione(2,0))
					{
						redirect('gestione_concorso/gestione_partecipanti', 'refresh');
					}
					else
					{
						redirect('/', 'refresh');
					}

			    }

			}
		}

		//display the edit user form
		$this->data['csrf'] = $this->_get_csrf_nonce();

		//set the flash data error message if there is one
		$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

		//pass the user to the view
		$this->data['user'] = $user;
		$this->data['groups'] = $groups;
		$this->data['currentGroups'] = $currentGroups;
		$this->data['selectedGroupsIDs'] = $this->input->post('groups');
		$this->data['first_name'] = array(
			'name'  => 'first_name',
			'id'    => 'first_name',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('first_name', $user->nome),
		);
		$this->data['last_name'] = array(
			'name'  => 'last_name',
			'id'    => 'last_name',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('last_name', $user->cognome),
		);
		$this->data['password'] = array(
			'name' => 'password',
			'id'   => 'password',
			'type' => 'password'
		);
		$this->data['password_confirm'] = array(
			'name' => 'password_confirm',
			'id'   => 'password_confirm',
			'type' => 'password'
		);
		$this->data['codice_fiscale'] = array(
			'name'  => 'codice_fiscale',
			'id'    => 'codice_fiscale',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('codice_fiscale', $user->cf),
		);
		$this->data['data_nascita'] = array(
			'name'  => 'data_nascita',
			'id'    => 'data_nascita',
			'type'  => 'text',
			'readonly' => 'readonly',
			'value' => (property_exists($user,'data_nascita')&&($user->data_nascita))? $this->form_validation->set_value('data_nascita', DateTime::createFromFormat('Y-m-d G:i:s', $user->data_nascita)->format('d/m/Y')):'',
		);			
		$this->data['informativa_a'] = array(
			'name' => 'informativa_a',
			'id'   => 'informativa_a',
			'type' => 'checkbox'
		);
		$this->data['informativa_b'] = array(
			'name' => 'informativa_b',
			'id'   => 'informativa_b',
			'type' => 'checkbox'
		);
		$this->data['informativa_c'] = array(
			'name' => 'informativa_c',
			'id'   => 'informativa_c',
			'type' => 'checkbox'
		);
		//$this->data['urls'] = $this->url;
		$this->load->view('backend/includes/header', $this->data);
		$this->_render_page('backend/gestione_concorso/modifica_partecipante', $this->data);
		$this->load->view('backend/includes/footer', $this->data);
	}
	/******************************************************************************************************
	 * METODO: gestione_stato_utente_backend()
	 * Serve per gestire lo stato attivo/disattivo di un utente che può accedere al backend
	 *
	 *
	 ***************************************************************************************************** */

	function gestione_stato_partecipante($id)
	{
		if (!$this->ion_auth->logged_in() || !$this->ion_auth->amministrazione(2,0))
		{
			//redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else {		
			if (isset($_POST) && !empty($_POST))
			{	
				if ($this->input->post('status') == 'attivo')
				{
					redirect('gestione_concorso/activate_simple/'.$id, 'refresh');
				}
				elseif ($this->input->post('status') == 'disattivo')
				{
					redirect('gestione_concorso/deactivate_simple/'.$id, 'refresh');
				}
				else {
					
				}
			}
			else 
			{	
				$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
				$this->data['user'] = $this->ion_auth->user($id)->row();
				$this->load->view('backend/includes/header');
				$this->_render_page('backend/gestione_concorso/gestione_stato_partecipante', $this->data);
				$this->load->view('backend/includes/footer');
			}
		}
	}
	
	/******************************************************************************************************
	 * METODO: activate_simple($id, $code=false)
	 * 
	 * 	
	 *
	 ***************************************************************************************************** */

	function activate_simple($id, $code=false)
	{
		if ($code !== false)
		{
			$activation = $this->ion_auth->activate($id, $code);
		}
		else if ($this->ion_auth->amministrazione(2,0))
		{
			$activation = $this->ion_auth->activate($id);
		}

		if ($activation)
		{
			$this->session->set_flashdata('message', $this->ion_auth->messages());
			
			if ($this->ion_auth->is_contestant($id)){
				$this->attribuisciPunteggioIscrizione($id);
			}	
			
		}
		else
		{
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			
		}
		redirect("gestione_concorso/gestione_partecipanti", 'refresh');

	}

	/******************************************************************************************************
	 * METODO: deactivate_simple($id)
	 * //deactivate the user
	 * 	
	 *
	 ***************************************************************************************************** */
	 
	
	function deactivate_simple($id)
	{

		$id = (int) $id;
		if ($this->ion_auth->amministrazione(2,0))
		{
			$deactivation = $this->ion_auth->deactivate($id);
		}

		if ($deactivation)
		{
			$this->annullaPunteggioIscrizione($id);
			$this->session->set_flashdata('message', $this->ion_auth->messages());
			
		}
		else
		{
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			
		}	
		redirect("gestione_concorso/gestione_partecipanti", 'refresh');
	}

	// metodi da implementare o ricopiare qui
	
	
	function periodi_e_prove() {
		$this->load->model('Concorsi');
        $this->load->model('Periodi');
		// navigazione a cascata dei periodi e delle prove
		$this->data['nome'] = $this->nome_concorso;
		$this->load->view('backend/includes/header',$this->data);
		$totale = $this->Concorsi->count_all();		
		if ($totale == 1 ){
			$arr_multidim_concorsi = $this->Concorsi->get_all();	
			$arr_concorso = $arr_multidim_concorsi[0];
			$this->data['concorso_dati'] = (array) $arr_concorso;
			
			$arr_periodi = $this->Periodi->get_all();	
			$this->data['periodi_dati'] = (array) $arr_periodi;			
		}
		else {
			$this->data['msgtype'] = "alert";
			$this->data['output'] = "Errore durante il caricamento dei concorsi";			
		}
		$this->load->view('backend/gestione_concorso/periodi_e_prove',$this->data);
		$this->load->view('backend/includes/footer');
		
		
	}
	
	function prove($idPeriodo){
		$this->data['idperiodo'] = $idPeriodo;
		$this->load->model('Periodi');
		$this->data["dati_periodo"] = $this->Periodi->get($idPeriodo);
		$this->load->view('backend/includes/header');
		$this->load->view('backend/gestione_concorso/prove',$this->data);
		$this->load->view('backend/includes/footer');
	}
	
	function sollecita_contestant(){		
		$this->load->library('email');
		$this->load->config('ion_auth', TRUE);
		$user = $_POST['user'];
		$context = $_POST['context'];
		if (!$user){
			$arr = array('result' => 'error', 'message' => 'Errore nel passaggio dei dati');
			echo json_encode($arr);
			return FALSE;
		}
		$this->load->model('Users');
		$dati_utente = $this->Users->get($user);
		
		
		// Franz : creo un array con i dati dinamici da usare nei templates email
		$data = array(
			'nome' => $dati_utente->nome,
			'cognome' => $dati_utente->cognome,
			'activation' => $dati_utente->activation_code
			);
		
		$this->email->clear();
		$this->email->from($this->dati_concorso["output"]["admin_email"]);
		$this->email->to($dati_utente->email);
		$this->email->subject($this->dati_concorso["output"]["nome"] . ' - ' . 'Sollecito');
		if ($context == 'lazy'){
			$testo = $this->load->view($this->config->item('backend_email_templates', 'ion_auth').$this->config->item('email_sollecita_lazy', 'ion_auth'), $data, TRUE);
		}
		elseif ($context == 'pending'){
			$testo = $this->load->view($this->config->item('backend_email_templates', 'ion_auth').$this->config->item('email_sollecita_pending', 'ion_auth'), $data, TRUE);
		}
		else {
			$arr = array('result' => 'error', 'message' => 'Errore di configurazione.');
			echo json_encode($arr);
			return FALSE;
		}
		$this->email->message($testo);
		if ($this->email->send()){
			$arr = array('result' => 'success', 'message' => 'Email inviata.');
			echo json_encode($arr);
			return TRUE;
		}
		else {
			$arr = array('result' => 'alert', 'message' => 'Errore durante l\'invio della mail.');
			echo json_encode($arr);
			return FALSE;
		}
	}

}
