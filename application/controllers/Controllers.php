<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Controllers extends MY_Controller {

	public function __construct()
    {	
        parent::__construct();
        if (!$this->ion_auth->logged_in() OR !$this->ion_auth->amministrazione(2,2))
		{
		    return show_error('You must be an administrator to view this page.');
		}
        // Your own constructor code
        $this->load->database();  
        $this->load->model('Controllers_model');      
        //$this->output->enable_profiler(TRUE);		
    } 
	
	// Restituisce lista dei controllers nella tabella stati del db
	public function get_dati_controllers_json(){		
		$arr_controllers = $this->Controllers_model->get_all();
		
		
		$arr = array('result' => 'success', 'output' => (array) $arr_controllers);
		// stampa un json con la lista dei controllers
		echo json_encode($arr);
	}

}