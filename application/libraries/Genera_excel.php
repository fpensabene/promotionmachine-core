<?php if (!defined('BASEPATH')) exit('No direct script access allowed');  
class Genera_excel {
	
	function to_excel($array, $filename) {
	    header('Content-Disposition: attachment; filename='.$filename.'.xls');
	    header('Content-type: application/force-download');
	    header('Content-Transfer-Encoding: binary');
	    header('Pragma: public');
	    echo($this->print_table($array));
	    
	}
	
	function writeRow($val) {
	    return '<td>'.$val.'</td>';              
	}
	
	function print_table($result_array){
		$tabella = '<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />';
	    $h = array();
	    foreach($result_array as $row){
	        foreach($row as $key=>$val){
	            if(!in_array($key, $h)){
	                $h[] = $key;   
	            }
	        }
	    }
	    $tabella .= '<table><tr>';
	    foreach($h as $key) {
	        $key = ucwords($key);
	        $tabella .= '<th>'.$key.'</th>';
	    }
	    $tabella .= '</tr>';
	
	    foreach($result_array as $row){
	        $tabella .= '<tr>';
	        foreach($row as $val)
	            $tabella .= $this->writeRow($val);   
	    }
	    $tabella .= '</tr>';
	    $tabella .= '</table>';
		return $tabella;
	}

}
?>