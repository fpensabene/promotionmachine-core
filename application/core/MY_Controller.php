<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/*	==========================================================================================================================
	CLASSE: MY_Controller
	========================================================================================================================== 
	classe da cui tutti i controller dovrebbero ereditare, qui devo inserire tutto quello che so che mi servira' ovunque.
	nel controller avro'
	Nomecontroller extends MY_Controller
	e da li' potro' accedere a questi metodi o variabili tramite $this->metodo o $this->variabile
	ovviamente se nel controller "figlio" dichiaro un metodo o una variabile con lo stesso nome, quella avra' la precedenza
	==========================================================================================================================
	========================================================================================================================== */


class MY_Controller extends CI_Controller {

	public function __construct()
    {	
	    date_default_timezone_set("Europe/Rome");
	    parent::__construct();

		
		$this->dati_concorso = $this->get_dati_concorso();
		
		// recuperiamo il nome del concorso e lo rendiamo disponibile a tutte le classi figlie ed in particola sia in front-end e backend
		$this->nome_concorso = $this->dati_concorso['output']['nome'];     
	   
		// capire perchè carichiamo qui il db?????
	    $this->load->database();
	   
    } 
    
	/****************************************************************************************************** 
	 * METODO: get_dati_concorso()
	 * Restituisce tutti i dati del concorso corrente
	 *
	 *
	 ***************************************************************************************************** */	
	
	protected function get_dati_concorso(){
		
        $this->load->model('Concorsi');
        $this->load->model('Stati');
		$totale = $this->Concorsi->count_all(); // quanti concorsi ho?
		//$output_array = array();
		
		if ($totale){
			
			$data = $this->Concorsi->get_all(); //get_all() è una funzione del model
			// ora e data attuali
			$now_date = DateTime::createFromFormat('Y-m-d G:i:s', date('Y-m-d G:i:s')); 
			// vogliamo avere un solo concorso quindi prendo solo il primo elemento - qui ho tutti i dati del concorso è un oggetto!
			$obj_dati_concorso = $data[0];	
			
			// ORA DEVO CONTROLLARE IN CHE FASE SIAMO DEL CONCORSO: PRIMA, DOPO, DURANTE
			
			// Creo un oggetto di tipo DateTime per la data di inizio - con questo formato posso fare il confronto
			$data_inizio_date = DateTime::createFromFormat('Y-m-d G:i:s', $obj_dati_concorso->data_inizio);
			// Creo un oggetto di tipo DateTime per la data di fine - con questo formato posso fare il confronto
			$data_fine_date = DateTime::createFromFormat('Y-m-d G:i:s', $obj_dati_concorso->data_fine);
			
			// ora posso fare i vari confronti tra le date per stabilire in che fase del concorso siamo
			if ($now_date < $data_inizio_date){
				$obj_dati_concorso->in_corso = 'prima';
			}
			elseif ($now_date > $data_fine_date){
				$obj_dati_concorso->in_corso = 'dopo';
			}
			else {
				$obj_dati_concorso->in_corso = 'durante';
			}
			
			$stato_attivoID = $this->getStatoIDbyDescrizione('Attivo');
			if ($stato_attivoID&&$obj_dati_concorso->FK_concorsi_stati==$stato_attivoID){
				$obj_dati_concorso->stato = 'attivo';
			}
			else {
				$obj_dati_concorso->stato = 'disattivo';
			}
			// Ora ho bisogno delle date in formato utilizzabile nel form - vado asovrascrivere le proprietà dell'oggetto
			$obj_dati_concorso->data_inizio = $data_inizio_date->format('d/m/Y H:i:s');
			$obj_dati_concorso->data_fine = $data_fine_date->format('d/m/Y H:i:s');
			
			// Ora preparo l'array con i risultati - il primo elemento è result il secondo è l'oggetto trasformato in array			
			$arr = array('result' => 'success', 'output' => (array) $obj_dati_concorso);
		}
		else {
			// Qui non abbiamo nessun concorso
			$obj_dati_concorso['concorsoID'] = "_NEW";	// _NEW indica ai metodi che lo ricevono che devo creare un nuovo concorso
			// idem come sopra creo l'array da restituire			
			$arr = array('result' => 'success', 'output' => (array) $obj_dati_concorso);
		}
		return $arr;		
	}
	
	
	
	
	/****************************************************************************************************** 
	 * METODO: get_dati_prove()
	 * Restituisce tutti i dati del concorso corrente
	 *
	 *
	 
	 lista delle prove [0=disattive, 1=attive, 2=tutte] per periodo
	 lista delle prove attive per periodo
	 lista delle prove con qualsiasi stato per periodo
	 
	 se non specifico il periodo mi deve restituire tutte le prove [attive] [OGGI]!
	 
	 $statoProve = 'Attive' => specifica lo stato delle prove che devono essere restituite
	 
	 $idPeriodo = NULL => periodo di interesse, se non specifico prende quelle dei periodi ATTIVI di TUTTO IL CONCORSO
	 
	 $scarta = NULL => specifica un tipo di prova da scartare, fa logica sul nome del controller. Utile tipicamente per scartare la prova di iscrizione
	 
	 $skip_prove_future = FALSE => specifica se skippare le prove future oppure no
	 
	 
	 ***************************************************************************************************** */
	 
	
	protected function get_dati_prove($statoProve = 'Attive', $idPeriodo = NULL, $scarta = NULL, $skip_prove_future = FALSE, $skip_non_svolte = FALSE) {
		
		// $statoProve -> indica se la prova è attiva o disattiva o tutti gli stati

		//carico tutti i modelli
		$this->load->model('Prove');	
		$this->load->model('Periodi');
		$this->load->model('Concorsi');
		$this->load->model('Stati');
		$this->load->model('Tipi_prova');
		$this->load->model('Controllers_model');
		$this->load->model('Punteggio');
		
		$stato_attivoID = $this->getStatoIDbyDescrizione('Attivo');
		$stato_disattivoID = $this->getStatoIDbyDescrizione('Disattivo');		
		$id_prova_iscrizione = $this->getIDProvaIscrizione();
		//inizializzo $errore a TRUE, e solo se riesco a interrogare la tabella CORE_prove assegno FALSE
		$errore = TRUE;
			
		$concorso_attuale = $this->Concorsi->get_all();
		
		// Recupero l'id del concorso  - prendo solo il primo elemento
		$concorso_attualeID = $concorso_attuale[0]->concorsoID;
		
		// faccio la query : seleziona tutte le prove nello stato che mi interessa
		
		if ($statoProve == 'Disattive'){
			$stato_scelto = $stato_disattivoID;	
		}
		elseif ($statoProve == 'Attive'){
			$stato_scelto = $stato_attivoID;
		}
		elseif ($statoProve == 'Tutte'){
			$stato_scelto = '_ANY';
		}
		else {
			$stato_scelto = NULL;
		}
		
		if ($stato_scelto){
			// Recupero tutti i periodi di questo concorso - che sono attivi
			$periodi_attivi = $this->Periodi->get_many_by(array('FK_periodi_concorsi' => $concorso_attualeID, 'FK_periodi_stati' => $stato_attivoID));
			$this->db->select('*');
			$this->db->from('CORE_prove');
			
			if ($stato_scelto!='_ANY'){
				$this->db->where("FK_prove_stati",$stato_scelto);
			}
				
			// aggiungo altri filtri alla query
			if (!$idPeriodo){
				
				// Recupero una lista di id dei periodi attivi - separati da virgola
				$output = array_map(function ($object) { return $object->periodoID; }, $periodi_attivi); // array_map converte un oggetto in array
				$csv_periodi = implode(', ', $output); // una stringa con la lista degli id
		
				$this->db->where("FK_prove_periodi in (".$csv_periodi.")", NULL, FALSE);						
			}
			else {
				$this->db->where("FK_prove_periodi", $idPeriodo, FALSE);
			}
			if ($scarta == 'iscrizione'){					
				if ($id_prova_iscrizione){
					$this->db->where("provaID !=", $id_prova_iscrizione, FALSE);
				}
			}
			
			$query = $this->db->get();
			if ($query) {
				$arr_prove = $query->result();
				$result = "success";
				$errore = FALSE;
			}
			
		}
		if ($errore){
			$arr_prove = array();
			$result = "alert";
		}
		$now_date = DateTime::createFromFormat('Y-m-d G:i:s', date('Y-m-d G:i:s'));
		
		$arr_prove_size = sizeof($arr_prove);
		for ($i = 0; $i < $arr_prove_size; $i++){
			
			$periodo_obj = $this->Periodi->get($arr_prove[$i]->FK_prove_periodi);
			$arr_prove[$i]->date_custom = 1;
			if ($arr_prove[$i]->data_inizio) {	
				$data_inizio_string = $arr_prove[$i]->data_inizio;									
			}
			else {
				$data_inizio_string = $periodo_obj->data_inizio;
				$arr_prove[$i]->date_custom = 0;
			}
			$data_inizio_date = DateTime::createFromFormat('Y-m-d G:i:s', $data_inizio_string);
			$arr_prove[$i]->data_inizio = $data_inizio_date->format('d/m/Y H:i:s');
			
			if ($arr_prove[$i]->data_fine) {	
				$data_fine_string = $arr_prove[$i]->data_fine;									
			}
			else {
				$data_fine_string = $periodo_obj->data_fine;
				$arr_prove[$i]->date_custom = 0;
			}
			$data_fine_date = DateTime::createFromFormat('Y-m-d G:i:s', $data_fine_string);
			$arr_prove[$i]->data_fine = $data_fine_date->format('d/m/Y H:i:s');
			if($data_inizio_date > $now_date){
				if ($skip_prove_future){
					unset($arr_prove[$i]);
					continue;
				}				
				$arr_prove[$i]->collocazione_temporale = "futuro";				
			}
			elseif ($data_fine_date < $now_date){
				$arr_prove[$i]->collocazione_temporale = "passato";
				if ($skip_non_svolte){
					$riga_punteggio = $this->Punteggio->get_many_by(array('FK_punteggio_prova' => $arr_prove[$i]->provaID, 'FK_punteggio_user' => $this->ion_auth->user()->row()->id));
					if(!$riga_punteggio){
						unset($arr_prove[$i]);
						continue;
					}
				}
			}
			else {
				$arr_prove[$i]->collocazione_temporale = "presente";
			}
			
			$arr_prove[$i]->desc_periodo = $this->Periodi->get($arr_prove[$i]->FK_prove_periodi)->descrizione;
			
			$arr_prove[$i]->desc_stato = $this->Stati->get($arr_prove[$i]->FK_prove_stati)->descrizione;
			
			$tipo_prova = $this->Tipi_prova->get($arr_prove[$i]->FK_prove_tipiProve);
			
			$dati_controller = $this->Controllers_model->get($tipo_prova->FK_CORE_tipoProva_CORE_controllers);
			
			$arr_prove[$i]->nome_controller_frontend = $dati_controller->nome_controller_frontend;
			$arr_prove[$i]->nome_controller_backend = $dati_controller->nome_controller_backend;
			if ($dati_controller->nome_controller_backend != 'iscrizione'){
				$admin_link = $dati_controller->nome_controller_backend."/amministra/".$arr_prove[$i]->provaID;
				$arr_prove[$i]->admin_link = $admin_link;
			}			
		}
		//trick per "rebase" array
		$arr_prove = array_values($arr_prove);
		$arr = array('result' => $result, 'output' => (array) $arr_prove, 'size' => sizeof($arr_prove));
		return $arr;
	}

	
	
	
	/****************************************************************************************************** 
	 * METODO: _get_csrf_nonce()
	 * 
	 *
	 * 
	 *
	 ***************************************************************************************************** */
	 
	function _get_csrf_nonce()
	{
		$this->load->helper('string');
		$key   = random_string('alnum', 8);
		$value = random_string('alnum', 20);
		$this->session->set_flashdata('csrfkey', $key);
		$this->session->set_flashdata('csrfvalue', $value);

		return array($key => $value);
	}

	/****************************************************************************************************** 
	 * METODO: _valid_csrf_nonce()
	 * 
	 *
	 * 
	 *
	 ***************************************************************************************************** */
	function _valid_csrf_nonce()
	{
		if ($this->input->post($this->session->flashdata('csrfkey')) !== FALSE &&
			$this->input->post($this->session->flashdata('csrfkey')) == $this->session->flashdata('csrfvalue'))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	/****************************************************************************************************** 
	 * METODO: _render_page($view, $data=null, $render=false)
	 * 
	 *
	 * 
	 *
	 ***************************************************************************************************** */
	 
	function _render_page($view, $data=null, $render=false)
	{
		$this->viewdata = (empty($data)) ? $this->data: $data;
		$view_html = $this->load->view($view, $this->viewdata, $render);
		if (!$render) return $view_html;
	}
	
	/****************************************************************************************************** 
	 * METODO: check_codice_fiscale($p)	 
	 * 
	 * lo utilizzo per il controllo del codice fiscale (registrazione e modifica)
	 * 
	 *
	 ***************************************************************************************************** */
	
	function check_codice_fiscale($p)
	{ 
		$p = $this->input->post('codice_fiscale');
		if (preg_match('/^[a-z]{6}[0-9]{2}[a-z][0-9]{2}[a-z][0-9]{3}[a-z]$/i', $p)) return true;
		// it matched, see <ul> below for interpreting this regex   
		else {
			$this->form_validation->set_message('check_codice_fiscale','Errore codice fiscale');
			return false;
		} 
	}
	
	/****************************************************************************************************** 
	 * METODO: check_data_nascita($p)	 
	 * 
	 * lo utilizzo per il controllo del della data di nascita dell'utente
	 * 
	 *
	 ***************************************************************************************************** */
	
	function check_data_nascita($p)
	{ 
		$p = $this->input->post('data_nascita');
		$data_nascita = DateTime::createFromFormat('d/m/Y', $p);
		$now_date = DateTime::createFromFormat('Y-m-d G:i:s', date('Y-m-d G:i:s'));
		$interval = $now_date->diff($data_nascita);
		$this->load->model("Prove");
		$dati_prova_iscrizione = $this->Prove->get($this->getIDProvaIscrizione());
		
		if (!$dati_prova_iscrizione->solo_maggiorenni OR $dati_prova_iscrizione->solo_maggiorenni AND $interval->y >= 18) {
			return true;
		}
		else {
			$this->form_validation->set_message('check_data_nascita','Devi essere maggiorenne');
			return false;
		} 
	}
	
	
	/****************************************************************************************************** 
	 * METODO: addPunteggio($arr_punteggio)
	 * 
	 * aggiunge un punteggio. $arr_punteggio e' un array e deve contenere
	 *
	 *	'data_convalida' 
	 *	'FK_punteggio_validitaProva' 
	 *	'FK_punteggio_user' 
	 *	'FK_punteggio_prova' 
	 *	'punteggio' 
	 *	'ip'
	 * 
	 *
	 ***************************************************************************************************** */
	 
	protected function addPunteggio($arr_punteggio)
	{
		$this->load->model('Punteggio');
		return $this->Punteggio->insert($arr_punteggio);
	}
	/****************************************************************************************************** 
	 * METODO: getValiditaProvaIDbyDescrizione($descrizione)	 
	 * 
	 * data una descrizione (solitamente Valida/Non valida) mi restituisce l'id della riga validitaProva corrispondente
	 * 
	 *
	 ***************************************************************************************************** */
	protected function getValiditaProvaIDbyDescrizione($descrizione)
	{
		$this->load->model('Validita_prova');	
		$prova = $this->Validita_prova->get_many_by(array('descrizione' => $descrizione));
		if ($prova){
			$provaID = $prova[0]->validitaProvaID;
			return $provaID;
		}
		else{
			return false;
		}
			
	}
	
	/****************************************************************************************************** 
	 * METODO: getStatoIDbyDescrizione($descrizione)	 
	 * 
	 * data una descrizione (solitamente Attivo/Disattivo) mi restituisce l'id della riga stati corrispondente
	 * 
	 *
	 ***************************************************************************************************** */
	protected function getStatoIDbyDescrizione($descrizione)
	{
		$this->load->model('Stati');	
		$stato = $this->Stati->get_many_by(array('descrizione' => $descrizione));
		if ($stato){
			$statoID = $stato[0]->statoID;
			return $statoID;
		}
		else{
			return false;
		}
			
	}
	
	/****************************************************************************************************** 
	 * METODO: attribuisciPunteggioIscrizione($id) 
	 * 
	 * chiamata dopo l'attivazione di un account. inserisce o aggiorna la riga corrispondente di "punteggio"
	 * NB: SOLO PER CONTESTANT.
	 *
	 ***************************************************************************************************** */
	
	protected function attribuisciPunteggioIscrizione($id)
	{		
		//id da utilizzare nella FK_punteggio_validitaProva, corrisponde alla prova VALIDA
		$prova_validaID = $this->getValiditaProvaIDbyDescrizione('Valida');
		
		//se questo utente ha gia' un punteggio per l'iscrizione, devo solamente attribuire la validita, con un update
		$this->load->model('Punteggio');
		$punteggio_obj = $this->Punteggio->get_by(array(
														'FK_punteggio_user' =>$id,
														'FK_punteggio_prova' => $this->getIDProvaIscrizione()
														));														
		if ($punteggio_obj){
			$arr_update = array(
								'FK_punteggio_validitaProva' => $prova_validaID,
								'data_convalida' => date('Y-m-d H:i:s') 
								);
			return $this->Punteggio->update($punteggio_obj->punteggioID, $arr_update);
		}
		else {
			//altrimenti, devo procedere con una insert
			
			//ottengo l'object coi dati della prova di iscrizione
			$id_prova_iscrizione = $this->getIDProvaIscrizione();
			$this->load->model('Prove');
			$obj_prova_iscrizione = $this->Prove->get($id_prova_iscrizione);
			
			//devo ottenere il PUNTEGGIO che viene attribuito alla prova di iscrizione, lo trovo in Tipi_prova
			$this->load->model('Tipi_prova');
			$tipo_prova_obj = $this->Tipi_prova->get($obj_prova_iscrizione->FK_prove_tipiProve);
			
			//aggiungo la nuova riga
			$arr_punteggio = array(
				'data_convalida' => date('Y-m-d H:i:s'),
				'FK_punteggio_validitaProva' => $prova_validaID,
				'FK_punteggio_user' => $id,
				'FK_punteggio_prova' => $this->getIDProvaIscrizione(),
				'punteggio' => $tipo_prova_obj->punteggio,
				'ip' => $this->input->ip_address()
				 
			);
			return $this->addPunteggio($arr_punteggio);
		}
	}
	/****************************************************************************************************** 
	 * METODO: annullaPunteggioIscrizione($id) 
	 * 
	 * chiamata dopo la disattivazione di un account. setta a Non Valida la riga corrispondente di "punteggio"
	 * 
	 *
	 ***************************************************************************************************** */
	protected function annullaPunteggioIscrizione($id){		
		//id da utilizzare nella FK_punteggio_validitaProva, corrisponde alla prova VALIDA
		$prova_validaID = $this->getValiditaProvaIDbyDescrizione('Non valida');
		
		//se questo utente ha gia' un punteggio per l'iscrizione, devo solamente attribuire la validita, con un update
		$this->load->model('Punteggio');
		$punteggio_obj = $this->Punteggio->get_by(array(
														'FK_punteggio_user' =>$id,
														'FK_punteggio_prova' => $this->getIDProvaIscrizione()
														));														
		if ($punteggio_obj){
			$arr_update = array('FK_punteggio_validitaProva' => $prova_validaID);
			return $this->Punteggio->update($punteggio_obj->punteggioID, $arr_update);
		}
		return false;
	}
	/****************************************************************************************************** 
	 * METODO: getIDProvaIscrizione() 
	 * 
	 * restituisce l'id della prova di iscrizione
	 * 
	 *
	 ***************************************************************************************************** */
	protected function getIDProvaIscrizione(){
		
		$this->load->model('Prove');
		
		$dati_prova_iscrizione = $this->Prove->get_by(array("FK_prove_tipiProve" => $this->getIDTipoProvaIscrizione()));
		if ($dati_prova_iscrizione){
			return $dati_prova_iscrizione->provaID;
		}
		return FALSE;
	}
	
	/****************************************************************************************************** 
	 * METODO: getIDTipoProvaIscrizione() 
	 * 
	 * restituisce l'id del tipo prova iscrizione
	 * 
	 *
	 ***************************************************************************************************** */
	protected function getIDTipoProvaIscrizione(){
		
		$this->load->model('Controllers_model');
		$dati_controller_iscrizione = $this->Controllers_model->get_by(array("nome_controller_backend" => "iscrizione"));
		$this->load->model('Tipi_prova');
		$dati_tipo_prova_iscrizione = $this->Tipi_prova->get_by(array("FK_CORE_tipoProva_CORE_controllers" => $dati_controller_iscrizione->controllerID));
		if ($dati_tipo_prova_iscrizione){
			return $dati_tipo_prova_iscrizione->tipoProvaID;
		}
		return FALSE;
	}
	/****************************************************************************************************** 
	 * METODO: checkUnivocitaIscrizione($codice_fiscale, $email) 
	 * 
	 * restituisce l'id della prova di iscrizione
	 * 
	 *
	 ***************************************************************************************************** */
	protected function checkUnivocitaIscrizione($codice_fiscale, $email){
		$univocita = $this->db->query("SELECT *
			FROM CORE_Users
			WHERE active = 1 
			AND (
				cf = '$codice_fiscale'
				OR email = '$email'
			)")->result();
		if ($univocita){
			return FALSE;
		}
		return TRUE;
	}
	
	
} // chiudo MY_controller - la classe




/*==========================================================================================================================
	class MY_Controller_frontend
	la usiamo per encapsulare tutte le variabili che contengono embeds html necessari nel frontend
 
 ==========================================================================================================================*/	
class MY_Controller_frontend extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		/* EMBEDS VIEW DEFINITE COME VARIABILI */
		$this->data['testataConcorso'] = $this->load->view('frontend/includes/testata-concorso', NULL, TRUE);
		$this->data['barraNavigazione'] = $this->load->view('frontend/includes/barra-navigazione', $this->data, TRUE);
		
		// carico il file di configurazione con le variabili del concorso
		$this->config->load('parametri-concorso-frontend');

	}
	
	/*==========================================================================================================================
	permessiVisualizzazione()
	
	Funzione centralizzata che gestisce:
	-concorso accessibile solo da contestants
	-fornisce le view prima e dopo la durata del concorso	
 
 ==========================================================================================================================*/	
	protected function permessiVisualizzazione($contestant_only = TRUE)
	{
		
		
		// Gestisco il fatto che solo i contestants possono accedere al concorso
		if ($contestant_only){
			if ($this->ion_auth->logged_in() && (!$this->ion_auth->is_contestant() /*|| $this->ion_auth->is_banned()*/)){
				
				$this->data['body_class'] = 'dopo'; // classe del body
				
				echo $this->load->view('frontend/includes/header-prima-dopo',$this->data, TRUE);
				echo $this->load->view('frontend/home/non-contestant', $this->data, TRUE);
				echo $this->load->view('frontend/includes/footer',$this->data, TRUE);
				die;
			}
		}
		
		// Controllo se il concorso è attivo o no
		if ($this->dati_concorso['output']['stato']!='attivo'){	
			
			$this->data['body_class'] = 'dopo'; // classe del body
			
			// Il concorso non è attivo
			echo $this->load->view('frontend/includes/header-prima-dopo',$this->data, TRUE);
			echo $this->load->view('frontend/home/concorso_non_attivo', $this->data, TRUE);
			echo $this->load->view('frontend/includes/footer-prima-dopo-admin',$this->data, TRUE);
			die;
		}
		
		// Controllo se siamo prima o dopo la durata del concorso
		if ($this->dati_concorso['output']['in_corso']!='durante'){

			
			if ($this->dati_concorso['output']['in_corso']=="prima"){	
				
				//Il concorso non è ancora iniziato
				$this->data['titolo_pagina'] = 'Il concorso non è ancora iniziato'; //TITOLO PAGINA
				$this->data['body_class'] = 'prima'; // classe del body
				echo $this->load->view('frontend/includes/header-prima-dopo',$this->data, TRUE);
				echo $this->load->view('frontend/home/prima', $this->data, TRUE);
				
			}
			elseif ($this->dati_concorso['output']['in_corso']=="dopo"){
				
				// Il concorso è finito
				$this->data['titolo_pagina'] = 'Il concorso si è concluso'; //TITOLO PAGINA
				$this->data['body_class'] = 'dopo'; // classe del body
				echo $this->load->view('frontend/includes/header-prima-dopo',$this->data, TRUE);
				echo $this->load->view('frontend/home/dopo', $this->data, TRUE);
				
			}			
			echo $this->load->view('frontend/includes/footer-prima-dopo-admin',$this->data, TRUE);
			die;
		}
		
	}
	
	
	
}	// chiudo MY_controller_frontend
	
/*	==========================================================================================================================
	CLASSE: Gestione_utenti
	========================================================================================================================== 
	classe da cui tutti i controller dovrebbero ereditare, qui devo inserire tutto quello che so che mi servira' ovunque.
	nel controller avro'
	Nomecontroller extends MY_Controller
	e da li' potro' accedere a questi metodi o variabili tramite $this->metodo o $this->variabile
	ovviamente se nel controller "figlio" dichiaro un metodo o una variabile con lo stesso nome, quella avra' la precedenza
	==========================================================================================================================
	========================================================================================================================== */
	
class Gestione_utenti extends MY_Controller_frontend {
	public function __construct()
	{
		parent::__construct();
		
	}
	
	
}




/*==========================================================================================================================
	class MY_Controller_backend 
	la usiamo per encapsulare tutte le variabili che contengono embeds html necessari nel backend
 
 ==========================================================================================================================*/	
class MY_Controller_backend extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		// Creo una variabile disponibile in tutti i metodi con il topNav
		$this->data['top_menu'] = $this->load->view('backend/includes/top_menu', NULL, TRUE);
	
		// carico il file di configurazione con le variabili del concorso
		$this->config->load('parametri-concorso-backend');
	
	}		
}