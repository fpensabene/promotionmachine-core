<body class="backend-page"> 

<?php echo $top_menu; ?>

<!---*** BANDA PROMOTIONMACHINE ***-->
<section class="promotion-machine-background">
	<div class="row">
		<div class="small-12 text-center column">
		</div>
	</div>
</section>

<!---*** BREADCRUMBS ***-->
<section class="hrow">
	<div class="row">
		<div class="column">
	
			<ul class="breadcrumbs">
			  <li><a href="<?=site_url()?>admin/index">DASHBOARD</a></li>
			  <li class="unavailable"><a href="#">Admin</a></li>
			  <li><a href="<?=site_url().$this->uri->segment(1)."/".$this->uri->segment(2)?>">Gestione partecipanti</a></li>
			</ul>
		
		</div> <!--chiudo column-->
	</div> <!--chiudo row-->
</section>



<section class="hrow">
	<div class="row">
		<div class="small-12 column">
		
		<table style="width: 100%;">
		  <thead>
		    <tr>
			  <th>NUMERO</th>
		      <th>NOME ESTRAZIONE</th>
		      <th>VEDI A SCHERMO</th>
		      <th>ESPORTA EXCEL</th>
		      
		    </tr>
		  </thead>
		  <tbody>
		    <tr>
		      <td>1</td>
		      <td>Estrazione contestants - settembre 2015 </td>
		      <td><a href="<?= site_url()?>/Estrazioni_UHU_Renature/estrazione_camera_commercio/0/2">schermo</a></td>
		      <td><a href="<?= site_url()?>/Estrazioni_UHU_Renature/estrazione_camera_commercio/1/2">excel</a></td>
		    </tr>
		    <tr>
		      <td>1</td>
		      <td>Estrazione contestants - ottobre 2015</td>
		      <td><a href="<?= site_url()?>/Estrazioni_UHU_Renature/estrazione_camera_commercio/0">schermo</a></td>
		      <td><a href="<?= site_url()?>/Estrazioni_UHU_Renature/estrazione_camera_commercio/1">excel</a></td>
		    </tr>
		    <tr>
		      <td>1</td>
		      <td>Estrazione contestants - novembre 2015</td>
		      <td><a href="<?= site_url()?>/Estrazioni_UHU_Renature/estrazione_camera_commercio/0">schermo</a></td>
		      <td><a href="<?= site_url()?>/Estrazioni_UHU_Renature/estrazione_camera_commercio/1">excel</a></td>
		    </tr>
		    <tr>
		      <td>1</td>
		      <td>Estrazione contestants - dicembre 2015</td>
		      <td><a href="<?= site_url()?>/Estrazioni_UHU_Renature/estrazione_camera_commercio/0">schermo</a></td>
		      <td><a href="<?= site_url()?>/Estrazioni_UHU_Renature/estrazione_camera_commercio/1">excel</a></td>
		    </tr>
		    <tr>
		      <td>1</td>
		      <td>Estrazione contestants - gennaio 2016</td>
		      <td><a href="<?= site_url()?>/Estrazioni_UHU_Renature/estrazione_camera_commercio/0">schermo</a></td>
		      <td><a href="<?= site_url()?>/Estrazioni_UHU_Renature/estrazione_camera_commercio/1">excel</a></td>
		    </tr>
		   <tr>
		      <td>1</td>
		      <td>Estrazione contestants - febbraio 2016</td>
		      <td><a href="<?= site_url()?>/Estrazioni_UHU_Renature/estrazione_camera_commercio/0">schermo</a></td>
		      <td><a href="<?= site_url()?>/Estrazioni_UHU_Renature/estrazione_camera_commercio/1">excel</a></td>
		    </tr>
		  </tbody>
		</table>
		
		</div> <!--chiudo column-->
	</div> <!--chiudo row-->	
</section>