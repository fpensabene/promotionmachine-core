<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Estrazioni_UHU_Renature extends MY_Controller_backend {

	public function __construct()
    {	
        parent::__construct();
        // Your own constructor code
        if (!$this->ion_auth->logged_in() OR !$this->ion_auth->amministrazione(2,2))
		{
		    return show_error('You must be an administrator to view this page.');
		}
        $this->load->database();
    } 
    
	public function index(){
		
		//devo fare una lista delle estrazioni (metodi di questo controller) disponibili
		
		$this->load->view('backend/includes/header', $this->data);
		$this->load->view('index', $this->data);
		$this->load->view('backend/includes/footer', $this->data);
		
		
	}
	
	public function export_validi(){
		header("Content-Type: application/xls");    
		header("Content-Disposition: attachment; filename=$filename.xls");  
		header("Pragma: no-cache"); 
		header("Expires: 0");
		$this->load->model('Export_view');
	}
	
	
	/* modificare questa funzione per gestire le estrazioni di tutti i mesi */
	/* id della prova non è sicuro perchè dipende dalla configurazione del db */ 
	
	
	public function estrazione_camera_commercio($excel = 0, $id_prova = 0) {
	    $this->load->library('genera_excel');
	    $result = $this->db->query("SELECT
										prove.punteggioID AS `Punteggio`,
										prove.id AS `ID Utente`,
										prove.email AS `Email`,
										prove.nome AS `Nome`,
										prove.cognome AS `Cognome`,
										prove.cf AS `Codice Fiscale`,
										date_format(prove.data_nascita,'%m/%d/%Y') AS `Data di Nascita`,
										date_format(prove.datetimecol_1,'%m/%d/%Y') AS `Data Scontrino`,
										prove.textcol_3 AS `Ora Scontrino`,
										prove.textcol_1 AS `Importo Scontrino`, 
										prove.textcol_2 AS `Numero scontrino`,
										prove.filecol_1 AS `Path Scontrino`
									FROM
										(
											SELECT
												j.punteggioID,
												j.id,
												j.email,
												j.nome,
												j.cognome,
												j.cf,
												j.data_nascita,
												up.datetimecol_1,
												up.textcol_3,
												up.textcol_1,
												up.textcol_2,
												up.filecol_1,
												j.punteggio
											FROM
												upload up
											JOIN (
												SELECT
													*
												FROM
													punteggio punt
												JOIN CORE_Users ut ON punt.FK_punteggio_user = ut.id
												WHERE
													punt.FK_punteggio_validitaProva = (
														SELECT
															validitaProvaID
														FROM
															validitaProva
														WHERE
															descrizione = 'Valida'
														AND punt.FK_punteggio_prova = $id_prova
														AND ut.id IN (
															SELECT
																FK_punteggio_user
															FROM
																punteggio
															WHERE
																FK_punteggio_prova = 1
														)
													)
											) j ON up.FK_upload_punteggio = j.punteggioID
										) prove
									JOIN generator_4k gen
									WHERE
										gen.n < prove.punteggio
									ORDER BY
										punteggioID");	
		if ($result->result_array()){						
										
			if (!$excel){
				$this->data["tabella"] =  $this->genera_excel->print_table($result->result_array());
				$this->load->view('backend/includes/header', $this->data);
				$this->load->view('camera_commercio', $this->data);
				$this->load->view('backend/includes/footer', $this->data);
				}
			else {
				$this->genera_excel->to_excel($result->result_array(), 'elenco_camera_commercio');
			}
		}
		else {
			$this->load->view('backend/includes/header', $this->data);
			$this->load->view('nessun_dato_disponibile', $this->data);
			$this->load->view('backend/includes/footer', $this->data);
		}
	}
	
	
}
