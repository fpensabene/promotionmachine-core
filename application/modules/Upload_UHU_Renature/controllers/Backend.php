<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Backend extends MY_Controller_backend {

	public function __construct()
    {	
        parent::__construct();
        if (!$this->ion_auth->logged_in() OR !$this->ion_auth->amministrazione(2,2))
		{
		    return show_error('You must be an administrator to view this page.');
		}
        // Your own constructor code
        $this->load->database();   
        $this->load->library(array('email'));
        $this->load->model('Prova_upload');
        $this->load->model('Punteggio');
        $this->array_dati_concorso = $this->dati_concorso["output"];
        
    } 
	
	private function check_prova_ended($ripetibile, $numero_prove_sostenute){
		$partecipa_ancora = 0;
		if ($ripetibile == "INF")  {
			//se e' ripetibile infinite volte, allora l'utente puo' partecipare di nuovo, in ogni caso
			$partecipa_ancora = 1;
		}
		elseif (((int)$ripetibile == 0)){
			//se non e' ripetibile E se l'utente NON ha gia' svolto la prova, non puo' partecipare di nuovo	
			if ($numero_prove_sostenute == 0){
				$partecipa_ancora = 1;
			}
			else {
				$partecipa_ancora = 0;
			}
			
		}
		else {
			//caso piu' complesso, la prova e' ripetibile N volte. Controllo se la differenza tra $prova_obj->ripetibile e $numero_prove_sostenute  e' > 0
			if ((int)$ripetibile - $numero_prove_sostenute > 0){
				$partecipa_ancora = 1;
			}
			else {
				$partecipa_ancora = 0;
			}
		}
		return $partecipa_ancora;
	}
	
	public function amministra($idProva)
	{
		if (!$this->ion_auth->amministrazione(2,2)) //remove this elseif if you want to enable this for non-admins
		{
			//redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		$this->load->model('Prove');
		$this->load->model('Periodi');
		$this->load->view('backend/includes/header');
		$prova_obj = $this->Prove->get($idProva);
		if (!$prova_obj){
			redirect('admin');
		}
		$this->data['idprova'] = $idProva;
		$dati_prova = $this->Prove->get($idProva);
		$this->data["dati_periodo"] = $this->Periodi->get($dati_prova->FK_prove_periodi);
		$this->load->view('backend/amministra', $this->data);
		$this->load->view('backend/includes/footer');
	}
	
	public function get_dati_upload($idProva, $all = 0){
		$this->load->model('Validita_prova');
		$this->load->model('Users');
		if ($all){			
			$dati_punteggi = $this->get_dati_punteggi($idProva);
		}
		else {
			$dati_punteggi = $this->get_dati_punteggi($idProva, $this->ion_auth->user()->row()->id);
		}
		$arr_upload = array();
		if ($dati_punteggi){
			$csv_punteggi = $this->convert_dati_punteggi_to_csv($dati_punteggi);
			$this->db->select('*');
			$this->db->from('upload');
			$this->db->where("FK_upload_punteggio in (".$csv_punteggi.")", null, false);
			$query = $this->db->get();
			$arr_upload = $query->result();
			for ($i = 0; $i<count($arr_upload); $i++){	
				$arr_upload[$i]->datetimecol_1 = DateTime::createFromFormat('Y-m-d G:i:s', $arr_upload[$i]->datetimecol_1)->format('d/m/Y');
				$punteggio_obj = $this->Punteggio->get($arr_upload[$i]->FK_upload_punteggio);			
				$validita_prova_obj = $this->Validita_prova->get($punteggio_obj->FK_punteggio_validitaProva);
				$arr_upload[$i]->descr_validita_prova = $validita_prova_obj->descrizione;
				$prova = $this->Punteggio->get($arr_upload[$i]->FK_upload_punteggio);
				
				$dati_utente = $this->Users->get($prova->FK_punteggio_user);
				$arr_upload[$i]->user = $dati_utente->email;
			}
		}
		$arr = array('result' => 'success', 'output' => (array) $arr_upload);
		echo json_encode($arr);
	}
	
	public function modera($idUpload){
		$dati_upload = $this->Prova_upload->get($idUpload);
		$dati_punteggio = $this->Punteggio->get($dati_upload->FK_upload_punteggio);
		$this->load->model('Users');
		$dati_utente = $this->Users->get($dati_punteggio->FK_punteggio_user);
		$dati_upload->user = $dati_utente->email;
		$prova_non_validaID = $this->getValiditaProvaIDbyDescrizione('Non valida');
		$prova_validaID = $this->getValiditaProvaIDbyDescrizione('Valida');
		$prova_attesaID = $this->getValiditaProvaIDbyDescrizione('In attesa di validazione');
		if ($dati_punteggio->FK_punteggio_validitaProva == $prova_non_validaID){
			$dati_upload->validita_text = "Non valida";
		}
		elseif ($dati_punteggio->FK_punteggio_validitaProva == $prova_validaID) {
			$dati_upload->validita_text = "Valida";
		}
		elseif ($dati_punteggio->FK_punteggio_validitaProva == $prova_attesaID) {
			$dati_upload->validita_text = "In attesa di validazione";
		}
		else {
			$dati_upload->validita_text = "";
		}
		$dati_upload->validita_id = $dati_punteggio->FK_punteggio_validitaProva;
		$this->dati_upload = $dati_upload;
		$this->load->view('backend/includes/header');
		$this->load->view('backend/modera', $this->data);		
		$this->load->view('backend/includes/footer');
		
	}
	public function cambiaValidita(){
		$validita = $_POST['validita'];
		$idPunteggio = $_POST['idpunteggio'];
		$idUpload = $_POST['idupload'];
		$motivazione = $_POST['motivazione'];
		$this->load->model('Validita_prova');
		$dati_validita = $this->Validita_prova->get($validita);
		
		$arr_update = array('FK_punteggio_validitaProva' => $validita);
		if ($this->Punteggio->update($idPunteggio, $arr_update)){
			$this->Prova_upload->update($idUpload,array("motivazione_moderazione" => $motivazione));
			
			$dati_punteggio = $this->Punteggio->get($idPunteggio);
			$this->load->model('Users');
			$dati_utente = $this->Users->get($dati_punteggio->FK_punteggio_user);
			
			$this->email->clear();
			$this->email->from($this->array_dati_concorso["admin_email"], $this->array_dati_concorso["nome"]);
			$this->email->to($dati_utente->email);
			$this->email->subject($this->array_dati_concorso["nome"] . ' - ' . 'Moderazione scontrino');
			$message = "Ciao ".$dati_utente->nome.", il tuo scontrino &egrave; stato ";
			switch($dati_validita->descrizione){
				case 'Valida':
					$message .="approvato";
					break;
				case 'Non valida':
					$message .="respinto";
					break;
				case 'In attesa di validazione':
					$message .="messo in attesa di validazione";
					break;
			}
			$message .=" per questo motivo: $motivazione";
			$this->email->message($message);
			$this->email->send();			
			$message = "Moderazione effettuata con successo";
			$msgtype = "success";
		}
		else {
			$message = "Errore";
			$msgtype = "alert";
		}
		$arr = array('result' => $msgtype, 'message' => $message);
		echo json_encode($arr);
		return true;
	}
	private function check_scontrino_duplicato($idProva, $importo, $numero, $data_scontrino, $orario, $user){		
		$dati_punteggi = $this->get_dati_punteggi($idProva, $this->ion_auth->user()->row()->id);
		if (!$dati_punteggi){
			//se non ho punteggi vuol dire che l'utente non ha mai svolto questa prova, e non possono esserci duplicati
			return false;
		}
		//se invece ne ho, devo interrogare anche la tabella upload per vedere se i dati immessi sono identici a quelli di una prova gia' completata da questo utente.
		//converto in ID separati da virgola, tipo X, Y, Z. mi servira' per la condizione IN sulla colonna FK_upload_punteggio
		$csv_punteggi = $this->convert_dati_punteggi_to_csv($dati_punteggi);
		$this->db->select('*');
		$this->db->from('upload');
		$this->db->where("FK_upload_punteggio in ($csv_punteggi)", null, false);
		//compongo la query coi dati immessi
		$this->db->where(array(
			'textcol_1' => $importo,
			'textcol_2' => $numero,
			'datetimecol_1' => $data_scontrino->format('Y-m-d 00:00:00'),
			'textcol_3' => $orario
			));
		$query = $this->db->get();
		$arr_prove = $query->result();
		if ($arr_prove){
			//se ho prove, ho duplicati
			return true;
		}
		return false;
		//echo $this->db->last_query();

	}
	
	private function get_dati_punteggi($idProva, $user = NULL){
		if ($user){			
			return $this->Punteggio->get_many_by(array('FK_punteggio_prova' => $idProva, 'FK_punteggio_user' => $this->ion_auth->user()->row()->id));
		}
		else {
			return $this->Punteggio->get_many_by(array('FK_punteggio_prova' => $idProva));
		}
	}
	
	private function convert_dati_punteggi_to_csv($dati_punteggi){
		$output = array_map(function ($object) { return $object->punteggioID; }, $dati_punteggi);
		return implode(', ', $output);
	}
	
	public function get_dati_validita_prove(){
		$this->load->model('Validita_prova');
		$dati_validita = $this->Validita_prova->get_all();
		$arr = array('result' => 'success', 'output' => (array) $dati_validita);
		echo json_encode($arr);		
		
	}

}