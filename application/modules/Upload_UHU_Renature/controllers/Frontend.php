<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Frontend extends MY_Controller_frontend {

	public function __construct()
    {	
        parent::__construct();
        if (!$this->ion_auth->logged_in())
		{
		    redirect('');
		}
        // Your own constructor code
        $this->load->database();   
        $this->load->model('prova_upload');
        $this->permessiVisualizzazione(TRUE);
        
    } 
	public function index($slug = '')
	{	
		if($this->dati_concorso['output']['stato'] != 'attivo'){
			redirect('');
		}
		$this->data['body_class'] = "testata-small";
		$this->load->view('frontend/includes/header',$this->data);
		
		$this->load->model('Prove');	
		$this->load->model('Validita_prova');
		$this->load->model('Punteggio');		
		$this->load->model('Periodi');
		
		//check esistenza prova
		$prova_obj = $this->Prove->get_by("slug", $slug);
		if (!$prova_obj){
			redirect('');
		}
		
		//check se l'utente ha gia' svolto la prova
		//array di oggetti con i punteggi delle prove sostenute dall'utente
		$punteggi_obj = $this->Punteggio->get_many_by(array('FK_punteggio_user' => $this->ion_auth->user()->row()->id, 'FK_punteggio_prova' => $prova_obj->provaID/*, 'FK_punteggio_validitaProva' => $prova_validaID)*/));
		$numero_prove_sostenute = sizeof($punteggi_obj);
		
		//recupero periodo relativo alla prova
		$periodo_obj = $this->Periodi->get($prova_obj->FK_prove_periodi);
		//data attuale, mi serve per il confronto successivo
		$now_date = DateTime::createFromFormat('Y-m-d G:i:s', date('Y-m-d G:i:s'));
		
		//se la prova ha una sua data di inizio, uso quella, altrimenti uso quella del periodo
		if ($prova_obj->data_inizio) {	
			//la data di inizio in formato stringa
			$data_inizio_string = $prova_obj->data_inizio;									
		}
		else {
			//la data di inizio in formato stringa
			$data_inizio_string = $periodo_obj->data_inizio;
		}
		//la data di inizio in formato DateTime
		$data_inizio_date = DateTime::createFromFormat('Y-m-d G:i:s', $data_inizio_string);
		
		//se la prova ha una sua data di fine, uso quella, altrimenti uso quella del periodo
		if ($prova_obj->data_fine) {	
			//la data di fine in formato stringa
			$data_fine_string = $prova_obj->data_fine;									
		}
		else {
			//la data di fine in formato stringa
			$data_fine_string = $periodo_obj->data_fine;
		}
		//la data di fine in formato DateTime
		$data_fine_date = DateTime::createFromFormat('Y-m-d G:i:s', $data_fine_string);
		
		//stabilisco se la prova e' in corso oppure no
		if (($data_inizio_date <= $now_date)&&($now_date <= $data_fine_date ) ){
			$this->data['in_corso'] = 1;
		}
		else {
			$this->data['in_corso'] = 0;
		}
		
		//in questo caso mi serve una data minima per lo scontrino. coincide con la data di inizio della prova oppure del periodo relativo		
		
		$data_minima = $this->nvl($prova_obj->data_inizio, $periodo_obj->data_inizio);
		
		$this->data['partecipa_ancora'] = $this->check_prova_ended($prova_obj->ripetibile, $numero_prove_sostenute);
		
		$this->data['idprova'] = $prova_obj->provaID;
		
		$this->data['message']['result'] = $this->session->flashdata('result');
		$this->data['message']['text'] = $this->session->flashdata('text');
		
		$this->load->view('frontend/svolgi_prova_upload', $this->data);
		$this->load->view('frontend/includes/footer');
	}
	
	private function check_prova_ended($ripetibile, $numero_prove_sostenute){
		$partecipa_ancora = 0;
		if ($ripetibile == "INF")  {
			//se e' ripetibile infinite volte, allora l'utente puo' partecipare di nuovo, in ogni caso
			$partecipa_ancora = 1;
		}
		elseif (((int)$ripetibile == 0)){
			//se non e' ripetibile E se l'utente NON ha gia' svolto la prova, non puo' partecipare di nuovo	
			if ($numero_prove_sostenute == 0){
				$partecipa_ancora = 1;
			}
			else {
				$partecipa_ancora = 0;
			}
			
		}
		else {
			//caso piu' complesso, la prova e' ripetibile N volte. Controllo se la differenza tra $prova_obj->ripetibile e $numero_prove_sostenute  e' > 0
			if ((int)$ripetibile - $numero_prove_sostenute > 0){
				$partecipa_ancora = 1;
			}
			else {
				$partecipa_ancora = 0;
			}
		}
		return $partecipa_ancora;
	}
	
	public function get_dati_upload($idProva, $all = 0){
		$this->load->model('Punteggio');
		$this->load->model('Validita_prova');
		$this->load->model('Users');
		if ($all){			
			$dati_punteggi = $this->get_dati_punteggi($idProva);
		}
		else {
			$dati_punteggi = $this->get_dati_punteggi($idProva, $this->ion_auth->user()->row()->id);
		}
		$arr_upload = array();
		if ($dati_punteggi){
			$csv_punteggi = $this->convert_dati_punteggi_to_csv($dati_punteggi);
			$this->db->select('*');
			$this->db->from('upload');
			$this->db->where("FK_upload_punteggio in (".$csv_punteggi.")", null, false);
			$query = $this->db->get();
			$arr_upload = $query->result();
			for ($i = 0; $i<count($arr_upload); $i++){	
				$arr_upload[$i]->datetimecol_1 = DateTime::createFromFormat('Y-m-d G:i:s', $arr_upload[$i]->datetimecol_1)->format('d/m/Y');
				$punteggio_obj = $this->Punteggio->get($arr_upload[$i]->FK_upload_punteggio);			
				$validita_prova_obj = $this->Validita_prova->get($punteggio_obj->FK_punteggio_validitaProva);
				$arr_upload[$i]->descr_validita_prova = $validita_prova_obj->descrizione;
				$prova = $this->Punteggio->get($arr_upload[$i]->FK_upload_punteggio);
				
				$dati_utente = $this->Users->get($prova->FK_punteggio_user);
				$arr_upload[$i]->user = $dati_utente->email;
			}
		}
		$arr = array('result' => 'success', 'output' => (array) $arr_upload);
		echo json_encode($arr);
	}
	
	public function gestione($idProva){
		//variabili che uso per i vari controlli durante il flusso della funzione
		$errore = FALSE;
		//dettagli relativi agli eventuali errori
		$dettagli = "";
		//salvo in variabili cio' che arriva dal form...
		$importo = $_POST['importo'];
		$numero = $_POST['numero'];
		$data_scontrino = DateTime::createFromFormat('d/m/Y', $_POST['data_scontrino']);
		$orario = $_POST['orario_ora'].":".$_POST['orario_min'];				
		
		//data di oggi in formato date.			
		$now_date = new DateTime();
		//data inizio concorso in formato date
		$data_inizio_concorso = DateTime::createFromFormat('d/m/Y G:i:s', $this->dati_concorso['output']['data_inizio']);
		
		if ($data_scontrino < $data_inizio_concorso ){
			$dettagli .= "La data dello scontrino non pu&ograve; essere antecedente quella di inizio del concorso.<br/>";
			$msgtype = "alert";
			$errore = TRUE;	
		}
		
		if ($data_scontrino > $now_date){
			$dettagli .= "La data dello scontrino non pu&ograve; essere successiva a quella odierna.<br/>";
			$msgtype = "alert";
			$errore = TRUE;	
		}
		
		//e lo analizzo alla ricerca di eventuali errori
		if (!is_numeric($importo)){
			$dettagli .= "Il campo 'Importo' &egrave; errato.<br/>";
			$msgtype = "alert";
			$errore = TRUE;	
		}		
		if (!$numero){		
			$dettagli .= "Il campo 'Numero scontrino' &egrave; errato.<br/>";
			$msgtype = "alert";
			$errore = TRUE;	
		}
		if (!$data_scontrino){
			$dettagli .= "Il campo 'Data scontrino' &egrave; errato.<br/>";
			$msgtype = "alert";
			$errore = TRUE;	
		}
		if (!preg_match('/^\d{2}:\d{2}$/', $orario)) {
			$dettagli .= "L'orario inserito &egrave; errato.<br/>";
			$msgtype = "alert";
			$errore = TRUE;	
		}
		if (empty($_FILES['scontrino']['name'])){
			$dettagli .= "Non &egrave; stato caricato alcuno scontrino.<br/>";
			$msgtype = "alert";
			$errore = TRUE;	
		}
		
		//basta che uno solo di questi dia errore per far uscire la funzione stampando la/le causa/e
		if ($errore){
			$arr = array('result' => $msgtype, 'message' => $dettagli, 'dettagli' => $dettagli);
			echo json_encode($arr);
			return FALSE;
		}
		
		//controllo se esiste gia' un caricamento con stesso importo/numero/data/orario
		$duplicato = $this->check_scontrino_duplicato($idProva, $importo, $numero, $data_scontrino, $orario, $this->ion_auth->user()->row()->id);
		
		if (!$duplicato){
			//inizio la transazione, se poi andra' bene committero', altrimenti rollback
			$this->db->trans_begin();
			//carico library upload
			$this->load->library('upload');		
			//solo immagini JPG o PNG			
			$config['allowed_types'] = 'jpeg|jpg|png';
			$config['max_size']	= '5120';
			//data odierna in formato timestamp. per la composizione del nome file
			$now_timestamp = $now_date->getTimestamp();
			//nome file cosi' composto: TIMESTAMP - TRATTINO - NOME (spazio sostituiti da underscore)
			$config['file_name'] = str_replace(' ','_',$now_timestamp."-".$_FILES['scontrino']['name']);
			$ext = pathinfo($_FILES['scontrino']['name'], PATHINFO_EXTENSION);
			$data_odierna_string = $now_date->format('d/m/Y');
			$ora_attuale_string = $now_date->format('G.i.s');
			$config['file_name'] = "IDUT-".$this->ion_auth->user()->row()->id."-DATA-".$data_odierna_string."-ORA-".$ora_attuale_string.".".$ext;	
			//dove andro' a caricare lo scontrino
			$config['upload_path'] = './uploads/scontrini/';
			//salvo path completo per poi inserirlo nel db
			$file_scontrino = $config['upload_path'].$config['file_name'];
			//inizializzo upload
			$this->upload->initialize($config);

			//se riesco a caricare il file proseguo
			if ($this->upload->do_upload('scontrino')){
				$file_scontrino = $config['upload_path'].$this->upload->file_name;
				//ottengo id prova valida
				$prova_validaID = $this->getValiditaProvaIDbyDescrizione('Valida');	
				//se ce l'ho, proseguo				
				if ($prova_validaID){
					//caricamento model necessari
					$this->load->model('Punteggio');
					$this->load->model('Tipi_prova');
					$this->load->model('Prove');
					//ottengo dati della prova e del tipo prova
					$dati_prova = $this->Prove->get($idProva);
					$dati_tipo_prova = $this->Tipi_prova->get($dati_prova->FK_prove_tipiProve);
					
					//array di oggetti con i punteggi delle prove sostenute dall'utente, mi serve per un ulteriore controllo sul fatto che l'utente possa ancora partecipare
					$punteggi_obj = $this->Punteggio->get_many_by(array('FK_punteggio_user' => $this->ion_auth->user()->row()->id, 'FK_punteggio_prova' => $idProva/*, 'FK_punteggio_validitaProva' => $prova_validaID)*/));
					$numero_prove_sostenute = sizeof($punteggi_obj);
					//se puo farlo, preparo l'array di insert punteggio					
					if ($this->check_prova_ended($dati_prova->ripetibile, $numero_prove_sostenute)){
						$arr_punteggio = array(
							'FK_punteggio_validitaProva' => $prova_validaID,
							'FK_punteggio_user' => $this->ion_auth->user()->row()->id,
							'FK_punteggio_prova' => $idProva,
							'punteggio' => $dati_tipo_prova->punteggio,
							'ip' => $this->input->ip_address()
						);
						//inserisco il punteggio
						$idPunteggio = $this->addPunteggio($arr_punteggio);
						//se ci riesco, inserisco la prova upload. Ho inserito prima il punteggio perche' nella tabella l'id ho una FK che referenzia punteggio.
						if ($idPunteggio){
							$arr_insert = array(
								'textcol_1' => $importo,
								'textcol_2' => $numero,
								'datetimecol_1' => $data_scontrino->format('Y-m-d 00:00:00'),
								'textcol_3' => $orario,
								'filecol_1' => $file_scontrino,
								'FK_upload_tipoProva' => $dati_prova->FK_prove_tipiProve,
								'FK_upload_punteggio' => $idPunteggio
							);							
							$idScontrino = $this->prova_upload->insert($arr_insert);
							//ultimo check, se lo passo e' tutto ok
							if (!$idScontrino) {
								$errore = true;
								$dettagli = "Errore durante il salvataggio dello scontrino.";
							}							
						}
						else {
							$errore = true;
							$dettagli = "Errore durante il salvataggio dello scontrino.";
						}
					}
					else {
						$errore = true;
						$dettagli = "Non puoi pi&ugrave; ripetere questa prova.";
					}
				}
				else {
					$errore = true;
					$dettagli = "Errore durante la gestione della prova.";
				}	
												
			}	
			else {
				$dettagli = $this->upload->display_errors();
				$msgtype = "alert";
				$errore = true;
			}					
		}
		else {
			$dettagli = "Hai gi&agrave; caricato questo scontrino.";
			$msgtype = "alert";
			$errore = true;
		}
		if (!$errore){
			$message = "Scontrino aggiunto con successo";
			$msgtype = "success";				
			$this->db->trans_commit();
		}
		else {
			$message = $dettagli;
			$msgtype = "alert";
			$this->db->trans_rollback();
			}
		$arr = array('result' => $msgtype, 'message' => $message, 'dettagli' => $dettagli);
		$this->session->set_flashdata('result', $msgtype);
		$this->session->set_flashdata('text', $message);
		echo json_encode($arr);
		return true;
	}
	
	public function nvl($val, $replace)
	{
	    if( is_null($val) || $val === '' || $val == '0000-00-00 00:00:00' )  return $replace;	    
	    else                                
	    	return $val;
	}
	
	private function check_scontrino_duplicato($idProva, $importo, $numero, $data_scontrino, $orario, $user){		
		$dati_punteggi = $this->get_dati_punteggi($idProva, $this->ion_auth->user()->row()->id);
		if (!$dati_punteggi){
			//se non ho punteggi vuol dire che l'utente non ha mai svolto questa prova, e non possono esserci duplicati
			return false;
		}
		//se invece ne ho, devo interrogare anche la tabella upload per vedere se i dati immessi sono identici a quelli di una prova gia' completata da questo utente.
		//converto in ID separati da virgola, tipo X, Y, Z. mi servira' per la condizione IN sulla colonna FK_upload_punteggio
		$csv_punteggi = $this->convert_dati_punteggi_to_csv($dati_punteggi);
		$this->db->select('*');
		$this->db->from('upload');
		$this->db->where("FK_upload_punteggio in ($csv_punteggi)", null, false);
		//compongo la query coi dati immessi
		$this->db->where(array(
			'textcol_1' => $importo,
			'textcol_2' => $numero,
			'datetimecol_1' => $data_scontrino->format('Y-m-d 00:00:00'),
			'textcol_3' => $orario
			));
		$query = $this->db->get();
		$arr_prove = $query->result();
		if ($arr_prove){
			//se ho prove, ho duplicati
			return true;
		}
		return false;
		//echo $this->db->last_query();

	}
	
	private function get_dati_punteggi($idProva, $user = NULL){
		$this->load->model('Punteggio');
		if ($user){			
			return $this->Punteggio->get_many_by(array('FK_punteggio_prova' => $idProva, 'FK_punteggio_user' => $this->ion_auth->user()->row()->id));
		}
		else {
			return $this->Punteggio->get_many_by(array('FK_punteggio_prova' => $idProva));
		}
	}
	
	private function convert_dati_punteggi_to_csv($dati_punteggi){
		$output = array_map(function ($object) { return $object->punteggioID; }, $dati_punteggi);
		return implode(', ', $output);
	}

}