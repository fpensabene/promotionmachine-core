<body class="backend-page"> 

<?php echo $top_menu;?>

<!---*** BANDA PROMOTIONMACHINE ***-->
<section class="promotion-machine-background">
	<div class="row">
		<div class="small-12 text-center column">
		</div>
	</div>
</section>

<!---*** BREADCRUMBS ***-->
<section class="hrow">
	<div class="row">
		<div class="column">
	
			<ul class="breadcrumbs">
			  <li><a href="<?=site_url()?>admin/index">DASHBOARD</a></li>
			  <li class="unavailable"><a href="#">Admin</a></li>
			  <li><a href="<?=site_url().$this->uri->segment(1)."/".$this->uri->segment(2)?>">Gestione partecipanti</a></li>
			</ul>
		
		</div> <!--chiudo column-->
	</div> <!--chiudo row-->
</section>


<!---*** CANCELLARE!!!!! ***-->
<div class="row hrow">
	<div class="column">
		<div class="panel callout  radius">
		  <h5><span class="alert label">Attenzione:</span> PROBLEMI da risolvere</h5>
		  <ul>
		  	<li><strike>Al posto di id mettiamo la mail del contestant</strike></li>
		  	<li><strike>Compiliamo i dati del periodo qui sotto</strike></li>
		  	<li><strike>Nello status togliamo le funzioni di moderazione e mettiamo veramente lo status (approvato, annullato, da approvare)</strike></li>
		  	<li><strike>Aggiungiamo una colonna in più all'inizio con una icona - da li cliccando su va sulla moderazione</strike></li>
		  	<li><strike>vedi come deve essere fatta la pagina di moderazione su FLOW - UPLOAD > AMMINISTRA > AGGIUNGERE UNA ICONA PER FARE LA MODERAZIONE DELLO SCONTRINO - VEDI IMMAGINE</strike></li>
		  </ul>
		</div>
	</div> <!--chiudo column-->
</div> <!--chiudo row-->
<!---*** CANCELLARE!!!!! ***-->


<!---*** BOX CON I DATI DEL PERIODO ***-->
<section class="hrow">
	<div class="row" data-equalizer>
		<div class="small-12 medium-4 large-4 column">
			<div class="dati-concorso-box" data-equalizer-watch>
				<h2 class="dati-concorso-title ">PERIODO</h2>
				<p class="dati-concorso-subtitle">Nome del periodo</p>
				<p class="dati-concorso-value dati-concorso-value--title"><?=$dati_periodo->descrizione?></p>
			</div>
		</div> <!--chiudo column-->
		
		<div class="small-12 medium-4 large-4 column">
			<div class="dati-concorso-box" data-equalizer-watch>
				<h2 class="dati-concorso-title ">DATA INIZIO</h2>
				<p class="dati-concorso-subtitle">Data in cui inizia il periodo</p>
				<p class="dati-concorso-value dati-concorso-value--start"><?=DateTime::createFromFormat('Y-m-d G:i:s', $dati_periodo->data_inizio)->format('d/m/Y H:i:s')?></p>
			</div>
		</div> <!--chiudo column-->
		
		<div class="small-12 medium-4 large-4 column">
			<div class="dati-concorso-box" data-equalizer-watch>
				<h2 class="dati-concorso-title ">DATA FINE</h2>
				<p class="dati-concorso-subtitle">Data in cui finisce il periodo</p>
				<p class="dati-concorso-value dati-concorso-value--end"><?=DateTime::createFromFormat('Y-m-d G:i:s', $dati_periodo->data_fine)->format('d/m/Y H:i:s')?></p>
			</div>
		</div> <!--chiudo column-->
	</div> <!--chiudo row-->
</section>




<section class="hrow">
	<div class="row">
		<h2>Amministrazione scontrini</h2>
		<div id="output" data-alert class="alert-box radius" style="display: none">
		  <a href="#" class="close">&times;</a>
		</div>
	</div>
	<div class="row">
		<div class="small-12 column">
			<label for="filter">Filtra:
			<input id="filter" type="text"/>
			
			</label>
			<table id="scontrini" class="footable" data-filter="#filter" data-filter-minimum="1">
			</table>
		</div>
	</div>
</section>
<script>


function popolaScontrini(where,base_url){
	$.ajax({
	    type:'GET',
	    dataType: 'json',
	    url:base_url+'upload_backend/get_dati_upload/<?=$idprova?>/1',
	    data:{},
	    success:function(response){
	    //attenzione. success (questo sopra) vuol dire che la chiamata ha avuto successo, che il server ha risposto senza dare errori fatali, non vuol dire che la funzione abbia fatto quel che doveva fare
	    	if (response.result=='success'){
		    	var dati = response.output;
		        $(where).html("");
		        $(where).append("<thead><tr></tr></thead>");
		        $(where+ " tr").append("<th data-sort-ignore=\"true\"></th>");
		        $(where+ " tr").append("<th>Utente</th>");
		        $(where+ " tr").append("<th>Numero</th>");
		        $(where+ " tr").append("<th>Importo</th>");
		        $(where+ " tr").append("<th>Data</th>");
		        $(where+ " tr").append("<th data-sort-ignore=\"true\">File</th>");
		        $(where+ " tr").append("<th>Stato</th>");		        
		        $(where).append("<tbody></tbody>");		        
		        for (var i = 0; i < dati.length; i++) {
			        var trclass = "";
			        if (dati[i]['descr_validita_prova']!='Valida'){
				        trclass = "respinto";
			        }
			        $(where).append("<tr class=\"row_"+dati[i]['idUPLOAD_carica']+" "+trclass+"\"></tr>");
			        $(where+" tr:last").append("<td><a href='<?echo base_url();?>upload_backend/modera/"+dati[i]['idUPLOAD_carica']+"'><i class='fi-zoom-in'></i></a></td>")
			        $(where+" tr:last").append("<td>"+dati[i]['user']+"</td>");
			        $(where+" tr:last").append("<td>"+dati[i]['textcol_2']+"</td>");
			        $(where+" tr:last").append("<td>"+dati[i]['textcol_1']+"</td>");
			        $(where+" tr:last").append("<td>"+dati[i]['datetimecol_1']+"</td>");
			        $(where+" tr:last").append("<td><a href='<?echo base_url();?>"+dati[i]['filecol_1']+"'>Vedi scontrino</a></td>");
			        $(where+" tr:last").append("<td>"+dati[i]['descr_validita_prova']+"</td>");
				}
				$('.footable').footable();
	        }
	        else {
	        }
	    },
	    error:function(response){
	    }
	});		
}
$(document).ready(function(){
	popolaScontrini("#scontrini","<?echo base_url();?>");
	
});

</script>
