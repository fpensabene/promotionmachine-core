<body class="backend-page"> 

<?php echo $top_menu; ?>

<!---*** BANDA PROMOTIONMACHINE ***-->
<section class="promotion-machine-background">
	<div class="row">
		<div class="small-12 text-center column">
		</div>
	</div>
</section>

<!---*** BREADCRUMBS ***-->
<section class="hrow">
	<div class="row">
		<div style="display:none" id="output" data-alert class="alert-box radius">
		  <a href="#" class="close">&times;</a>
		</div>
		<div class="column">
	
			<ul class="breadcrumbs">
			  <li><a href="<?=site_url()?>admin/index">DASHBOARD</a></li>
			  <li class="unavailable"><a href="#">Admin</a></li>
			  <li><a href="<?=site_url().$this->uri->segment(1)."/".$this->uri->segment(2)?>">Gestione partecipanti</a></li>
			</ul>
		
		</div> <!--chiudo column-->
	</div> <!--chiudo row-->
</section>
<div class="row">
	<div class="small-12 column">
		L'utente <?=$this->dati_upload->user?> <br/>
		ha caricato questo scontrino: <br/>
		<img style="width: 400px; height: 200px;" src="<?=substr($this->dati_upload->filecol_1,1)?>"/><br/>
		numero: <?=$this->dati_upload->textcol_2?><br/>
		importo: <?=$this->dati_upload->textcol_1?><br/>
		data: <?=DateTime::createFromFormat('Y-m-d G:i:s', $this->dati_upload->datetimecol_1)->format('d/m/Y')?> <br/>
		ora:  <?=$this->dati_upload->textcol_3?><br/>
		<form action="" method="POST" id="moderazione_upload" method="POST" enctype="multipart/form-data">
			<div class="row">
				<div class="small-12 columns">		
					<label for="validita">Moderazione:
						<select id="validita" class="medium" required>
						  <option value="">Seleziona...</option>
						</select>
				    </label>
				</div>
				<div class="small-12 columns">
					<label for "motivazione">Motivazione:
						<textarea id="motivazione" name="motivazione"><?=$this->dati_upload->motivazione_moderazione?></textarea>
					</label>
				</div>
			</div>
			<div class="row">  
				<div class="small-12 columns">
					<button class="radius button" id="button_go" type="button" onclick="javascript:cambiaValidita('<?=base_url()?>')">Procedi</button>
					<div id="spinner" class="spinner" style="display:none;">
					    <img id="img-spinner" src="/assets/img-backend/ajax-loader.gif" alt="Loading"/>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

<script>
$(document).ready(function(){
	popolaValiditaStati("#validita",'<?=base_url()?>');
	$("#validita").val(<?=$this->dati_upload->validita_id?>);
});
function popolaValiditaStati(where, base_url){
	$.ajax({
	    type:'GET',
	    dataType: 'json',	    
	    async: false,
	    url:base_url+'upload_backend/get_dati_validita_prove',
	    data:{},
	    success:function(response){
	    //attenzione. success (questo sopra) vuol dire che la chiamata ha avuto successo, che il server ha risposto senza dare errori fatali
	    	if (response.result=='success'){		    	
		    	var dati = response.output;
		        for (var i = 0; i < dati.length; i++) {
			        $(where).append("<option value='"+dati[i]['validitaProvaID']+"'>"+dati[i]['descrizione']+"</option>");
				}
	        }
	        else {
	        }
	    },
	    error:function(response){
	    }
	});	
}

function cambiaValidita(base_url){
	var validita = $("#validita").val();
	var motivazione = $("#motivazione").val();
	var idupload = <?=$this->dati_upload->idUPLOAD_carica?>;
	var idpunteggio = <?=$this->dati_upload->FK_upload_punteggio?>;

	$.ajax({
	    type:'POST',
	    dataType: 'json',
	    beforeSend: function(){
		  $("#spinner").show();
		  $("#button_go").attr("disabled","disabled");
		  $("#button_go").addClass("disabled");
		},
		complete: function(){
		  $("#spinner").hide();
		  $("#button_go").removeAttr("disabled");
		  $("#button_go").removeClass("disabled");
		},
	    url:base_url+'upload_backend/cambiaValidita',
	    data: {
		    validita : validita,
		    idpunteggio : idpunteggio,
		    idupload : idupload,
		    motivazione : motivazione
	    },
	    success:function(response){
	    //attenzione. success (questo sopra) vuol dire che la chiamata ha avuto successo, che il server ha risposto senza dare errori fatali, non vuol dire che la funzione abbia fatto quel che doveva fare
	    	if (response.result=='success'){
		        printMsg('success',response.message); 
		        
	        }
	        else {
		        printMsg('alert',response.message); 
	        }
	    },
	    error:function(response){
	        printMsg('alert','Errore durante la chiamata'); 
	    }
	});	
}
</script>