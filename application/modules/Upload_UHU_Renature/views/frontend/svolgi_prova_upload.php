<?php echo $barraNavigazione; ?>
<?php echo $testataConcorso; ?>

<?php 	if ($partecipa_ancora&&$in_corso): ?>

	<!---***********************************-->
	<!---*** ERRORI E ALLERT SERVER SIDE ***-->
	<!---***********************************-->
	<div class="row">
		<div class="small-12 medium-8 large-8 medium-centered large-centered column">
			<div id="output" data-alert class="alert-box radius <?if($message['result']){echo $message['result'];} ?>" <?if(!$message['result']){?>style="display: none"<?}?>>
				<?if($message['result']){ echo $message['text'];}?>
				<a href="#" class="close">&times;</a>
			</div>
		</div> <!--chiudo column-->
	</div> <!--chiudo row-->
	
	
	<!---*************-->
	<!---*** INTRO ***-->
	<!---*************-->
	<section class="hrow">
		<div class="row">
			<div class="small-12 medium-8 large-8 medium-centered large-centered column">
			<div class="hrow-intro">
				<h1>REGISTRA SCONTRINO - MESEXXXXX</h1>
				<p class="hrow-intro-text">Ciao <strong><?=$this->ion_auth->user()->row()->nome?></strong>,<br>
				Acquista almeno uno stic UHU RENATURE da 8, 21 o 40 grammi nei punti vendita I LOVE UHU, registra i dati dello scontrino e carica la foto leggibile dello scontrino in formato .jpg: se lo scontrino è parlante basta lo scontrino, altrimenti nella stessa foto devi far vedere anche il codice a barre EAN del prodotto acquistato. Non c'è limite al numero di scontrini caricabili. Ogni scontrino può essere caricato una sola volta.</p>
				</div>	
				
			</div>
		</div>
	</section>
	
	<!---*****************************-->
	<!---*** FORM UPLOAD SCONTRINO ***-->
	<!---*****************************-->
	<div class="row">
		<div class="small-12 medium-8 large-8 medium-centered large-centered column">
			
		<form style="margin-top:20px;" action="" method="POST" id="upload_scontrino" method="POST" enctype="multipart/form-data">
			<div class="row">  
				<input name="idprova" id="idprova" type="hidden" value="<?=$idprova?>"/>
				<div class="small-12  columns">			
					<label>Inserisci l'importo totale così come indicato sullo scontrino, usando la virgola prima dei decimali
						<input name="importo" id="importo" type="text" value=""/>
					</label>
				</div>
				<div class="small-12  columns">		
					<label>Inserisci il numero dello scontrino
						<input name="numero" id="numero" type="text" value=""/>	
					</label>		    
				</div>
				<div class="small-12 columns">
					<label>Inserisci la data scontrino
						<input name="data_scontrino" id="data_scontrino" type="text" readonly="readonly" value=""/>
					</label>
				</div>
				<div class="small-12 columns">
					<label>Inserisci l'orario dello scontrino
						<label>Ora
							<input name="orario_ora" id="orario_ora" type="text" value=""/>
						</label>
						<label>Min
							<input name="orario_min" id="orario_min" type="text" value=""/>
						</label>
					</label>
				</div>
				<div class="small-12 columns">			
					<label>Carica la foto dello scontrino: se nello scontrino compare scritto stic renature basta la loto leggibile dello scontrino, altrimenti nella foto deve comparire anche lo stic che hai acquistato. Peso massimo della foto: 5MB
						<input name="scontrino" accept="image/jpeg, image/png" id="scontrino" type="file"/>
					</label>
				</div>
			</div>
			<div class="row">  
				<div class="small-12 columns">
					<button class="radius button" id="button_go" type="button" onclick="javascript:gestioneUpload('<?echo base_url();?>', $('#idprova').val())">Procedi</button>
					<div id="spinner" class="spinner" style="display:none;">
					    <img id="img-spinner" src="/assets/img-frontend/ajax-loader.gif" alt="Loading"/>
					</div>
				</div>
			</div>		
		</form>
		
		</div> <!--chiudo column -->
	</div> <!--chiudo row-->


<?php else: ?>

	<div class="row">
		<div class="small-12 medium-8 large-8 medium-centered large-centered colum">
			<h1>non puoi eseguire di nuovo questa prova - dobbiamo dare una motivazione DINAMICA</h1>
		</div> <!--chiudo column-->
	</div> <!--chiudo row-->

<?php endif; ?>



<div class="row">
	<div class="small-12 medium-8 large-8 medium-centered large-centered column">
		<h3>Scontrini caricati</h3>
		<table id="scontrini" style="width: 100%;">
		</table>
	</div>
</div>

<script>
		
	function gestioneUpload(base_url, idprova){
		if (!$("#upload_scontrino").valid()){
			return false;
		}
		$.ajax({
		    type:'POST',
		    dataType: 'json',
		    beforeSend: function(){
			  $("#spinner").show();
			  $("#button_go").attr("disabled","disabled");
			  $("#button_go").addClass("disabled");
			},
			complete: function(){
			  $("#spinner").hide();
			  $("#button_go").removeAttr("disabled");
			  $("#button_go").removeClass("disabled");
			},
		    url:base_url+'upload/gestione/'+idprova,
		    data: new FormData( $("#upload_scontrino")[0] ),
			processData: false,
			contentType: false,
		    success:function(response){
		    //attenzione. success (questo sopra) vuol dire che la chiamata ha avuto successo, che il server ha risposto senza dare errori fatali, non vuol dire che la funzione abbia fatto quel che doveva fare
		    	if (response.result=='success'){
			        location.reload(); 
		        }
		        else {
			        printMsg('alert',response.message); 
		        }
		    },
		    error:function(response){
		        printMsg('alert','Errore durante la chiamata'); 
		    }
			});	
	}
	
	
	function popolaScontrini(where,base_url){
		$.ajax({
		    type:'GET',
		    dataType: 'json',
		    url:base_url+'upload/get_dati_upload/<?=$idprova?>',
		    data:{},
		    success:function(response){
		    //attenzione. success (questo sopra) vuol dire che la chiamata ha avuto successo, che il server ha risposto senza dare errori fatali, non vuol dire che la funzione abbia fatto quel che doveva fare
		    	if (response.result=='success'){
			    	var dati = response.output;
			        //console.log(response.output); 
			        if (dati.length>0){
				        $(where).html("");
				        $(where).append("<tr><th>Numero</th><th>Importo</th><th>Data</th><th>Ora</th><th>File</th><th>Stato</th></tr>");
				        for (var i = 0; i < dati.length; i++) {
					        var trclass = "";
					        if (dati[i]['descr_validita_prova']!='Valida'){
						        trclass = "respinto";
					        }
					        $(where).append("<tr class='"+trclass+"'></tr>");
					        $(where+" tr:last").append("<td>"+dati[i]['textcol_2']+"</td>");
					        $(where+" tr:last").append("<td>"+dati[i]['textcol_1']+"</td>");
					        $(where+" tr:last").append("<td>"+dati[i]['datetimecol_1']+"</td>");
					        $(where+" tr:last").append("<td>"+dati[i]['textcol_3']+"</td>");
					        $(where+" tr:last").append("<td><a target='_blank' href='<?echo base_url();?>"+dati[i]['filecol_1']+"'>Vedi scontrino</a></td>");
					        $(where+" tr:last").append("<td>"+dati[i]['descr_validita_prova']+"</td>");
						}
					}
		        }
		        else {
		        }
		    },
		    error:function(response){
		    }
		});		
	}
	$(document).ready(function(){
		popolaScontrini("#scontrini","<?echo base_url();?>");
		$("input[name='data_scontrino']").datepicker({
			dateFormat: 'dd/mm/yy',
			maxDate: 0,
			minDate: '<?=DateTime::createFromFormat('d/m/Y G:i:s', $this->dati_concorso['output']['data_inizio'])->format('d/m/Y')?>'
		});	
		

		$("#upload_scontrino").validate({
			rules: {
				importo: {
					required: true,
					number: true
				},
				numero: {
					required: true
				},
				data_scontrino: {
					required: true,
					minlength: 10,
					regex: /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/
				},
				orario_ora: {
					required: true,
					min: 0,
					max: 23
				},
				orario_min: {
					required: true,
					min: 0,
					max: 59
				},
				scontrino: {
					required: true,
					accept: 'image/jpeg, image/png'
				}
			},
			messages: {
				importo: {
					required: 'Devi inserire l\'importo che appare sullo scontrino',
					number: 'L\'importo inserito non &egrave; valido.'
				},
				numero: {
					required: 'Devi inserire il numero che compare sullo scontrino.'
				},
				data_scontrino: {
					required: 'Devi inserire la tua data dello scontrino.',
					minlength: 'La data dello scontrino inserita non &egrave; valida.',
					regex: 'La data dello scontrino inserita non &egrave; valida.'
				},
				orario_ora: {
					required: 'Devi inserire l\'ora dell\'orario di acquisto che compare sullo scontrino. Esempio, acquisto alle 18:25, qui dovrai inserire 18.',
					min: 'Il valore deve essere compreso tra 0 e 23.',
					max: 'Il valore deve essere compreso tra 0 e 23.'
				},
				orario_min: {
					required: 'Devi inserire i minuti dell\'orario di acquisto che compare sullo scontrino. Esempio, acquisto alle 18:25, qui dovrai inserire 25.',
					min: 'Il valore deve essere compreso tra 0 e 59.',
					max: 'Il valore deve essere compreso tra 0 e 59.'
				},
				scontrino: {
					required: 'Devi obbligatoriamente caricare uno scontrino.'					
				}
			}
			
		});
	});
</script>
